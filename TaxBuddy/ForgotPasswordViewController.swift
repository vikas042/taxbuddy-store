//
//  ForgotPasswordViewController.swift
//  TaxBuddy
//
//  Created by Vikash Rajput on 25/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var txtFieldEmail: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }


    @IBAction func submitButtonTapped(_ sender: Any) {
        if txtFieldEmail.text?.count == 0 {
            AppHelper.showAlertView(message: EnterEmailMsg)
            return;
        }
        else if AppHelper.isNotValidEmail(email: txtFieldEmail.text!) {
            AppHelper.showAlertView(message: EnterValidEmailMsg)
            return;
        }
        
        UserDetails.sharedInstance.userEmail = txtFieldEmail.text
        let parameters = ["option": "send_link","email": UserDetails.sharedInstance.userEmail!]
        callForgotPasswordApi(params: parameters)
    }
    
    func callForgotPasswordApi(params :[String: String])  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kForgotPasswordURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    let message = result!["message"]?.string
                    
                    let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertController.Style.alert)
                    
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    
                    self.navigationController?.present(alert, animated: true, completion: nil)
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
            
        }
    }
}
