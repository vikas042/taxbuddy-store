//
//  LoginViewController.swift
//  TaxBuddy
//
//  Created by Vikash Rajput on 25/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import GoogleSignIn
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import SwiftyJSON

class LoginViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {

    @IBOutlet weak var passwordFieldView: UIView!
    @IBOutlet weak var emailFieldView: UIView!

    @IBOutlet weak var txtPassowrd: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    var isGoogleLoginSilently = false

    @IBOutlet weak var showButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        AppHelper.getGuestToken()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    

    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        let forgotPasswordVC = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController")
        navigationController?.pushViewController(forgotPasswordVC!, animated: true)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        
        if txtEmail.text?.count == 0 {
            AppHelper.showAlertView(message: EnterEmailMsg)
            return;
        }
        else if txtPassowrd.text?.count == 0 {
            AppHelper.showAlertView(message: EnterPasswordMsg)
            return;
        }
        else if txtPassowrd.text!.count < 6{
            AppHelper.showAlertView(message: MinPasswordLenghtMsg)
            return;
        }
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        var deviceToken: String = UserDetails.sharedInstance.deviceToken
        
        if deviceToken.count == 0 {
            deviceToken = "1F7C97AB9A2885C2178E09B2C69C679877CE38E1F6321DBBCBAE31EBE12A4123";
        }
        
        let parameters = ["option": "login","email": txtEmail.text! , "password": txtPassowrd.text!,"device_id": deviceID,"device_token": deviceToken,"account_type": "1","device_type": "1","user_type":"store"]
        callLoginApi(params: parameters)
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
        if !AppHelper.isInterNetConnectionAvailable() {
            return;
        }
        FacebookLoginManager.sharedInstance.callLoginMangerWithCompletion { (result, error) in
            let responseJSON = JSON(result as AnyObject);
            AppHelper.sharedInstance.removeSpinner();
            if (result != nil){
                let statusCode = responseJSON["status"]
                
                if statusCode == "200"{
                    if let response = responseJSON.dictionary!["response"]{
                        let apiSecretKey = response["api_secret_key"].string
                        let userId = response["user_id"].string
                        UserDetails.sharedInstance.userEmail = response["email"].string
                        UserDetails.sharedInstance.userName = response["name"].string
                        UserDetails.sharedInstance.userCity = response["city"].string
                        UserDetails.sharedInstance.userAddress = response["address1"].string
                        
                        UserDetails.sharedInstance.accessToken = response["access_token"].string ?? ""
                        UserDetails.sharedInstance.cartCount = response["total_cart"].string ?? "0"
                        
                        AppHelper.saveUserDetails()
                        
                        if let phone = response["phone"].string{
                            UserDetails.sharedInstance.phoneNumber = phone
                        }

                        UserDetails.sharedInstance.userID = userId ?? "";
                        UserDetails.sharedInstance.apiSecretKey = apiSecretKey ?? "";
                        
                        
                        //                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        //                            self.loginCompletionBlock(true)
                        //                        }
                        
                    }
                    
                    AppHelper.saveUserDetails()
                    //                    if self.isNeedToShowCrossButton{
                    //                        self.dismiss(animated: true, completion: nil)
                    //                    }
                    //                    else if self.isScreenForLogin{
                    //                        self.navigationController?.popViewController(animated: true)
                    //                    }
                    //                    else{
                    //                        AppHelper.makeHomeViewControllerAsRootViewController()
                    //                    }
                    AppHelper.makeHomeViewControllerAsRootViewController()

                }
                else if statusCode == "801"{
                    AppHelper.getGuestToken()
                }
                else{
                    let errorMsg = responseJSON["message"].string ?? kErrorMsg
                    AppHelper.showAlertView(message: errorMsg)
                }
            }
        }
    }
    
    @IBAction func googleButtonTapped(_ sender: Any) {
        callGoogleSignIn()
    }
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        let signupVC = storyboard?.instantiateViewController(withIdentifier: "SignupViewController")
        navigationController?.pushViewController(signupVC!, animated: true)
    }
    
    @IBAction func showButtonTapped(_ sender: Any) {
        txtPassowrd.isSecureTextEntry = !txtPassowrd.isSecureTextEntry
        if txtPassowrd.isSecureTextEntry{
            showButton.setTitle("SHOW", for: .normal)
        }
        else{
            showButton.setTitle("HIDE", for: .normal)
        }
    }
    
    func callLoginApi(params :[String : String])  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        txtEmail.resignFirstResponder()
        txtPassowrd.resignFirstResponder()

        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kLoginURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                UserDetails.sharedInstance.userToken = result!["token"]?.string
                if statusCode == "200"
                {
                    if let reponse = result!["response"]?.dictionary{
                        let apiSecretKey = reponse["api_secret_key"]?.string
                        let userId = reponse["store_id"]?.string

                        if let details = reponse["store_details"]?.dictionary{
                            UserDetails.sharedInstance.store_gmt = details["store_gmt"]?.string ?? ""
                            StoreDetail.sharedInstance.store_gmt = details["store_gmt"]?.string ?? ""
                            StoreDetail.sharedInstance.store_distance = details["store_gmt"]?.string ?? ""
                            StoreDetail.sharedInstance.store_id = details["store_id"]?.string ?? ""
                            StoreDetail.sharedInstance.store_image = details["store_image"]?.string ?? ""
                            
                            StoreDetail.sharedInstance.in_service = details["in_service"]?.string ?? ""
                            
                            StoreDetail.sharedInstance.store_name = details["store_name"]?.string ?? ""
                            StoreDetail.sharedInstance.store_latitude = details["store_latitude"]?.string ?? ""
                            StoreDetail.sharedInstance.store_longitude = details["store_longitude"]?.string ?? ""
                            
                            StoreDetail.sharedInstance.store_image = details["store_image"]?.string ?? ""
                            StoreDetail.sharedInstance.store_close_time = details["store_close_time"]?.string ?? ""
                            StoreDetail.sharedInstance.store_open_time = details["store_open_time"]?.string ?? ""
                            StoreDetail.sharedInstance.store_address = details["store_address"]?.string ?? ""
                            StoreDetail.sharedInstance.store_phone = details["store_phone"]?.string ?? ""
                            StoreDetail.sharedInstance.store_email = details["store_email"]?.string ?? ""
                            AppHelper.saveStoreDetails()
                            AppHelper.saveUserDetails()
                        }
                        
                        UserDetails.sharedInstance.userID = userId ?? "";
                        UserDetails.sharedInstance.apiSecretKey = apiSecretKey ?? "";
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                            self.loginCompletionBlock(true)
//                        }
                        
                    }
                    AppHelper.saveStoreDetails()
                    AppHelper.saveUserDetails()
//                    if self.isNeedToShowCrossButton{
//                        self.dismiss(animated: true, completion: nil)
//                    }
//                    else if self.isScreenForLogin{
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                    else{
//                        AppHelper.makeHomeViewControllerAsRootViewController()
//                    }
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        let storyboard = UIStoryboard.init(name: "MainIPadStoryboard", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
                        let navigationVC = UINavigationController(rootViewController: homeVC)
                        navigationVC.navigationBar.isHidden = true
                        APPDELEGATE.window?.rootViewController = navigationVC
                        
                    }else{
                        AppHelper.makeHomeViewControllerAsRootViewController()
                    }

                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
            
        }
    }
    
    //MARK:- Google Login Delegates
    func callGoogleSignIn()
    {
        if GIDSignIn.sharedInstance().hasAuthInKeychain() == true{
            AppHelper.sharedInstance.displaySpinner()
            isGoogleLoginSilently = true;
            GIDSignIn.sharedInstance().signInSilently()
        }
        else{
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            AppHelper.showAlertView(message: error.localizedDescription)
        } else {
            
            if !isGoogleLoginSilently{
                AppHelper.sharedInstance.displaySpinner()
            }
            
            let userId = user.userID
            let fullName = user.profile.name
            let email = user.profile.email
            
            UserDetails.sharedInstance.userEmail = email
            UserDetails.sharedInstance.userName = fullName
            
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            
            var deviceToken: String = UserDetails.sharedInstance.deviceToken
            
            if deviceToken.count == 0 {
                deviceToken = "1111111111";
            }
            
            let parameters: Parameters = ["option": "signup","name": UserDetails.sharedInstance.userName!, "email": UserDetails.sharedInstance.userEmail!,"account_type": "3", "device_type": "1", "device_id": deviceID, "device_token": deviceToken,"google_id":userId!]
            
            callSocialLoginApi(params: parameters)
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print(error)
    }
    
    
    
    private func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        print(error)
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    private func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func callSocialLoginApi(params :Parameters)  {
        
        WebServiceHandler.performPOSTRequest(withURL: kGpLoginURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                UserDetails.sharedInstance.userToken = result!["token"]?.string
                if statusCode == "200"
                {
                    if let reponse = result!["response"]?.dictionary{
                        let apiSecretKey = reponse["api_secret_key"]?.string
                        let userId = reponse["user_id"]?.string
                        UserDetails.sharedInstance.userEmail = reponse["email"]?.string
                        UserDetails.sharedInstance.userName = reponse["name"]?.string
                        UserDetails.sharedInstance.userCity = reponse["city"]?.string
                        UserDetails.sharedInstance.userAddress = reponse["address1"]?.string
                        UserDetails.sharedInstance.userAddress1 = reponse["address2"]?.string
                        UserDetails.sharedInstance.userZipCode = reponse["zipcode"]?.string
                        UserDetails.sharedInstance.userCountry = reponse["country"]?.string ?? ""
                        UserDetails.sharedInstance.accessToken = reponse["access_token"]?.string ?? ""
                        
                        UserDetails.sharedInstance.notificaionCount = reponse["new_notifications"]?.string ?? "0"
                        UserDetails.sharedInstance.cartCount = reponse["total_cart"]?.string ?? "0"
                        
                        AppHelper.saveUserDetails()
                       
                        
                        if let phone = reponse["phone"]?.string{
                            UserDetails.sharedInstance.phoneNumber = phone
                        }
                        if  let userImage = reponse["image"]?.string{
                            UserDetails.sharedInstance.userImage = userImage
                        }
                        UserDetails.sharedInstance.userID = userId ?? "";
                        UserDetails.sharedInstance.apiSecretKey = apiSecretKey ?? "";
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                            self.loginCompletionBlock(true)
//                        }
                        
                    }
                    
                    AppHelper.saveUserDetails()
//                    if self.isNeedToShowCrossButton{
//                        self.dismiss(animated: true, completion: nil)
//                    }
//                    else if self.isScreenForLogin{
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                    else{
//                        AppHelper.makeHomeViewControllerAsRootViewController()
//                    }
                    AppHelper.makeHomeViewControllerAsRootViewController()

                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
}

