//
//  OrdersTableViewCell.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 10/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
protocol OrdersTableViewCellDelegate{
    func moreBtnTapped(index:Int)
    func cancelTapped(index:Int)
    func approvedTapped(index:Int)
    func readyToPickTapped(index:Int)
}
class OrdersTableViewCell: UITableViewCell {

   
    @IBOutlet weak var orderedLbl: UILabel!
    @IBOutlet weak var readyToPickOutlet: UIButton!
    @IBOutlet weak var approvedOutlet: UIButton!
    @IBOutlet weak var cancelOutlet: UIButton!
    var delegate:OrdersTableViewCellDelegate?
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var moreBtnOutlet: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var customerMobile: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    
    var popUpStatus = -1
    var deliveryType = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        readyToPickOutlet.isEnabled = false
        approvedOutlet.isEnabled = false
        cancelOutlet.isEnabled = false
        
        dropDownView.isHidden = true
        AppHelper.showShadowOnView(view: containerView)
        AppHelper.poshITCustomPopupShadow(view: dropDownView)
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        statusLabel.layer.borderWidth = 1
        statusLabel.layer.borderColor =  UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1).cgColor
        statusLabel.layer.cornerRadius = 5
        
        if popUpStatus == 0{
            cancelOutlet.isEnabled = true
            approvedOutlet.isEnabled = true
        }else if popUpStatus == 1{
            if deliveryType == "1"{
                readyToPickOutlet.setTitle("Ready To Ship", for: .normal)
            }else if deliveryType == "2"{
                readyToPickOutlet.setTitle("Ready To Pick", for: .normal)
            }
            cancelOutlet.isEnabled = false
            approvedOutlet.isEnabled = false
        }else if popUpStatus == 2{
           // moreBtnOutlet.isHidden = true
        }else if popUpStatus == 3{
           // moreBtnOutlet.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func moreBtnAction(_ sender: UIButton) {
        self.delegate?.moreBtnTapped(index:sender.tag)
    }
    @IBAction func markThisOrderAction(_ sender: Any) {
    }
    @IBAction func cancelAction(_ sender: UIButton) {
        dropDownView.isHidden = true
        self.delegate?.cancelTapped(index: sender.tag)
    }
    @IBAction func approvedAction(_ sender: UIButton) {
        dropDownView.isHidden = true
        self.delegate?.approvedTapped(index: sender.tag)
    }
    
    @IBAction func readyToPick(_ sender: UIButton) {
        dropDownView.isHidden = true
        self.delegate?.readyToPickTapped(index: sender.tag)
    }
    
}
