//
//  LocalMessages.swift
//
//  Created by Vikash Rajput on 3/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import Foundation
import UIKit


let kLoginFirstAlert = "Please login first"

let kPaymentMsg = "Processing payment... please wait."

let kAlert = "TaxBuddy Store"

let kErrorMsg = "Something went wrong."

let kNoInternetConnection = "Unable to connect to internet. Please check your internet connection."


let EnterEmailMsg = "Please enter email ID."

let EnterValidEmailMsg = "Please enter a valid email ID."

let EnterPasswordMsg = "Please enter your password."

let EnterConfirmPasswordMsg = "Please confirm your password."

let PasswordNotMatchedMsg = "Confirm password must be same."

let EnterNameMsg = "Please enter your name."

let EnterProductNameMsg = "Please enter product name."

let EnterAddressMsg = "Please enter your address."

let EnterZipCodeMsg = "Please enter your zip code."

let EnterCityCodeMsg = "Please enter your city."

let EnterCountryMsg = "Please enter your country."

let MinPasswordLenghtMsg = "Password length should be a minimum of 6 characters."

