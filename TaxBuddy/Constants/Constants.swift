//
//  Constants.swift
//  
//
//  Created by Vikash Rajput on 3/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import Foundation
import UIKit


let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate


//let SQUARE_APP_ID = "sq0idp-68uFGlhhgnKmfDM-D0z2Nw" // New Live
//let SQUARE_APP_ID = "sandbox-sq0idp-mFeb3efcFC3MXgXs2Hx6rQ" // sandbox


// Main URl
//let BASEURL = "http://52.37.151.212/"

// Developement URL
let BASEURL = "http://52.37.151.212:8081/"

// Main URL
//let BASEURL = "https://api.taxbuddyapp.com/"

//let imageBASEURL = "https://api.taxbuddyapp.com/"

let kToakenURL = BASEURL + "myoauth"
let kSignupURL = BASEURL + "user-signup"
let kCheckLoginURL = BASEURL + "user-login"
let kLoginURL = BASEURL + "user-login"
let kFbLoginURL = BASEURL + "user-signup"
let kGpLoginURL = BASEURL + "user-signup"
let kForgotPasswordURL = BASEURL + "user-forgotpassword"
let kChangePasswordURL = BASEURL + "uuser"
let kProductsURL = BASEURL + "uproducts"
let kOrdersURL = BASEURL + "seller"
let kUsersURL = BASEURL + "uuser"
let kWebViewURL = BASEURL + "acknowledge"
let kSellerAppURL = BASEURL + "seller"


let maxLimitForAddtoCart = 5

