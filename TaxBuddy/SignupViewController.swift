//
//  SignupViewController.swift
//  TaxBuddy
//
//  Created by Vikash Rajput on 25/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
   
    @IBOutlet weak var passwordFieldView: UIView!
    @IBOutlet weak var emailFieldView: UIView!
    
    @IBOutlet weak var txtFullName: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!

    @IBOutlet weak var showButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showButtonTapped(_ sender: Any) {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
        if txtPassword.isSecureTextEntry{
            showButton.setTitle("SHOW", for: .normal)
        }
        else{
            showButton.setTitle("HIDE", for: .normal)
        }

    }

    
    @IBAction func signupButtonTapped(_ sender: Any) {
        
        if txtFullName.text?.count == 0{
            AppHelper.showAlertView(message: EnterNameMsg)
            return;
        }
        else if txtEmail.text?.count == 0{
            AppHelper.showAlertView(message: EnterEmailMsg)
            return;
        }
        else if txtMobileNumber.text?.count == 0{
            AppHelper.showAlertView(message: "Please enter your phone number")
            return;
        }
        else if txtPassword.text?.count == 0{
            AppHelper.showAlertView(message: EnterPasswordMsg)
            return;
        }
        else if txtPassword.text!.count < 6{
            AppHelper.showAlertView(message: MinPasswordLenghtMsg)
            return;
        }


//        UserDetails.sharedInstance.fullName = txtFullName.text!;
//        UserDetails.sharedInstance.emailAdress = txtEmail.text!;
//        UserDetails.sharedInstance.phoneNumber = txtMobileNumber.text!;
//
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        var deviceToken: String = UserDetails.sharedInstance.deviceToken
        
        if deviceToken.count == 0 {
            deviceToken = "1111111111";
        }
        txtFullName.resignFirstResponder();
        txtFullName.resignFirstResponder();
        txtFullName.resignFirstResponder();

//        let phoneNumber = phoneNumberTxtField.text!.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        let parameters = ["option": "signup","name": txtFullName.text!, "email": txtEmail.text!,"phone": txtMobileNumber.text!, "password": txtPassword.text!, "account_type": "1", "device_type": "1", "device_id": deviceID, "device_token": deviceToken]
        
        callSignupAPI(params: parameters)
    }
    
    //MARK:- API Related Methods
    func callSignupAPI(params : [String: String])  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kSignupURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if let reponse = result!["response"]?.dictionary{
                        let apiSecretKey = reponse["api_secret_key"]?.string
                        let userId = reponse["user_id"]?.string
                        UserDetails.sharedInstance.userEmail = reponse["email"]?.string
                        UserDetails.sharedInstance.userName = reponse["name"]?.string
                        UserDetails.sharedInstance.accessToken = reponse["access_token"]?.string ?? ""
                        UserDetails.sharedInstance.userCity = reponse["city"]?.string
                        UserDetails.sharedInstance.userAddress = reponse["address1"]?.string
                        UserDetails.sharedInstance.userAddress1 = reponse["address2"]?.string
                        UserDetails.sharedInstance.userZipCode = reponse["zipcode"]?.string
                        UserDetails.sharedInstance.userCountry = reponse["country"]?.string ?? ""
                        AppHelper.saveUserDetails()
                        if let phone = reponse["phone"]?.string{
                            UserDetails.sharedInstance.phoneNumber = phone
                        }
                        if  let userImage = reponse["image"]?.string{
                            UserDetails.sharedInstance.userImage = userImage
                        }
                        UserDetails.sharedInstance.userID = userId ?? "";
                        UserDetails.sharedInstance.apiSecretKey = apiSecretKey ?? "";
                    }
                    
                    AppHelper.saveUserDetails()
                    AppHelper.makeHomeViewControllerAsRootViewController()
//                    self.delegate?.signUpSuccessfull()
//                    if self.isViewPresented {
//                        self.delegate?.dismissLoginViewController()
//                    }
//                    else{
//                        self.navigationController?.popViewController(animated: true)
//                    }
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
            
        }
    }
    
}
