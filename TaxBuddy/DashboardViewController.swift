//
//  DashboardViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 09/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CalendarDateRangePickerViewController
@available(iOS 10.0, *)

@available(iOS 10.0, *)
class DashboardViewController: UIViewController,CalendarDateRangePickerViewControllerDelegate {
    
    var selectedBtn = 1
    
    @IBOutlet weak var totalPayout: UILabel!
    @IBOutlet weak var totalOrderPlaced: UILabel!
    @IBOutlet weak var totalOrderAmount: UILabel!
    @IBOutlet weak var totalOrderDelivered: UILabel!
    @IBOutlet weak var totalOrderCanceled: UILabel!
    @IBOutlet weak var totalOrderRefunded: UILabel!
    @IBOutlet weak var totalDiscountOffered: UILabel!
    @IBOutlet weak var totalProducts: UILabel!
    
    
    
    
    
    @IBOutlet weak var fromToDateLbl: UILabel!
    @IBOutlet weak var totalEarnings: UILabel!
  
    
    @IBOutlet weak var revenueContainer: UIView!
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var yesterdayBtn: UIButton!
    @IBOutlet weak var todayBtn: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var fromToBtn: UIButton!

    let date = Date()
    let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.DashboardButtonTapped), name: NSNotification.Name(rawValue: "DashButtonTapped"), object: nil)
        dateRangePickerViewController.delegate = self
        
        
        revenueContainer.layer.cornerRadius = 5
        revenueContainer.clipsToBounds=true
        fromToBtn.layer.cornerRadius = 5
        containerView.layer.cornerRadius = 5
        fromToBtn.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        AppHelper.poshITCustomPopupShadow(view: todayBtn)
        self.DashboardButtonTapped()
        // Do any additional setup after loading the view.
    }
    
    @objc func DashboardButtonTapped(){
    
        todayBtn.setTitleColor(UIColor(red: 60/255, green: 67/255, blue: 94/255, alpha: 1), for: .normal)
        fromToBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        yesterdayBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        
        
        
        fromToDateLbl.isHidden = true
        todayBtn.backgroundColor = UIColor.white
        yesterdayBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        fromToBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        yesterdayBtn.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
        fromToBtn.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
        todayBtn.layer.shadowOpacity = 1
        yesterdayBtn.layer.shadowOpacity = 0
        fromToBtn.layer.shadowOpacity = 0
        fromToBtn.setImage(UIImage(named: "iconBagBlue"), for: .normal)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //  dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let newDate = dateFormatter.string(from: date)
        let parameters = ["option": "dashboard","store_id": "\(UserDefaults.standard.string(forKey: "store_id")!)" , "store_gmt":"\(UserDefaults.standard.string(forKey: "store_gmt")!)","tStart":"\(newDate)","tEnd": "\(newDate)"]
        callDashboardApi(params: parameters)

    
    }
    
    
    @IBAction func todayBtnAction(_ sender: Any) {
        AppHelper.poshITCustomPopupShadow(view: todayBtn)
        selectedBtn = 1
        todayBtn.setTitleColor(UIColor(red: 60/255, green: 67/255, blue: 94/255, alpha: 1), for: .normal)
        fromToBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        yesterdayBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        
        
        fromToDateLbl.isHidden = true
        todayBtn.backgroundColor = UIColor.white
        yesterdayBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        fromToBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        yesterdayBtn.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
        fromToBtn.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
        todayBtn.layer.shadowOpacity = 1
        yesterdayBtn.layer.shadowOpacity = 0
        fromToBtn.layer.shadowOpacity = 0
        fromToBtn.setImage(UIImage(named: "iconBagBlue"), for: .normal)
        let parameters = ["option": "dashboard","store_id": "\(UserDefaults.standard.string(forKey: "store_id")!)" , "store_gmt":"\(UserDefaults.standard.string(forKey: "store_gmt")!)","tStart":"","tEnd": ""]
        callDashboardApi(params: parameters)
    }
    
    @IBAction func yesterdayBtnAction(_ sender: Any) {
        selectedBtn = 2
        
        yesterdayBtn.setTitleColor(UIColor(red: 60/255, green: 67/255, blue: 94/255, alpha: 1), for: .normal)
        fromToBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        todayBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        
        fromToDateLbl.isHidden = true
        AppHelper.poshITCustomPopupShadow(view: yesterdayBtn)
        yesterdayBtn.backgroundColor = UIColor.white
        todayBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        fromToBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        
        yesterdayBtn.layer.shadowOpacity = 1
        todayBtn.layer.shadowOpacity = 0
        fromToBtn.layer.shadowOpacity = 0
        fromToBtn.setImage(UIImage(named: "iconBagBlue"), for: .normal)
        let parameters = ["option": "dashboard","store_id": "\(UserDefaults.standard.string(forKey: "store_id")!)" , "store_gmt":"\(UserDefaults.standard.string(forKey: "store_gmt")!)","tStart":"","tEnd": ""]
        callDashboardApi(params: parameters)

    }
    @IBAction func fromToBtnAction(_ sender: Any) {
        selectedBtn = 3
        fromToBtn.setTitleColor(UIColor(red: 60/255, green: 67/255, blue: 94/255, alpha: 1), for: .normal)
        todayBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        yesterdayBtn.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: .normal)
        fromToDateLbl.isHidden = false
        AppHelper.poshITCustomPopupShadow(view: fromToBtn)
        fromToBtn.backgroundColor = UIColor.white
        todayBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        yesterdayBtn.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        yesterdayBtn.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
        todayBtn.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
        fromToBtn.layer.shadowOpacity = 1
        todayBtn.layer.shadowOpacity = 0
        yesterdayBtn.layer.shadowOpacity = 0
        fromToBtn.setImage(UIImage(named: "iconBagBlue"), for: .normal)
       
        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .year, value: -2, to: Date())
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        
        
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
        
       
        
    }
    
    func callDashboardApi(params :[String : String])  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
     //   AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kSellerAppURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                UserDetails.sharedInstance.userToken = result!["token"]?.string
                if statusCode == "200"
                {
                    if let response = result!["response"]?.dictionary{
                        
                        if self.selectedBtn == 1 {
                            if let ordersData = response["ordersData"]?.dictionary{
                                
                                if let dataToday = ordersData["today"]?.dictionary{
                                    let totalOrders = dataToday["total_orders"]?.string ?? "0"
                                    self.totalOrderPlaced.text = totalOrders
                                    let totalOrdersAmount = dataToday["total_order_amount"]?.string ?? "0"
                                    self.totalOrderAmount.text = UserDetails.sharedInstance.currenySymbol + "\(totalOrdersAmount)"
                                }
                            }
                            
                            if let packResponse = response["totalPacked"]?.dictionary{
                                print(packResponse)
                            //    self.totalPacked.text = "\(packResponse["today"]?.string ?? "0")"
                            }
                            
                            if let totalProduct = response["totalProducts"]?.dictionary{
                                print(totalProduct)
                                     self.totalProducts.text = "\(totalProduct["today"]?.int ?? 0)"
                            }
                            
                            if let totalDiscount = response["totalDiscount"]?.dictionary{
                                print(totalDiscount)
                                self.totalDiscountOffered.text = UserDetails.sharedInstance.currenySymbol +  "\(totalDiscount["today"]?.string ?? "0")"
                            }
                            
                            if let payOutResponse = response["totalPayOuts"]?.dictionary{
                                print(payOutResponse)
                                   self.totalPayout.text = UserDetails.sharedInstance.currenySymbol +  "\(payOutResponse["today"]?.string ?? "0")"
                            }
                            
                            if let totalRefunds = response["totalRefunds"]?.dictionary{
                                print(totalRefunds)
                                   self.totalOrderRefunded.text = UserDetails.sharedInstance.currenySymbol +  "\(totalRefunds["today"]?.string ?? "0")"
                            }
                            
                            if let shippedResponse = response["totalShipped"]?.dictionary{
                           //     self.totalShipped.text = "\(shippedResponse["today"]?.string ?? "0")"
                            }
                            if let approvedResponse = response["totalApproved"]?.dictionary{
                            //    self.totalApproved.text = "\(approvedResponse["today"]?.string ?? "0")"
                            }
                            if let cancelledResponse = response["totalCancelled"]?.dictionary{
                                self.totalOrderCanceled.text = "\(cancelledResponse["today"]?.string ?? "0")"
                            }
                            if let deliveredResponse = response["totalDelivered"]?.dictionary{
                                self.totalOrderDelivered.text = "\(deliveredResponse["today"]?.string ?? "0")"
                            }
                            if let earningResponse = response["totalEarnings"]?.dictionary{
                                self.totalEarnings.text = UserDetails.sharedInstance.currenySymbol +  "\(earningResponse["today"]?.string ?? "0")"
                            }
                        }else if self.selectedBtn == 2{
                            if let ordersData = response["ordersData"]?.dictionary{
                                
                                if let dataToday = ordersData["all"]?.dictionary{
                                    let totalOrders = dataToday["total_orders"]?.string ?? "0"
                                    self.totalOrderPlaced.text = totalOrders
                                    let totalOrdersAmount = dataToday["total_order_amount"]?.string ?? "0"
                                    self.totalOrderAmount.text = UserDetails.sharedInstance.currenySymbol + "\(totalOrdersAmount)"
                                }
                            }
                            
                            if let packResponse = response["totalPacked"]?.dictionary{
                                print(packResponse)
                             //   self.totalPacked.text = "\(packResponse["yesterday"]?.string ?? "0")"
                            }
                            
                            if let totalProduct = response["totalProducts"]?.dictionary{
                                print(totalProduct)
                                self.totalProducts.text = "\(totalProduct["all"]?.int ?? 0)"
                            }
                            
                            if let totalDiscount = response["totalDiscount"]?.dictionary{
                                print(totalDiscount)
                                self.totalDiscountOffered.text = UserDetails.sharedInstance.currenySymbol +  "\(totalDiscount["all"]?.string ?? "0")"
                            }
                            
                            if let payOutResponse = response["totalPayOuts"]?.dictionary{
                                print(payOutResponse)
                                 self.totalPayout.text = UserDetails.sharedInstance.currenySymbol +  "\(payOutResponse["all"]?.string ?? "0")"
                            }
                            
                            if let totalRefunds = response["totalRefunds"]?.dictionary{
                                print(totalRefunds)
                                self.totalOrderRefunded.text = UserDetails.sharedInstance.currenySymbol +  "\(totalRefunds["all"]?.string ?? "0")"
                            }
                            
                            if let shippedResponse = response["totalShipped"]?.dictionary{
                            //    self.totalShipped.text = "\(shippedResponse["yesterday"]?.string ?? "0")"
                            }
                            if let approvedResponse = response["totalApproved"]?.dictionary{
                            //    self.totalApproved.text = "\(approvedResponse["yesterday"]?.string ?? "0")"
                            }
                            if let cancelledResponse = response["totalCancelled"]?.dictionary{
                                self.totalOrderCanceled.text = "\(cancelledResponse["all"]?.string ?? "0")"
                            }
                            if let deliveredResponse = response["totalDelivered"]?.dictionary{
                                self.totalOrderDelivered.text = "\(deliveredResponse["all"]?.string ?? "0")"
                            }
                            if let earningResponse = response["totalEarnings"]?.dictionary{
                                self.totalEarnings.text = UserDetails.sharedInstance.currenySymbol +  "\(earningResponse["all"]?.string ?? "0")"
                            }
                        }else{
                            if let ordersData = response["ordersData"]?.dictionary{
                                
                                if let dataToday = ordersData["interval"]?.dictionary{
                                    let totalOrders = dataToday["total_orders"]?.string ?? "0"
                               //     self.totalPending.text = totalOrders
                                    let totalOrdersAmount = dataToday["total_order_amount"]?.string ?? "0"
                                    self.totalOrderAmount.text = UserDetails.sharedInstance.currenySymbol + "\(totalOrdersAmount)"
                                }
                            }
                            
                            if let packResponse = response["totalPacked"]?.dictionary{
                                print(packResponse)
                            //    self.totalPacked.text = "\(packResponse["interval"]?.string ?? "0")"
                            }
                            
                            if let totalProduct = response["totalProducts"]?.dictionary{
                                print(totalProduct)
                                self.totalProducts.text = "\(totalProduct["interval"]?.int ?? 0)"
                            }
                            
                            if let totalDiscount = response["totalDiscount"]?.dictionary{
                                print(totalDiscount)
                                self.totalDiscountOffered.text = UserDetails.sharedInstance.currenySymbol +  "\(totalDiscount["interval"]?.string ?? "0")"
                            }
                            
                            if let payOutResponse = response["totalPayOuts"]?.dictionary{
                                print(payOutResponse)
                                 self.totalPayout.text = UserDetails.sharedInstance.currenySymbol +  "\(payOutResponse["interval"]?.string ?? "0")"
                            }
                            
                            if let totalRefunds = response["totalRefunds"]?.dictionary{
                                print(totalRefunds)
                                self.totalOrderRefunded.text = UserDetails.sharedInstance.currenySymbol +  "\(totalRefunds["interval"]?.string ?? "0")"
                            }
                            
                            if let shippedResponse = response["totalShipped"]?.dictionary{
                           //     self.totalShipped.text = "\(shippedResponse["interval"]?.string ?? "0")"
                            }
                            if let approvedResponse = response["totalApproved"]?.dictionary{
                            //    self.totalApproved.text = "\(approvedResponse["interval"]?.string ?? "0")"
                            }
                            if let cancelledResponse = response["totalCancelled"]?.dictionary{
                                self.totalOrderCanceled.text = "\(cancelledResponse["interval"]?.string ?? "0")"
                            }
                            if let deliveredResponse = response["totalDelivered"]?.dictionary{
                                self.totalOrderDelivered.text = "\(deliveredResponse["interval"]?.string ?? "0")"
                            }
                            if let earningResponse = response["totalEarnings"]?.dictionary{
                                self.totalEarnings.text = UserDetails.sharedInstance.currenySymbol +  "\(earningResponse["interval"]?.string ?? "0")"
                            }
                        }
                        
                        
                        
                        print(response)
                        AppHelper.sharedInstance.removeSpinner()
                    }
                }
                else{
                    if let errorMsg = result!["message"]?.string{
                        AppHelper.showAlertView(message: errorMsg)
                    }else{
                        AppHelper.showAlertView(message: "No Data Found")
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
            
        }
    }
    
    func didTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        self.dismiss(animated: true, completion: nil)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stDate = dateFormatter.string(from: startDate)
        let enDate = dateFormatter.string(from: endDate)
        let parameters = ["option": "dashboard","store_id": "\(UserDefaults.standard.string(forKey: "store_id")!)" , "store_gmt":"\(UserDefaults.standard.string(forKey: "store_gmt")!)","tStart":"\(stDate)","tEnd": "\(enDate)"]
        callDashboardApi(params: parameters)
        fromToDateLbl.text = "From: \(AppHelper.convertSpecifiedDateStringToFormattedDateString(date: stDate)) To: \(AppHelper.convertSpecifiedDateStringToFormattedDateString(date: enDate))"
        
    }

}
