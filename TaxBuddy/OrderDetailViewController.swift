//
//  OrderDetailViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 02/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//



import UIKit
import SwiftyJSON
import Alamofire
//import BraintreeDropIn
//import Braintree



class OrderDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate/*,CartTableViewCellDelegate */{
    
    
   
    @IBOutlet weak var tableOrderSummary: UITableView!
    
    var date = "Date               "
    var time = "Time               "
    var isTime = 0
    var productDetails: ProductDetails?
    var currentLatitude = ""
    var currentLongitude = ""
    var currentAddress = ""
    var deliveryPhoneNumber = ""
    var deliveryPhoneNumberTxtField: UITextField?
    var appartmentNumberTxtField: UITextField?
    var deliveryChargesArray = [JSON]()
    var isViewForBuyNow = false
    var isViewForReOrder = false
    var orderID = ""
    
    var shouldDeliverToMe = 0
    var orderDetails: OrderDetails?
    var ordersDetailsArray = [OrderDetails]()
    var productDetailsArray = [ProductDetails]()
    var promoCode = ""
    var promoAmount = "0"
    var orderAmountValue = ""
    var orderTransactionID = ""
    
    @IBOutlet weak var placeholderButton: UIButton!
    
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var errorMessageContainerViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var errorMessageContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        placeholderButton.isEnabled = false
        UserDetails.sharedInstance.apiHitInt = "21"
        currentLatitude = UserDetails.sharedInstance.latitude
        currentLongitude = UserDetails.sharedInstance.longitude
        currentAddress = UserDetails.sharedInstance.googleCoordinatesAddress
        deliveryPhoneNumber = format(phoneNumber: UserDetails.sharedInstance.phoneNumber, shouldRemoveLastDigit: false)
        if isViewForBuyNow {
            productDetailsArray.append(productDetails!)
        }
        
      //  placeholderButton.applyGradient(colours: [UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1),UIColor(red: 132/255, green: 199/255, blue: 158/255, alpha: 1)])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if currentLatitude == UserDetails.sharedInstance.latitude{
            return;
        }
        
        currentLatitude = UserDetails.sharedInstance.latitude
        currentLongitude = UserDetails.sharedInstance.longitude
        currentAddress = UserDetails.sharedInstance.googleCoordinatesAddress
       
        
    }
    
    func callShipmentsAPI() {
        if isViewForBuyNow {
            let params: Parameters = ["option": "ship_charges_product", "latitude": currentLatitude,"longitude": currentLongitude, "product_id": productDetails!.productInfo.productId, "quantities":  productDetails!.cartAddedQuantities, "drop_address": currentAddress]
            callDeliveryEstimateAPI(params: params)
        }
        else if isViewForReOrder {
            let params: Parameters = ["option": "ship_charges_previous_order", "latitude": currentLatitude,"longitude": currentLongitude, "order_id": orderID , "drop_address": currentAddress]
            callDeliveryEstimateAPI(params: params)
        }
        else{
            
            let params: Parameters = ["option": "ship_charges", "latitude": currentLatitude,"longitude": currentLongitude, "drop_address": currentAddress]
            callDeliveryEstimateAPI(params: params)
        }
        
        tableOrderSummary.reloadData()
    }
    // MARK:- IBAction Methods
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deliverMeAction(_ sender: Any) {
        date = "Date               "
        time = "Time               "
        shouldDeliverToMe = 1
        tableOrderSummary.reloadData()
        
    }
    @IBAction func pickStoreAction(_ sender: Any) {
        shouldDeliverToMe = 0
        tableOrderSummary.reloadData()
    }
    
    
    @IBAction func btnPhoneChangeTapped(_ sender: Any) {
        
    }
    
    @objc func cancelButtonTapped(button: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPlaceOrderTapped(_ sender: Any) {
        
        let phoneNumber = deliveryPhoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let appartment = appartmentNumberTxtField?.text;
        
        if isViewForBuyNow{
            let params: Parameters = ["option": "create_order_product", "latitude": currentLatitude,"longitude": currentLongitude, "product_id": productDetails!.productInfo.productId, "quantities":  productDetails!.cartAddedQuantities, "drop_address": currentAddress, "promocode": promoCode, "phone": phoneNumber, "appartment": appartment ?? ""]
            callCreateOrderAPI(params: params)
        }
        else if isViewForReOrder{
            let params: Parameters = ["option": "create_previous_order", "latitude": currentLatitude,"longitude": currentLongitude, "order_id": orderID, "drop_address": currentAddress, "promocode": promoCode, "phone": phoneNumber, "appartment": appartment ?? ""]
            callCreateOrderAPI(params: params)
        }
        else{
            
            let params: Parameters = ["option": "create_order_cart", "latitude": currentLatitude,"longitude": currentLongitude, "drop_address": currentAddress, "promocode": promoCode, "phone": phoneNumber, "appartment": appartment ?? ""]
            callCreateOrderAPI(params: params)
        }
    }
    
    @IBAction func netBankingTapped(_ sender: Any) {
        
    }
    
    @IBAction func removePromoCodeButtonTapped(_ sender: Any) {
        promoCode = ""
        promoAmount = "0"
        tableOrderSummary.reloadData()
    }
    
    @IBAction func creditCardTapped(_ sender: Any) {
        
    }
    
    @IBAction func debitCardTapped(_ sender: Any) {
        
    }
    
    @IBAction func cashOnDeliveryTapped(_ sender: Any) {
        
    }
    
    @IBAction func btnEditAddressTapped(_ sender: Any) {
       
    }
    
    /*
     @IBAction func promoCodeButtonTapped(_ sender: Any) {
     
     let promoCodeView = PromotionCodeView(frame: APPDELEGATE.window!.frame)
     promoCodeView.isViewForReorder = isViewForReOrder
     promoCodeView.orderID = orderID;
     promoCodeView.delegate = self
     if isViewForBuyNow{
     promoCodeView.productDetails = productDetails
     }
     
     APPDELEGATE.window?.addSubview(promoCodeView)
     }
     */
    
    func promoCodeApplySuccessfully(promoCode: String, promoCodeAmount: String) {
        self.promoCode = promoCode
        self.promoAmount = promoCodeAmount
        self.tableOrderSummary.reloadData()
    }
    
    
    //MARK:- TableView Data source and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 100 {
            return productDetailsArray.count;
        }
        return productDetailsArray.count + 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100 {
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "InnerCellTableViewCell") as? InnerCellTableViewCell
            
            cell?.selectionStyle = .none
            
            let productDetails = productDetailsArray[indexPath.row]
            
            cell?.lblProductName.text = productDetails.productName
            let totalPrice = Double(productDetails.productInfo.offerPrice)! *  Double(productDetails.cartAddedQuantities)!
            
            cell?.lblPriceDetail.text = UserDetails.sharedInstance.currenySymbol + String(totalPrice)
            
            
            if Int(productDetails.cartAddedQuantities)! > 1{
                cell?.lblNumberOfUnit.text = UserDetails.sharedInstance.currenySymbol + productDetails.productInfo.offerPrice+" * "+productDetails.cartAddedQuantities + " Units"
            }
            else{
                cell?.lblNumberOfUnit.text = UserDetails.sharedInstance.currenySymbol + productDetails.productInfo.offerPrice+" * "+productDetails.cartAddedQuantities + " Unit"
            }
            
            return cell!;
        }
        else if(indexPath.row == productDetailsArray.count){
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell") as? AddressTableViewCell
         
            cell?.selectionStyle = .none
            if orderDetails?.delivery_type == "1"{
                shouldDeliverToMe = 1
            }else{
                shouldDeliverToMe = 0
            }
            if shouldDeliverToMe == 0{
                cell?.dateBtn.isHidden = false
                cell?.timeBtn.isHidden = false
                cell?.dateBtn.setTitle(date, for: .normal)
                cell?.timeBtn.setTitle(time, for: .normal)
                AppHelper.poshITCustomPopupShadow(view: (cell?.pickAtStore)!)
                cell!.pickAtStore.backgroundColor = UIColor.white
                cell?.pickAtStore.setImage(UIImage(named: "iconBagBlue"), for: .normal)
                cell?.delieverMe.setImage(UIImage(named: "iconPinGrey"), for: .normal)
                cell?.delieverMe.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
                cell?.delieverMe.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
                cell?.pickAtStore.layer.shadowOpacity = 1
                cell?.delieverMe.layer.shadowOpacity = 0
                
                
            }else{
                cell?.lblAddress.text = orderDetails?.dropAddress
                cell?.cityLbl.text = orderDetails?.drop_city
                cell?.stateLbl.text = orderDetails?.drop_state
                cell?.zipCodeLbl.text = orderDetails?.drop_zip
                cell?.mobileLbl.text = orderDetails?.dropPhone
                cell?.lblAddress.isEditable = false
                cell?.cityLbl.isEnabled = false
                cell?.stateLbl.isEnabled = false
                cell?.zipCodeLbl.isEnabled = false
                cell?.mobileLbl.isEnabled = false
                cell?.dateBtn.isHidden = true
                cell?.timeBtn.isHidden = true
                AppHelper.poshITCustomPopupShadow(view: (cell?.delieverMe)!)
                cell!.delieverMe.backgroundColor = UIColor.white
                cell?.delieverMe.setImage(UIImage(named: "iconPin"), for: .normal)
                
                cell?.pickAtStore.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
                cell?.pickAtStore.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
                cell?.pickAtStore.setImage(UIImage(named: "iconBag"), for: .normal)
                cell?.pickAtStore.layer.shadowOpacity = 0
                cell?.delieverMe.layer.shadowOpacity = 1
                
            }
            //    cell?.txtPhone.text = deliveryPhoneNumber
            //   cell?.viewPhone.isHidden = true;
            
            //  cell?.lblAddress.text = currentAddress
            // cell?.txtPhone.delegate = self;
            //  cell?.appartmentNumber.delegate = self;
            
            //   cell?.btnPhoneChange.tag = indexPath.row
            // deliveryPhoneNumberTxtField = cell?.txtPhone;
            //   appartmentNumberTxtField = cell?.appartmentNumber;
            //   cell?.btnPhoneChange.addTarget(self,action:#selector(buttonClicked(sender:)), for: .touchUpInside)
            
            return cell!;
        }
        else if(indexPath.row == productDetailsArray.count + 1){
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "PriceDetailTableViewCell") as? PriceDetailTableViewCell

            cell?.selectionStyle = .none
            
            
            cell?.tblViewHeightConstraint.constant = CGFloat(productDetailsArray.count * 65)
            
            
            var totalProductsPrice = 0.0
            cell?.lblPromoCode.text = "(" + promoCode + ")"
            
            cell?.lblPromoCodeAmt.text = UserDetails.sharedInstance.currenySymbol + promoAmount
            
            if promoCode.count != 0{
                cell?.lblPromoCodeAmt.isHidden = false
                cell?.lblPromoCode.isHidden = false
                cell?.lblPromoCodeTitle.isHidden = false
                cell?.removePromoCodeButton.isHidden = false
            }
            else{
                cell?.lblPromoCodeAmt.isHidden = true
                cell?.lblPromoCode.isHidden = true
                cell?.lblPromoCodeTitle.isHidden = true
                cell?.removePromoCodeButton.isHidden = true
            }
            
            for productDetails in productDetailsArray{
                totalProductsPrice = totalProductsPrice + Double(productDetails.productInfo.offerPrice)! * Double(productDetails.cartAddedQuantities)!
            }
            
            
            cell?.lblDeliveryCharges.text = UserDetails.sharedInstance.currenySymbol + "0"
            
            var totalDeliveryCharges = 0.0
            var totalConvenienceFee = 0.0
            
            for deliveryChargesDetails in deliveryChargesArray{
                
                let deliveryChargesStr = deliveryChargesDetails["delivery_fee"].string ?? "0"
                
                
                let deliveryCharges = Double(deliveryChargesStr)!/100
                
                totalDeliveryCharges = totalDeliveryCharges + deliveryCharges
                
            }
            
            
            
            
            cell?.lblDeliveryCharges.text = UserDetails.sharedInstance.currenySymbol + String(totalDeliveryCharges)
            //  cell?.lblConvenenceFee.text = UserDetails.sharedInstance.currenySymbol +  String(totalConvenienceFee)
            let promoCodeAmt = Double(promoAmount)!
            
            if promoCodeAmt > totalProductsPrice{
                let total = totalDeliveryCharges + totalConvenienceFee
                cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", total)
                
            }
            else{
                if shouldDeliverToMe == 0{
                    let total = totalProductsPrice  /*- promoCodeAmt */
                    cell?.lblDeliveryCharges.isHidden = true
                    cell?.deliveryChargeText.isHidden = true
                    
                    cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", total)
                }else{
                    let total = totalProductsPrice + totalDeliveryCharges  /*- promoCodeAmt */
                    cell?.lblDeliveryCharges.isHidden = false
                    cell?.deliveryChargeText.isHidden = false
                    cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", total)
                }
                
            }
            cell?.layoutIfNeeded()
            return cell!
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell2") as? OrderTableViewCell2
            

            
            cell?.selectionStyle = .none
            let productDetails = productDetailsArray[indexPath.row]
            cell?.configData(productDetails: productDetails)
            
            
            for deliverInfo in deliveryChargesArray{
                
                if productDetails.storeID == deliverInfo["store_id"].string{
                    
                    break;
                }
            }
            return cell!;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView.tag == 100{
            return 65.0
        }
        else{
            if(indexPath.row == productDetailsArray.count){
                if shouldDeliverToMe == 0{
                    return 131
                }else{
                    return 305
                }
                
            }
            else if(indexPath.row == productDetailsArray.count + 1){
                if promoCode.count == 0{
                    if shouldDeliverToMe == 0{
                        return CGFloat(115.0 + Double(productDetailsArray.count * 65));
                    }else{
                        return CGFloat(135.0 + Double(productDetailsArray.count * 65));
                    }
                    
                }
                return CGFloat(220.0 + Double(productDetailsArray.count * 65))
            }
            else {
                return 151.0
                // return 200
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
         if (tableView.tag != 100) && (indexPath.row < productDetailsArray.count){
         let productDetails = productDetailsArray[indexPath.row]
         let productDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as?
         ProductDetailViewController
         productDetailVC?.productId = productDetails.productId
         self.navigationController?.pushViewController(productDetailVC!, animated: true)
         }
         */
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if deliveryPhoneNumberTxtField == textField {
            deliveryPhoneNumber = textField.text ?? "";
            tableOrderSummary.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == deliveryPhoneNumberTxtField {
            var fullString = textField.text ?? ""
            fullString.append(string)
            if range.length == 1 {
                textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: true)
            } else {
                textField.text = format(phoneNumber: fullString)
            }
            return false
        }
        return true
    }
    
    func format(phoneNumber: String, shouldRemoveLastDigit: Bool = false) -> String {
        guard !phoneNumber.isEmpty else { return "" }
        guard let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive) else { return "" }
        let r = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: .init(rawValue: 0), range: r, withTemplate: "")
        
        if number.count > 10 {
            let tenthDigitIndex = number.index(number.startIndex, offsetBy: 10)
            number = String(number[number.startIndex..<tenthDigitIndex])
        }
        
        if shouldRemoveLastDigit {
            let end = number.index(number.startIndex, offsetBy: number.count-1)
            number = String(number[number.startIndex..<end])
        }
        
        if number.count < 7 {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d+)", with: "($1) $2", options: .regularExpression, range: range)
            
        } else {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: range)
        }
        
        return number
    }
    
    //MARK:- API Related Methods
    
    func callDeliveryEstimateAPI(params: Parameters)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kProductsURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            self.deliveryChargesArray.removeAll()
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if let responseDict  = result!["response"]?.dictionary{
                        
                        if let cartCount = responseDict["total_cart"]?.string{
                            UserDetails.sharedInstance.cartCount = cartCount
                        }
                        
                        if (responseDict["total_cart"]?.string) != nil
                            
                        {
                            UserDetails.sharedInstance.cartCount = (responseDict["total_cart"]?.string)!
                        }
                        
                        if let shipChargesDict  = responseDict["ship_charges"]?.dictionary{
                            
                            let deliveryCharges = shipChargesDict["fee"]?.stringValue ?? "0"
                            var cartStoreID = self.productDetails?.storeID ?? "0"
                            if self.isViewForReOrder{
                                if self.productDetailsArray.count > 0{
                                    let details = self.productDetailsArray[0];
                                    cartStoreID = details.storeID
                                }
                            }
                            let convenienceFee = responseDict["convenience_fee"]?.stringValue ?? "0"
                            let dropEta = shipChargesDict["drop_eta"]?.stringValue ?? "0"
                            
                            let devliveryDetails: JSON = ["delivery_fee": deliveryCharges, "store_id":  cartStoreID, "convenience_fee": convenienceFee,"drop_eta": dropEta]
                            self.deliveryChargesArray.append(devliveryDetails)
                        }
                        else if let shipChargesArray  = responseDict["shippings"]?.array{
                            
                            for shipChargesDict in shipChargesArray{
                                
                                let deliveryCharges = shipChargesDict["fee"].stringValue
                                let cartStoreID = shipChargesDict["store_id"].stringValue
                                let convenienceFee = responseDict["convenience_fee"]?.string ?? "0"
                                let dropEta = shipChargesDict["drop_eta"].stringValue
                                let devliveryDetails: JSON = ["delivery_fee": deliveryCharges, "store_id":  cartStoreID, "convenience_fee": convenienceFee,"drop_eta": dropEta]
                                
                                self.deliveryChargesArray.append(devliveryDetails)
                            }
                        }
                    }
                    else if let responseDictArray  = result!["response"]?.array{
                        
                        for responseDict in responseDictArray{
                            if let shipChargesDict  = responseDict["ship_charges"].dictionary{
                                
                                let deliveryCharges = shipChargesDict["fee"]?.string ?? "0"
                                let cartStoreID = shipChargesDict["store_id"]?.string ?? "0"
                                let convenienceFee = responseDict["convenience_fee"].string ?? "0"
                                let dropEta = shipChargesDict["drop_eta"]?.stringValue ?? "0"
                                let devliveryDetails: JSON = ["delivery_fee": deliveryCharges, "store_id":  cartStoreID, "convenience_fee": convenienceFee,"drop_eta": dropEta]
                                self.deliveryChargesArray.append(devliveryDetails)
                            }
                        }
                    }
                    self.placeholderButton.isEnabled = true
                    self.lblErrorMessage.text = ""
                    self.errorMessageContainerView.isHidden = false
                    self.errorMessageContainerView.isHidden = true
                    
                    
                    self.tableOrderSummary.reloadData()
                }
                else{
                    
                    let errorMsg = result!["message"]?.string
                    
                   
                    self.placeholderButton.isEnabled = false
                   
                }
            }
            else{
                
                self.placeholderButton.isEnabled = false
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            self.tableOrderSummary.reloadData()
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    func callCreateOrderAPI(params: Parameters)  {
        
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if let response = result!["response"]?.dictionary{
                        self.orderAmountValue = response["order_price"]?.string ?? ""
                        self.orderTransactionID = response["txn_id"]?.string ?? ""
                        UserDefaults.standard.set("final", forKey: "order")
                        UserDefaults.standard.synchronize()
                        let params: Parameters = ["option": "place_order", "amount": self.orderAmountValue, "nonce": "nonce", "txn_id": self.orderTransactionID]
                        self.updatePlaceorderToServer(params: params)
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string ?? kErrorMsg
                    AppHelper.showAlertView(message: errorMsg)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    func updatePlaceorderToServer(params: Parameters) {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner(loaderMessage: kPaymentMsg)
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    let alertController = UIAlertController(title: "Payment Successful. Your order has been placed.", message: nil, preferredStyle: .alert)
                    
                    let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        let oderConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationViewController")
                        self.navigationController?.pushViewController(oderConfirmVC!, animated: true)
                    }
                    
                    alertController.addAction(action1)
                    AppHelper.sharedInstance.removeSpinner()
                    self.present(alertController, animated: true, completion: nil)
                }
                else{
                    
                    let errorMsg = result!["message"]?.string ?? kErrorMsg
                    AppHelper.showAlertView(message: errorMsg)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    /*
     //CartCellDelegate
     func btnDecrementTapped(index:Int) {
     let cartDetails = productDetailsArray[index]
     
     if(Int(cartDetails.quantities)! > 1){
     let quantitiyCount = Int(cartDetails.quantities)! - 1;
     cartDetails.quantities = String(quantitiyCount)
     }
     else{
     return;
     }
     
     tableOrderSummary.reloadData()
     
     DispatchQueue.global().async {
     let parameters: Parameters = ["option": "add_to_cart","product_id":cartDetails.productId,"quantities":cartDetails.quantities]
     self.callAddToCartApi(params: parameters)
     }
     
     }
     
     func btnIncrementTapped(index:Int) {
     
     let cartDetails = productDetailsArray[index]
     
     
     if (Int(cartDetails.productDetails!.productInfo.quantities)! <= Int(cartDetails.quantities)!) {
     return
     }
     
     if(Int(cartDetails.quantities)! < maxLimitForAddtoCart){
     let quantitiyCount = Int(cartDetails.quantities)! + 1;
     cartDetails.quantities = String(quantitiyCount)
     }
     else{
     return;
     }
     
     tableOrderSummary.reloadData()
     DispatchQueue.global().async {
     let parameters: Parameters = ["option": "add_to_cart","product_id":cartDetails.productId,"quantities":cartDetails.quantities]
     self.callAddToCartApi(params: parameters)
     }
     
     }
     
     func cancelAction(index: Int)  {
     
     let cartDetails = productArray[index]
     let params: Parameters = ["option": "remove_from_cart","product_id": cartDetails.productId]
     
     if !AppHelper.isInterNetConnectionAvailable(){
     AppHelper.showAlertView(message: kNoInternetConnection)
     return
     }
     //     AppHelper.sharedInstance.displaySpinner()
     
     WebServiceHandler.performPOSTRequest(withURL: kProductsURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
     
     if (result != nil){
     let statusCode = result!["status"]?.string
     if statusCode == "200"
     {
     if  let dataDict = result!["response"]?.dictionary{
     
     UserDetails.sharedInstance.cartCount = (dataDict["total_cart"]?.string)!
     }
     
     if index < self.productArray.count{
     self.productArray.remove(at: index)
     
     if self.productArray.count != 0{
     //                            self.lblMycart.text = "My Cart (" + String(self.productArray.count) + " items )"
     //                     self.tblContainerView.isHidden = false
     }
     else{
     //       self.lblMycart.text = "My Cart"
     //         self.lblNoDataFound.isHidden = false
     //       self.tblContainerView.isHidden = true
     }
     }
     self.tableOrderSummary.reloadData()
     }
     else{
     let errorMsg = result!["message"]?.string
     AppHelper.showAlertView(message: errorMsg!)
     }
     }
     else{
     AppHelper.showAlertView(message: kErrorMsg)
     }
     //         AppHelper.sharedInstance.removeSpinner()
     }
     }
     
     func callAddToCartApi(params :Parameters)  {
     
     WebServiceHandler.performPOSTRequest(withURL: kProductsURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
     }
     }
     */
    
    
   
    @objc func buttonClicked(sender:UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if   let cell = tableOrderSummary.cellForRow(at: indexPath) as? AddressTableViewCell {
            
            cell.txtPhone.isEnabled = true;
            cell.viewPhone.isHidden = false;
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


// MARK: - UINavigationControllerDelegate

extension OrderDetailViewController: UINavigationControllerDelegate {
    
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return .portrait
    }
}



