//
//  AppHelper.swift
//
//  Created by Vikash Rajput on 3/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import Reachability

class AppHelper: NSObject {

    var spinnerView: UIView?
    static let sharedInstance = AppHelper()
    
    class func resetDefaults() {
        AppHelper.getGuestToken()
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if (key != "isUserLoggedIn") && (key != "isUserLoggedInFinal"){
                defaults.removeObject(forKey: key)
            }
        }

        UserDetails.sharedInstance.userName = ""
        UserDetails.sharedInstance.userID = ""
        UserDetails.sharedInstance.phoneNumber = ""
        UserDetails.sharedInstance.userImage = ""
        UserDetails.sharedInstance.userCountry = ""
        UserDetails.sharedInstance.userZipCode = ""
        UserDetails.sharedInstance.userCity = ""
        UserDetails.sharedInstance.userEmail = ""
        UserDetails.sharedInstance.userAddress = ""
        UserDetails.sharedInstance.apiSecretKey = ""
        UserDetails.sharedInstance.accessToken = ""
        UserDetails.sharedInstance.cartCount = "0"
        UserDetails.sharedInstance.notificaionCount = "0"
    }
    
    /*
    class func showLoginScreenAspopup(complationBlock: @escaping (_ sucess: Bool) -> Void){
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVC.isScreenForLogin = true;
            loginVC.isNeedToShowCrossButton = true
            loginVC.userToLoginScreenSuccessfull { (status) in
                complationBlock(status)
            }
            navVC.present(loginVC, animated: true, completion: nil)
        }
    }

    
    class func sendUserToLoginScreen(complationBlock: @escaping (_ sucess: Bool) -> Void){
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVC.isScreenForLogin = true;
            loginVC.userToLoginScreenSuccessfull { (status) in
                complationBlock(status)
            }
            navVC.pushViewController(loginVC, animated: true)
        }
    }
    
    */
   class func showShadowOnView(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: -1, height: 1)
        view.layer.shadowRadius = 2
    }
    
    class func poshITCustomPopupShadow(view: UIView)  {
        // Shadow and Radius
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        
        view.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        
        view.layer.shadowOpacity = 3.0
        view.layer.shadowRadius = 3.0
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 6.0
    }
    
    
    class func saveUserDetails(){
        if UserDetails.sharedInstance.userID.count > 0{
            UserDefaults.standard.set(UserDetails.sharedInstance.userID, forKey: "userID")
        }

        UserDefaults.standard.set(UserDetails.sharedInstance.apiSecretKey, forKey: "apiSecretKey")
        UserDefaults.standard.set(UserDetails.sharedInstance.accessToken, forKey: "accessToken")
        UserDefaults.standard.set(UserDetails.sharedInstance.phoneNumber, forKey: "phoneNumber")
        UserDefaults.standard.set(UserDetails.sharedInstance.userName, forKey: "name")
        UserDefaults.standard.set(UserDetails.sharedInstance.userEmail, forKey: "email")
        UserDefaults.standard.set(UserDetails.sharedInstance.userAddress, forKey: "userAddress")
        UserDefaults.standard.set(UserDetails.sharedInstance.userCity, forKey: "userCity")
        UserDefaults.standard.set(UserDetails.sharedInstance.userCountry, forKey: "userCountry")
        UserDefaults.standard.set(UserDetails.sharedInstance.userZipCode, forKey: "userZipCode")
        UserDefaults.standard.set(UserDetails.sharedInstance.userAddress1, forKey: "userAddress1")
        UserDefaults.standard.set(UserDetails.sharedInstance.userImage, forKey: "userImage")
    }
    
    class func saveStoreDetails(){
        if UserDetails.sharedInstance.userID.count > 0{
            UserDefaults.standard.set(UserDetails.sharedInstance.userID, forKey: "userID")
        }

        UserDefaults.standard.set(StoreDetail.sharedInstance.in_service, forKey: "in_service")
        
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_username, forKey: "store_username")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_email, forKey: "store_email")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_address, forKey: "store_address")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_city, forKey: "store_city")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_country, forKey: "store_country")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_pincode, forKey: "store_pincode")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_image, forKey: "store_image")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_name, forKey: "store_name")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_phone, forKey: "store_phone")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_open_time, forKey: "store_open_time")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_close_time, forKey: "store_close_time")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_gmt, forKey: "store_gmt")
        UserDefaults.standard.set(StoreDetail.sharedInstance.store_id, forKey: "store_id")
    }
    
    class func format(phoneNumber: String, shouldRemoveLastDigit: Bool = false) -> String {
        guard !phoneNumber.isEmpty else { return "" }
        guard let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive) else { return "" }
        let r = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: .init(rawValue: 0), range: r, withTemplate: "")
        
        if number.count > 10 {
            let tenthDigitIndex = number.index(number.startIndex, offsetBy: 10)
            number = String(number[number.startIndex..<tenthDigitIndex])
        }
        
        if shouldRemoveLastDigit {
            let end = number.index(number.startIndex, offsetBy: number.count-1)
            number = String(number[number.startIndex..<end])
        }
        
        if number.count < 7 {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d+)", with: "($1) $2", options: .regularExpression, range: range)
            
        } else {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: range)
        }
        
        return number
    }
    
    class func getUserDetails(){
        let userdefaults = UserDefaults.standard
        
        if let userID = userdefaults.value(forKey: "userID"){
            UserDetails.sharedInstance.userID = userID as! String
        }

        if let apiSecretKey = userdefaults.value(forKey: "apiSecretKey"){
            UserDetails.sharedInstance.apiSecretKey = apiSecretKey as! String
        }
        
        if let accessToken = userdefaults.value(forKey: "accessToken"){
            UserDetails.sharedInstance.accessToken = accessToken as! String
        }

        if let phoneNumber = userdefaults.value(forKey: "phoneNumber"){
            UserDetails.sharedInstance.phoneNumber = phoneNumber as! String
        }
        
        if let name = userdefaults.value(forKey: "name"){
            UserDetails.sharedInstance.userName = name as? String ?? ""
        }
        
        if let address = userdefaults.value(forKey: "userAddress"){
            UserDetails.sharedInstance.userAddress = address as? String ?? ""
        }
        if let address1 = userdefaults.value(forKey: "userAddress1"){
            UserDetails.sharedInstance.userAddress1 = address1 as? String ?? ""
        }
        if let city = userdefaults.value(forKey: "userCity"){
            UserDetails.sharedInstance.userCity = city as? String ?? ""
        }
        if let name = userdefaults.value(forKey: "userZipCode"){
            UserDetails.sharedInstance.userZipCode = name as? String ?? ""
        }

        if let email = userdefaults.value(forKey: "email"){
            UserDetails.sharedInstance.userEmail = email as? String ?? ""
        }
        
        if let userImage = userdefaults.value(forKey: "userImage"){
            UserDetails.sharedInstance.userImage = userImage as? String ?? ""
        }

        if let country = userdefaults.value(forKey: "userCountry"){
            UserDetails.sharedInstance.userCountry = country as? String ?? ""
        }

    }
    
    class func getStoreDetails(){
        let userdefaults = UserDefaults.standard
        
        if let storeId = userdefaults.value(forKey: "store_id"){
            StoreDetail.sharedInstance.store_id = storeId as! String
        }
        
        
        if let in_service = userdefaults.value(forKey: "in_service"){
            StoreDetail.sharedInstance.in_service = in_service as! String
        }
        
        if let store_gmt = userdefaults.value(forKey: "store_gmt"){
            StoreDetail.sharedInstance.store_gmt = store_gmt as! String
        }
        
        if let store_email = userdefaults.value(forKey: "store_email"){
            StoreDetail.sharedInstance.store_email = store_email as! String
        }
        
        if let store_address = userdefaults.value(forKey: "store_address"){
            StoreDetail.sharedInstance.store_address = store_address as! String
        }
        
        if let store_username = userdefaults.value(forKey: "store_username"){
            StoreDetail.sharedInstance.store_username = store_username as? String ?? ""
        }
        
        if let store_open_time = userdefaults.value(forKey: "store_open_time"){
            StoreDetail.sharedInstance.store_open_time = store_open_time as? String ?? ""
        }
        if let store_close_time = userdefaults.value(forKey: "store_close_time"){
            StoreDetail.sharedInstance.store_close_time = store_close_time as? String ?? ""
        }
        if let store_phone = userdefaults.value(forKey: "store_phone"){
            StoreDetail.sharedInstance.store_phone = store_phone as? String ?? ""
        }
        if let store_image = userdefaults.value(forKey: "store_image"){
            StoreDetail.sharedInstance.store_image = store_image as? String ?? ""
        }
        
        if let store_name = userdefaults.value(forKey: "store_name"){
            StoreDetail.sharedInstance.store_name = store_name as? String ?? ""
        }
        
        if let store_city = userdefaults.value(forKey: "store_city"){
            StoreDetail.sharedInstance.store_city = store_city as? String ?? ""
        }
        
        if let store_country = userdefaults.value(forKey: "store_country"){
            StoreDetail.sharedInstance.store_country = store_country as? String ?? ""
        }
        
        if let store_pincode = userdefaults.value(forKey: "store_pincode"){
            StoreDetail.sharedInstance.store_pincode = store_pincode as? String ?? ""
        }
        
    }
    
    class func getGuestToken() {
       
        let parameters: Parameters = ["option": "generate","type": "guest"]

        WebServiceHandler.performPOSTRequest(withURL: kToakenURL, andParameters: parameters, andAcessToken: "") { (result, error) in
           
            if result != nil{
                let guestToken = result!["response"]!["access_token"].string
                UserDetails.sharedInstance.accessToken = guestToken ?? "";
                AppHelper.saveUserDetails()
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
        }
    }
    
    class func callGenrateUserAccessTokenAPIWithLoader(isLoader: Bool,  completionBlock: @escaping () -> Void) {
        
        let parameters: Parameters = ["option": "generate", "type": "store_users", "username": UserDetails.sharedInstance.userID,"password":UserDetails.sharedInstance.apiSecretKey]
        
        WebServiceHandler.performPOSTRequest(withURL: kToakenURL, andParameters: parameters, andAcessToken: "") { (result, error) in
            
            if error == nil{
                let guestToken = result!["response"]!["access_token"].string
                UserDetails.sharedInstance.accessToken = guestToken ?? "";
                AppHelper.saveUserDetails()
                completionBlock()
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
        }
    }
    
    class func isNotValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return !emailTest.evaluate(with: email)
    }

    class func showAlertView(message: String)  {
        let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    class func presentAlertView(message: String)  {
        let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }

    class func formattedTimeFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "HH:mm"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return ""
    }
    
    class func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MM/dd/yyyy"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return ""
    }
    
    class func convertDateStringToFormattedDateString(date: String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: date){
            dateFormatter.dateFormat = "MM/dd/yyyy"
            return  dateFormatter.string(from: date)
        }
        
        return ""
    }
    
    class func convertSpecifiedDateStringToFormattedDateString(date: String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: date){
            dateFormatter.dateFormat = "MM/dd/yyyy"
            return  dateFormatter.string(from: date)
        }
        
        return ""
    }
    
    
    class func convertDateStringToSendDateString(date: String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: date){
            dateFormatter.dateFormat = "YYYY/MM/dd"
            return  dateFormatter.string(from: date)
        }
        
        return ""
    }
    
    
    class func convertLiveTrackingEspimatedDateStringToFormattedDateString(date: String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z"
        if let date = dateFormatter.date(from: date){
            dateFormatter.dateFormat = "dd MMM yyyy HH:mm a"
            return  dateFormatter.string(from: date)
        }
        
        return "N/A"
    }

   
    
    class func convertDateStringToFormattedTimeString(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        if dt == nil {
            return ""
        }
        return dateFormatter.string(from: dt!)
        
    }
    
    class func makeHomeViewControllerAsRootViewController() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let navigationVC = UINavigationController(rootViewController: homeVC)
        navigationVC.navigationBar.isHidden = true
        APPDELEGATE.window?.rootViewController = navigationVC

    }
    
    class func makeTutorialsViewControllerAsRootViewController() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let tutVC = storyboard.instantiateViewController(withIdentifier: "TutorialsViewController")
        let navigationVC = UINavigationController(rootViewController: tutVC)
        navigationVC.navigationBar.isHidden = true
        APPDELEGATE.window?.rootViewController = navigationVC
        
    }
    
    class func makeStarterViewControllerAsRootViewController() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let startVC = storyboard.instantiateViewController(withIdentifier: "StarterViewController")
        let navigationVC = UINavigationController(rootViewController: startVC)
        navigationVC.navigationBar.isHidden = true
        APPDELEGATE.window?.rootViewController = navigationVC
        
    }

    
    func displaySpinner() {
        if (spinnerView != nil) {
            removeSpinner()
        }
        
        spinnerView = UIView.init(frame: UIScreen.main.bounds)
//        spinnerView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.7)
        spinnerView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)

        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = (spinnerView?.center)!
        ai.color = UIColor.white
//        ai.color = UIColor(red: 250.0/255, green: 99.0/255, blue: 138.0/255, alpha: 1)
        DispatchQueue.main.async {
            self.spinnerView?.addSubview(ai)
            APPDELEGATE.window?.addSubview(self.spinnerView!)
        }
    }
    
    func displaySpinner(loaderMessage: String) {
        if (spinnerView != nil) {
            removeSpinner()
        }
        
        spinnerView = UIView.init(frame: UIScreen.main.bounds)
        spinnerView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.7)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = (spinnerView?.center)!
        ai.color = UIColor(red: 250.0/255, green: 99.0/255, blue: 138.0/255, alpha: 1)
        let label = UILabel(frame: CGRect(x: 20, y: ai.center.y + 20, width: UIScreen.main.bounds.width - 40, height: 40))
        label.textAlignment = .center
        label.text = loaderMessage
//        label.text = "Processing payment... please wait."
        label.textColor = UIColor(red: 187.0/255, green: 51.0/255, blue: 89.0/255, alpha: 1)
        label.font = UIFont(name: "MuseoSansCyrl-700", size: 15)
        spinnerView?.addSubview(label)
        DispatchQueue.main.async {
            self.spinnerView?.addSubview(ai)
            APPDELEGATE.window?.addSubview(self.spinnerView!)
        }
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.spinnerView?.removeFromSuperview()
        }
    }
    
    class func isInterNetConnectionAvailable() -> Bool {
        
        let reachability = Reachability()!.connection
        
        if reachability == .cellular {
            print("Reachable via WiFi")
            return true
        }
        if reachability == .wifi {
            print("Reachable via WiFi")
            return true
        }else {
            print("Reachable via None")
            AppHelper.showAlertView(message: kNoInternetConnection)
            return false
        }
    }

}


extension UIColor {
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}


extension UIImageView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}


extension UILabel {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UIView {
    func roundViewCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
}
