//
//  StoreDetails.swift
//  Posh_IT
//
//  Created by Vikash Rajput on 18/12/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class StoreDetails: NSObject {
    
   
    var storeID = ""
    var storeDistance = ""
    var storeName = ""
    var isSelected = false
    
    class func getAllStores(dataArray: [JSON]) -> [StoreDetails]{
        
        var storeArray = Array<StoreDetails>()
        for details in dataArray {
            let storeDetails = StoreDetails()
            storeDetails.storeID = details["store_id"].string ?? ""
            storeDetails.storeName = details["store_name"].string ?? ""
            storeDetails.storeDistance = details["store_distance"].string ?? ""
            storeArray.append(storeDetails)
        }
        
        return storeArray
    }
    

}
