//
//  OrderDetails.swift
//  Posh_IT
//
//  Created by MADSTECH on 04/09/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderDetails: NSObject {
    
    var order_id  = ""
    var user_id  = ""
    var store_id  = ""
    var total_products  = ""
    
    var dropLatitude  = ""
    var dropLongitude  = ""
    var dropPhone  = ""
    var dropAddress  = ""
    var pickUpAddress  = ""
    
    var payment_mode  = ""
    var refund_status  = ""
    
    var order_status  = ""
    var status_message  = ""
    var delivery_id  = ""
    var delivery_status  = ""
    var tracking_url  = ""
    var date_added  = ""
    var modified_date  = ""
    var approve_time  = ""
    var ready_time  = ""
    var shipped_time  = ""
    var delivered_time  = ""
    var cancel_time  = ""
    
    var delivery_type = ""
    
    var purchase_quantities = ""
    var product_price = ""
    var total_price  = ""
    
    var shipCharges  = ""
    var convenienceFee  = ""
    var orderPrice  = ""
    
    var promoCode  = ""
    var promoCodeDiscount  = ""
    var promotion_Id  = ""
    var txnId = ""
    var conveniencePercent = ""
    var totalOrderPrice = ""
    var refund_id = ""
    var drop_state = ""
    var drop_city = ""
    var drop_name = ""
    
    var drop_zip = ""
    var drop_note = ""
    var user_share = ""
    var order_otp = ""
    
    var pickup_date = ""
    var pickup_time = ""
    var profile_image = ""
    
    var productDetailsArray = [ProductDetails]()
    
    
    class func getAllOrderList(ordersArray: [JSON]) -> Array<OrderDetails>{
        var orderDetailsArray = Array<OrderDetails>()
        
        for details in ordersArray {
            
            let orderDetails = OrderDetails.parseOrderDetails(details: details)
            orderDetailsArray.append(orderDetails)
        }
        
        return orderDetailsArray
    }
    
    class func parseOrderDetails(details: JSON) -> OrderDetails{
        
        let orderDetails = OrderDetails()
        orderDetails.user_share = details["user_share"].string ?? ""
        orderDetails.order_otp = details["order_otp"].string ?? ""
        orderDetails.refund_status = details["refund_status"].string ?? ""
        orderDetails.order_status = details["order_status"].string ?? ""
        orderDetails.date_added = details["date_added"].string ?? ""
        orderDetails.order_id = details["order_id"].string ?? ""
        orderDetails.shipCharges = details["ship_charges"].string ?? ""
        orderDetails.convenienceFee = details["convenience_fee"].string ?? ""
        
        orderDetails.delivery_id = details["delivery_id"].string ?? ""
        orderDetails.txnId = details["txn_id"].string ?? ""
        orderDetails.conveniencePercent = details["convenience_percent"].string ?? ""
        orderDetails.modified_date = details["modified_date"].string ?? ""
        orderDetails.totalOrderPrice = details["total_price"].string ?? ""
        orderDetails.shipped_time = details["shipped_time"].string ?? ""
        orderDetails.payment_mode = details["payment_mode"].string ?? ""
        orderDetails.user_id = details["user_id"].string ?? ""
        orderDetails.refund_id = details["refund_id"].string ?? ""
        orderDetails.profile_image = details["image"].string ?? ""
        
        
        
        
        orderDetails.orderPrice = String(format: "%.2f", details["order_price"].doubleValue)
        orderDetails.promoCode = details["promocode"].string ?? ""
        orderDetails.promoCodeDiscount = details["promo_discount"].string ?? ""
        orderDetails.promotion_Id = details["promotion_id"].string ?? ""
        
        
        orderDetails.approve_time = details["approve_time"].string ?? ""
        orderDetails.cancel_time = details["cancel_time"].string ?? ""
        orderDetails.delivered_time = details["delivered_time"].string ?? ""
        orderDetails.shipped_time = details["shipped_time"].string ?? ""
        orderDetails.ready_time = details["ready_time"].string ?? ""
        
        if let dropInfo = details["drop_info"].dictionary{
            orderDetails.dropPhone = dropInfo["drop_phone"]?.string ?? ""
            orderDetails.dropLatitude = dropInfo["drop_latitude"]?.string ?? ""
            orderDetails.dropLongitude = dropInfo["drop_longitude"]?.string ?? ""
            orderDetails.dropAddress = dropInfo["drop_address"]?.string ?? ""
            orderDetails.pickUpAddress = details["store_address"].string ?? ""
            orderDetails.drop_state = dropInfo["drop_state"]?.string ?? ""
            orderDetails.drop_city = dropInfo["drop_city"]?.string ?? ""
            orderDetails.drop_name = dropInfo["drop_name"]?.string ?? ""
            orderDetails.delivery_type = dropInfo["delivery_type"]?.string ?? ""
            orderDetails.drop_zip = dropInfo["drop_zip"]?.string ?? ""
            orderDetails.drop_note = dropInfo["drop_note"]?.string ?? ""
            orderDetails.pickup_date = dropInfo["pickup_date"]?.string ?? ""
            orderDetails.pickup_time = dropInfo["pickup_time"]?.string ?? ""
        }
        if let singleProduct = details["product_data"].dictionary{
            let productDetails  = ProductDetails.parseProductDetails(details: details["product_data"])
            orderDetails.productDetailsArray.append(productDetails)
        }
        
        if let productsArray = details["products"].array{
            
            for productDetailsObj in productsArray{
                
                let totalPrice = productDetailsObj["total_price"].string ?? ""
                let purchaseQuantities = productDetailsObj["purchase_quantities"].string ?? ""
                let productPrice = productDetailsObj["product_price"].string ?? ""
                
                let productDetails  = ProductDetails.parseProductDetails(details: productDetailsObj["product_data"])
                productDetails.cartAddedQuantities = purchaseQuantities
                productDetails.purchase_quantities = purchaseQuantities
                productDetails.total_price = totalPrice
                productDetails.product_price = productPrice
                orderDetails.productDetailsArray.append(productDetails)
            }
        }
        
        return orderDetails;
    }
}
