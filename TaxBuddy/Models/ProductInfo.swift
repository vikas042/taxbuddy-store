//
//  ProductInfo.swift
//  Posh_IT
//
//  Created by Vikash Rajput on 29/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON


class ProductInfo: NSObject {

    var productName = ""

    var regularPrice = ""
    var productId = ""
    var quantities = ""
    var inStock = ""
    var offerPrice = ""
    var galleryImages = Array<String>()

    var varientConfigDetailsArray = Array<JSON>()

    
    
    class func getAllProductsInfoList(productsArray: [JSON]) -> Array<ProductInfo>{
        var productInfoArray = Array<ProductInfo>()

        for details in productsArray {
            let prodcutInfoDetails = ProductInfo.parseProdetailsInfo(details: details)
            productInfoArray.append(prodcutInfoDetails)
        }
    
        return productInfoArray
    }
    
    
    class func parseProdetailsInfo(details: JSON) -> ProductInfo {
       
        let prodcutInfoDetails = ProductInfo()
        

        prodcutInfoDetails.regularPrice = String(format: "%.2f",details["regular_price"].floatValue)
        prodcutInfoDetails.productId = details["_product_id"].string ?? ""
        prodcutInfoDetails.quantities = String(format: "%d",details["quantities"].intValue)
        prodcutInfoDetails.inStock = String(format: "%d",details["in_stock"].intValue)
        prodcutInfoDetails.offerPrice = String(format: "%.2f",details["offer_price"].floatValue)
        
        return prodcutInfoDetails
    }    
    
    class func getProductConfig(details: JSON) -> ProductInfo {
        
        let prodcutInfoDetails = ProductInfo()
        prodcutInfoDetails.regularPrice = String(format: "%.2f",details["regular_price"].floatValue)
        prodcutInfoDetails.productId = details["_product_id"].string ?? ""
        prodcutInfoDetails.quantities = String(format: "%d",details["quantities"].intValue)
        prodcutInfoDetails.inStock = String(format: "%d",details["in_stock"].intValue)
        prodcutInfoDetails.offerPrice = String(format: "%.2f",details["offer_price"].floatValue)
        prodcutInfoDetails.productId = details["_product_id"].string ?? ""
        prodcutInfoDetails.productName = details["_config_name"].string ?? ""        
        if let galleryImagesArray = details["gallery_images"].array{
            for prodcutImage in galleryImagesArray{
                if let prodcutImageUrl = prodcutImage.string{
                    prodcutInfoDetails.galleryImages.append(prodcutImageUrl)
                }
            }
        }
        
        
        if let configDetailsArray = details["_config_details"].array{
            
            for varientDetails in configDetailsArray{
                let name = varientDetails["name"].string ?? ""
                let value = varientDetails["key"].string ?? ""
                let selectedVarientDetails: JSON = ["name": name, "key": value]
                prodcutInfoDetails.varientConfigDetailsArray.append(selectedVarientDetails)
            }
        }
        return prodcutInfoDetails
    }


}
