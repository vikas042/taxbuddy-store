//
//  UserDetails.swift
//
//  Created by Vikash Rajput on 3/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class UserDetails: NSObject {


    static let sharedInstance = UserDetails()
    
    var userName: String?
    var userEmail: String?
    var userID = ""
    var apiSecretKey = ""
    var userAddress: String?
    var userAddress1: String?
    var userCity: String?
    var userCountry: String?
    var userZipCode: String?
    var userToken: String?
    var productImagePath = ""
    var dealImagePath = ""
    var gallerybasePath = ""
    var userImagePath = ""
    var storeImagePath = ""
    var phoneNumber = ""
    var orderId = ""
    var emailAdress = ""
    var fullName = ""
    
    var store_id = ""
    var store_gmt = ""

    var deviceToken: String = ""
    var accessToken = ""
    var latitude = ""
    var longitude = ""
    var cartCount = "0"
    var notificaionCount = "0"
    var apiHitInt = ""
    
    var googleCoordinatesAddress = ""
    var userImage = ""

    let currenySymbol = "$ "
}
