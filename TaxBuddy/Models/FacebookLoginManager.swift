//
//  FacebookLoginManager.swift
//  Ballebaazi
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Alamofire
import SwiftyJSON

class FacebookLoginManager: NSObject {

    var facebookLoginCompletion = { (result: JSON?, error: Error?) -> () in }

    static let sharedInstance = FacebookLoginManager()
    
    func callLoginMangerWithCompletion(completionblock: @escaping(_ result: JSON?, _ error: Error?) -> Void) {
        
        let loginManager = LoginManager()
        facebookLoginCompletion = completionblock
        if AccessToken.isCurrentAccessTokenActive {
            AppHelper.sharedInstance.displaySpinner()
            getFBUserData()
        }
        else{
            weak var weakSelf = self

            loginManager.logIn(permissions: [.publicProfile, .email], viewController : nil) { loginResult in
                switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login")
                case .success( _, _, _):
                    
                    AppHelper.sharedInstance.displaySpinner()
                    weakSelf!.getFBUserData()
                }
            }
        }
    }
    
    // MARK: Get User Data From Facebook
    
    func getFBUserData(){

        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let responseJSON = JSON(result as AnyObject);

                    let deviceID = UIDevice.current.identifierForVendor!.uuidString
                    
                    var deviceToken: String = UserDetails.sharedInstance.deviceToken
                    
                    if deviceToken.count == 0 {
                        deviceToken = "1111111111";
                    }
                    let facebookId = responseJSON["id"].string ?? ""
                    let userName = responseJSON["name"].string ?? ""
                    let userEmail = responseJSON["email"].string ?? ""
                    
                    let parameters = ["option": "signup","name": userName, "email": userEmail, "account_type": "2", "device_type": "1", "device_id": deviceID, "device_token": deviceToken,"facebook_id":facebookId]

                    self.callFacebookLoginApi(params: parameters)
                }
                else{
//                    AppHelper.sharedInstance.removeSpinner()
                }
            })
        }
    }
    
    func callFacebookLoginApi(params :[String: String])  {
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(withURL: kGpLoginURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let responseJSON = JSON(result as AnyObject);
                weakSelf!.facebookLoginCompletion(responseJSON,error)
            }
            else{
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
}
