//
//  StoreDetail.swift
//  Posh_IT
//
//  Created by MADSTECH on 13/09/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class StoreDetail: NSObject {

    static let sharedInstance = StoreDetail()
    
    var store_id  = ""
    var store_name  = ""
    var store_latitude  = ""
    var store_longitude  = ""
    var store_distance  = ""
    var store_image  = ""
    var paypal_id  = ""
    var store_close_time  = ""
    var store_city_name  = ""
    var date_added  = ""
    var store_pincode  = ""
    var total_withdraws  = ""
    var store_gmt  = ""
    var store_state_name  = ""
    var store_country  = ""
    var store_city  = ""
    var store_address  = ""
    var store_timezone  = ""
    var store_open_time  = ""
    var store_email  = ""
    var store_username  = ""
    var working_time  = ""
    var store_phone  = ""
    
    var in_service = ""

    
    
    class func getAllStoreList(storeArray: [JSON]) -> Array<StoreDetail>{
        var storeDetailsArray = Array<StoreDetail>()
        
        for details in storeArray {
            
            let storeDetails = StoreDetail.parseStoreDetails(details: details)
            storeDetailsArray.append(storeDetails)
        }
        
        return storeDetailsArray
    }
    
    
    class func parseStoreDetails(details: JSON) -> StoreDetail{
        
        let storeDetails = StoreDetail()
    
        storeDetails.store_id = details["store_id"].string ?? ""
        storeDetails.store_name = details["store_name"].string ?? ""
        storeDetails.store_latitude = details["store_latitude"].string ?? ""
        storeDetails.store_longitude = details["store_longitude"].string ?? ""
        storeDetails.store_distance = details["store_distance"].string ?? ""
        storeDetails.store_image = details["store_image"].string ?? ""
       
        return storeDetails;
    }
}
