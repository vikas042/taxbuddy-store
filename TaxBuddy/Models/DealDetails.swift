//
//  DealDetails.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 16/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
class DealDetails: NSObject {
    
    var deal_id  = ""
    var deal_name  = ""
    var deal_date_till  = ""
    var deal_max_discount  = ""
    var deal_status  = ""
    
    var deal_image = ""
    var dropLongitude  = ""
    var dropPhone  = ""
    var dropAddress  = ""
    var pickUpAddress  = ""
    
    var payment_mode  = ""
    var refund_status  = ""
    
    var order_status  = ""
    var status_message  = ""
    var delivery_id  = ""
    var delivery_status  = ""
    var tracking_url  = ""
    var date_added  = ""
    var modified_date  = ""
    var approve_time  = ""
    var ready_time  = ""
    var shipped_time  = ""
    var delivered_time  = ""
    var cancel_time  = ""
    
    var delivery_type = ""
    
    var purchase_quantities = ""
    var product_price = ""
    var total_price  = ""
    
    var shipCharges  = ""
    var convenienceFee  = ""
    var orderPrice  = ""
    
    var promoCode  = ""
    var promoCodeAmt  = ""
    
    var txnId = ""
    var conveniencePercent = ""
    var totalOrderPrice = ""
    var refund_id = ""
    var drop_state = ""
    var drop_city = ""
    var drop_name = ""
    var delievery_type = ""
    var drop_zip = ""
    var drop_note = ""
    
    var from_date = ""
    var end_date = ""
    var from_time = ""
    var end_time = ""
    
    var productDetailsArray = [ProductDetails]()
    
    
    class func getAllDealsList(dealsArray: [JSON]) -> Array<DealDetails>{
        var dealDetailsArray = Array<DealDetails>()
        
        for details in dealsArray {
            
            let dealDetails = DealDetails.parseDealDetails(details: details)
            dealDetailsArray.append(dealDetails)
        }
        
        return dealDetailsArray
    }
    
    class func parseDealDetails(details: JSON) -> DealDetails{
        
        let dealDetails = DealDetails()

        dealDetails.deal_id = details["deal_id"].string ?? ""
         dealDetails.deal_image = details["deal_image"].string ?? ""
        dealDetails.deal_name = details["deal_name"].string ?? ""
        dealDetails.deal_date_till = details["start_date"].string ?? ""
        dealDetails.deal_max_discount = details["maximum_discount"].string ?? ""
        dealDetails.deal_status = details["status"].string ?? ""
        dealDetails.from_date = details["from_date"].string ?? ""
        dealDetails.end_date = details["end_date"].string ?? ""
        dealDetails.from_time = details["start_time"].string ?? ""
        dealDetails.end_time = details["end_time"].string ?? ""
        
        if let dropInfo = details["drop_info"].dictionary{
            /*
            orderDetails.dropPhone = dropInfo["drop_phone"]?.string ?? ""
            orderDetails.dropLatitude = dropInfo["drop_latitude"]?.string ?? ""
            orderDetails.dropLongitude = dropInfo["drop_longitude"]?.string ?? ""
            orderDetails.dropAddress = dropInfo["drop_address"]?.string ?? ""
            orderDetails.pickUpAddress = details["store_address"].string ?? ""
            orderDetails.drop_state = dropInfo["drop_state"]?.string ?? ""
            orderDetails.drop_city = dropInfo["drop_city"]?.string ?? ""
            orderDetails.drop_name = dropInfo["drop_name"]?.string ?? ""
            orderDetails.delivery_type = dropInfo["delivery_type"]?.string ?? ""
            orderDetails.drop_zip = dropInfo["drop_zip"]?.string ?? ""
            orderDetails.drop_note = dropInfo["drop_note"]?.string ?? ""
            */
        }
        
        if let productsArray = details["products"].array{
            
            for productDetailsObj in productsArray{
                
                let totalPrice = productDetailsObj["total_price"].string ?? ""
                let purchaseQuantities = productDetailsObj["purchase_quantities"].string ?? ""
                let productPrice = productDetailsObj["product_price"].string ?? ""
                
                let productDetails  = ProductDetails.parseProductDetails(details: productDetailsObj["product_data"])
                productDetails.cartAddedQuantities = purchaseQuantities
                productDetails.purchase_quantities = purchaseQuantities
                productDetails.total_price = totalPrice
                productDetails.product_price = productPrice
                dealDetails.productDetailsArray.append(productDetails)
            }
        }
        
        return dealDetails
    }
}
