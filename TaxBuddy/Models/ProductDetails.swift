
//
//  ProductDetails.swift
//  Posh_IT
//
//  Created by Vikash Rajput on 29/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductDetails: NSObject {
    
    
    var cartAddedQuantities = "1"
    
    var manufactureBy = ""
    var productDescription = ""
    var productAttributedDescriptions : NSMutableAttributedString?
    var productName = ""
    var productViews = ""
    var totalRatings = ""
    var totalReview = ""
    var productId = ""
    var totalProduct = ""
    var productStatus = ""
    var productSku = ""
    var productType = ""
    var productImage = ""
    var featureProduct = ""
    var baseProductId = ""
    var product_deals = Array<DealDetails>()
    var productInfo = ProductInfo()
    var productConfigArray = Array<ProductInfo>()
    var varientConfigArray = Array<JSON>()
    var leftSwipeValue: CGFloat = 0.0
    var rightSwipeValue: CGFloat = 0.0
    var distance: Float = 0.0
    var galleryImages = Array<String>()
    var selectedVarient = Array<JSON>()
    var productSpecificationArray = Array<JSON>()
    
    var perProductId = ""
    var promotion_id = ""
    var promotion_discount = ""
    var promocode = ""
    var inCart = ""
    var inWishlist = ""
    
    var purchase_quantities = ""
    var total_price = ""
    var product_price = ""
    var storeID = ""
    var storeName = ""
    var storeInfo = StoreDetail()
    var bestDeal = DealDetails()
    
    
    class func parseStoreDetails(details: JSON) -> StoreDetails{
        let storeDetails = StoreDetails()
        return storeDetails
    }
    class func getAllProductsList(productsArray: [JSON]) -> Array<ProductDetails>{
        var productDetailsArray = Array<ProductDetails>()
        
        for details in productsArray {
            
            let productDetails = ProductDetails.parseProductDetails(details: details)
            productDetailsArray.append(productDetails)
        }
        
        return productDetailsArray
    }
    
    
    class func parseProductDetails(details: JSON) -> ProductDetails{
        
        let productDetails = ProductDetails()
        
        productDetails.manufactureBy = details["manufacture_by"].string ?? "N/A"
        if productDetails.manufactureBy.count == 0 {
            productDetails.manufactureBy = "N/A"
        }
        
        productDetails.productDescription = details["description"].string ?? "N/A"
        
        if productDetails.productDescription.count == 0 {
            productDetails.productDescription = "N/A"
        }
        
        productDetails.productName = details["product_name"].string ?? "N/A"
        productDetails.productViews = details["product_views"].string ?? "N/A"
       
        productDetails.promocode = details["promocode"].string ?? ""
        productDetails.promotion_id = details["promotion_id"].string ?? ""
        productDetails.promotion_discount = details["promo_discount"].string ?? "0"
        
        
        productDetails.totalRatings = details["total_ratings"].stringValue
        productDetails.totalReview = details["total_reviews"].string ?? "N/A"
        productDetails.inCart = "\(details["in_cart"].int ?? 0)"
        productDetails.inWishlist = "\(details["in_wishlist"].int ?? 0)"
        if let dealDet = details["best_deal"].dictionary{
            let best = DealDetails.parseDealDetails(details: details["best_deal"])
            productDetails.bestDeal = best
            
        }
        
        if let storeInfo =  details["store_info"].dictionary{
            productDetails.storeID = storeInfo["store_id"]?.string ?? ""
            productDetails.storeName = storeInfo["store_name"]?.string ?? ""
            productDetails.distance = storeInfo["distance"]?.float ?? 0.0
            productDetails.storeInfo = StoreDetail.parseStoreDetails(details: details["store_info"])
        }
        
        if productDetails.totalReview.count == 0 {
            productDetails.totalReview = "N/A"
        }
        
        if productDetails.productViews.count == 0 {
            productDetails.productViews = "N/A"
        }
        
        if productDetails.productDescription.count == 0 {
            productDetails.productDescription = "N/A"
        }
        
        if let idDetails = details["_id"].dictionary{
            productDetails.perProductId = idDetails["$oid"]?.string ?? ""
        }
        
        productDetails.productId = details["product_id"].string ?? ""
        productDetails.productSku = details["sku"].string ?? ""
        
        productDetails.productType = details["product_type"].string ?? ""
        productDetails.productImage = details["product_image"].string ?? ""
        productDetails.featureProduct = details["feature_product"].string ?? ""
        productDetails.baseProductId = details["base_product_id"].string ?? ""
        let productInfo = details["product_info"];
        productDetails.productInfo = ProductInfo.parseProdetailsInfo(details: productInfo)
        
        if let varientConfigArray = details["config_info"].array{
            for varientDetails in varientConfigArray{
                let name = varientDetails["name"].string ?? ""
                let value = varientDetails["values"].array ?? []
                let selectedVarientDetails: JSON = ["name": name, "values": value]
                productDetails.varientConfigArray.append(selectedVarientDetails)
            }
        }
        
        if let varientConfigArray = productInfo["_config_details"].array{
            
            for varientDetails in varientConfigArray{
                let name = varientDetails["name"].string ?? ""
                let value = varientDetails["key"].string ?? ""
                let selectedVarientDetails: JSON = ["name": name, "key": value]
                productDetails.selectedVarient.append(selectedVarientDetails)
            }
        }
        
        if let configData = details["config_data"].array{
            for details in configData{
                let configDetails = ProductInfo.getProductConfig(details: details)
                productDetails.productConfigArray.append(configDetails)
            }
        }
        
        if let specificationArray = details["custom_attributes"].array{
            productDetails.productSpecificationArray = specificationArray
        }
        
        if let dealArray = details["product_deals"].array{
            for details in dealArray{
                let dealDet = DealDetails.parseDealDetails(details: details)
                productDetails.product_deals.append(dealDet)
            }
        }
        
        if let galleryImagesArray = details["gallery_images"].array{
            for prodcutImage in galleryImagesArray{
                if let prodcutImageUrl = prodcutImage.string{
                    productDetails.galleryImages.append(prodcutImageUrl)
                }
            }
        }
        
        return productDetails;
    }
}
