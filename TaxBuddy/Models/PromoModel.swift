//
//  PromoModel.swift
//  TaxBuddy
//
//  Created by MAC on 10/11/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON

class PromoModel: NSObject {
    
    var promotion_code = ""
    var end_date = ""
    var descrption = ""
    var date_added = ""
    var total_used = ""
    var fixed_discount = ""
    var promotion_type = ""
    
    var start_date = ""
    var minimum_order = ""
    var discount_type = ""
    
    var maximum_discount = ""
    var promotion_name = ""
    var products = ""
    var promotion_id = ""
    
    var store_id = ""
    var percent_discount = ""
    var status = ""
    
    var product_id = ""
    var promo_discount = ""


    
    
    class func getAllPromoList(responseArray: [String : JSON]) -> Array<PromoModel>{
        
       
        var promoDetailsArray = Array<PromoModel>()
        if let promoDetailsDict = responseArray["valid_promos"]!.array{
            
            for details in promoDetailsDict{
                let promoDetails = PromoModel()
                promoDetails.product_id = details["product_id"].string ?? ""
                promoDetails.promo_discount = details["promo_discount"].string ?? ""
                promoDetails.promotion_id = details["promotion_id"].string ?? ""
                if let promocode = details["promocode"].dictionary{
                    promoDetails.promotion_code = promocode["promotion_code"]?.string ?? ""
                    promoDetails.end_date = promocode["end_date"]?.string ?? ""
                    promoDetails.descrption = promocode["description"]?.string ?? ""
                    promoDetails.date_added = promocode["date_added"]?.string ?? ""
                    promoDetails.total_used = promocode["total_used"]?.string ?? ""
                    promoDetails.fixed_discount = promocode["fixed_discount"]?.string ?? "0"
                    
                    promoDetails.promotion_type = promocode["promotion_type"]?.string ?? ""
                    promoDetails.start_date = promocode["start_date"]?.string ?? ""
                    promoDetails.minimum_order = promocode["minimum_order"]?.string ?? ""
                    promoDetails.discount_type = promocode["discount_type"]?.string ?? ""
                    promoDetails.maximum_discount = promocode["maximum_discount"]?.string ?? ""
                    promoDetails.promotion_name = promocode["promotion_name"]?.string ?? "0"
                    
                    promoDetails.products = promocode["products"]?.string ?? ""
                    promoDetails.promotion_id = promocode["promotion_id"]?.string ?? ""
                    promoDetails.store_id = promocode["store_id"]?.string ?? ""
                    promoDetails.percent_discount = promocode["percent_discount"]?.string ?? ""
                    promoDetails.status = promocode["status"]?.string ?? "0"
                }
                
                if promoDetails.promotion_id == ""{
                    
                }else{
                    promoDetailsArray.append(promoDetails)
                }
            }
            
            
        }
        
        return promoDetailsArray
        
    }
    
    class func getPromo(responseArray: [String : JSON]) -> PromoModel{
        
        
        var promoDetails = PromoModel()
        if let promoDetailsDict = responseArray["valid_promos"]!.array{
            
            for details in promoDetailsDict{
                promoDetails.promo_discount = details["promo_discount"].string ?? ""
                promoDetails.promotion_code = details["promotion_code"].string ?? ""
                promoDetails.end_date = details["end_date"].string ?? ""
                promoDetails.descrption = details["description"].string ?? ""
                promoDetails.date_added = details["date_added"].string ?? ""
                promoDetails.total_used = details["total_used"].string ?? ""
                promoDetails.fixed_discount = details["fixed_discount"].string ?? "0"
                    
                promoDetails.promotion_type = details["promotion_type"].string ?? ""
                promoDetails.start_date = details["start_date"].string ?? ""
                promoDetails.minimum_order = details["minimum_order"].string ?? ""
                promoDetails.discount_type = details["discount_type"].string ?? ""
                promoDetails.maximum_discount = details["maximum_discount"].string ?? ""
                promoDetails.promotion_name = details["promotion_name"].string ?? "0"
                    
                promoDetails.products = details["products"].string ?? ""
                promoDetails.promotion_id = details["promotion_id"].string ?? ""
                promoDetails.store_id = details["store_id"].string ?? ""
                promoDetails.percent_discount = details["percent_discount"].string ?? ""
                promoDetails.status = details["status"].string ?? "0"
                }
            
            }
            return promoDetails
        }
   

}
