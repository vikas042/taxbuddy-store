//
//  OrderTableViewCell2.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 05/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class OrderTableViewCell2: UITableViewCell {

    
    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var currentStatus: UILabel!
    @IBOutlet weak var orderStatus: UILabel!
    
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var lblNameProduct: UILabel!
    @IBOutlet weak var lblrateProduct: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var btnWriteReview: UIButton!
    @IBOutlet weak var lblEstTime: UILabel!
    
  

    func configData(productDetails: ProductDetails) {
        
        //showShadowOnView(view: innerView)
        innerView.layer.masksToBounds = false
        innerView.layer.cornerRadius = 5
        innerView.layer.shadowColor = UIColor.gray.cgColor
        innerView.layer.shadowOpacity = 1
        innerView.layer.shadowOffset = CGSize(width: -1, height: 1)
        innerView.layer.shadowRadius = 2
        
        imageProduct.layer.cornerRadius = 5.0
        //imageProduct.clipsToBounds = true
        //imageProduct.layer.borderWidth = 1.0
        //imageProduct.layer.borderColor = UIColor.lightGray.cgColor
        var bestDeal = 0.0
        if productDetails.bestDeal.deal_id != ""{
            
            bestDeal = Double(productDetails.bestDeal.deal_max_discount)!
            let totalPaidPrice = Double(productDetails.productInfo.offerPrice)! * Double(bestDeal)/100.0
            lblrateProduct.text = "Amount: " + UserDetails.sharedInstance.currenySymbol + String(format: "%.2f", Double(productDetails.productInfo.offerPrice)! - totalPaidPrice)
            //lblrateProduct.text = "Amount: " + UserDetails.sharedInstance.currenySymbol + "\(Double(productDetails.productInfo.offerPrice)! - totalPaidPrice )"
           
        }else{
            lblrateProduct.text = UserDetails.sharedInstance.currenySymbol + productDetails.productInfo.offerPrice
            
            
        }
        let productInfoDetails = productDetails.productInfo;
        lblNameProduct?.text = productDetails.productName
        lblQuantity.text = "Quantity " + productDetails.cartAddedQuantities + ""
      
        if productDetails.productImage.count > 0 {
            let imageUrl = UserDetails.sharedInstance.productImagePath + productDetails.productImage
            
            if let url = URL(string: imageUrl){
                imageProduct.setImage(with: url , placeholder: UIImage(named: "icon_placeholder_image"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.imageProduct.image = image
                    }
                    else{
                        self?.imageProduct.image = UIImage(named: "icon_placeholder_image")
                    }
                })
            }
            else{
                self.imageProduct.image = UIImage(named: "icon_placeholder_image")
            }
        }
        else{
            self.imageProduct.image = UIImage(named: "icon_placeholder_image")
        }
    }
    
    
    func showShadowOnView(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: -1, height: 1)
        view.layer.shadowRadius = 2
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
