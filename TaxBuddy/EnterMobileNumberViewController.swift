//
//  EnterMobileNumberViewController.swift
//  TaxBuddy
//
//  Created by Vikash Rajput on 25/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class EnterMobileNumberViewController: UIViewController {

    @IBOutlet weak var txtPhone: CustomTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func submitButtonTapped(_ sender: Any) {
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
