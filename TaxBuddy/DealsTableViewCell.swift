//
//  DealsTableViewCell.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 10/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class DealsTableViewCell: UITableViewCell {
    @IBOutlet weak var offerName: UILabel!
    @IBOutlet weak var validDate: UILabel!
    @IBOutlet weak var dealsPercentOff: UILabel!
    
    @IBOutlet weak var toDateLbl: UILabel!
    
    
    @IBOutlet weak var dealImage: UIImageView!
    @IBOutlet weak var changeDateOutlet: UIButton!
    @IBOutlet weak var activateSwitch: UISwitch!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        AppHelper.showShadowOnView(view: containerView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
