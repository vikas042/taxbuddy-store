//
//  OrderTrackingViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 02/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

//
//  OrderDetailViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 02/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//



import UIKit
import SwiftyJSON
import Alamofire
//import BraintreeDropIn
//import Braintree



class OrderTrackingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate,ChangeStatusDelegate{
    
    
    var orderState = ""
    var delivery_type = ""
    var actionPerform = -1
    
    @IBOutlet weak var orderIdLbl: UILabel!
    
    @IBOutlet weak var tableOrderSummary: UITableView!
    
    var date = "Date               "
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    var time = "Time               "
    var isTime = 0
    var productDetails: ProductDetails?
    var currentLatitude = ""
    var currentLongitude = ""
    var currentAddress = ""
    var deliveryPhoneNumber = ""
    var deliveryPhoneNumberTxtField: UITextField?
    var appartmentNumberTxtField: UITextField?
    var deliveryChargesArray = [JSON]()
    var isViewForBuyNow = false
    var isViewForReOrder = false
    var orderID = ""
    
    var changeStatusView:ChangeStatus?
    
    var shouldDeliverToMe = 0
    var source = ""
    var orderDetails: OrderDetails?
    var ordersDetailsArray = [OrderDetails]()
    var productDetailsArray = [ProductDetails]()
    var promoCode = ""
    var promoAmount = "0"
    var orderAmountValue = ""
    var orderTransactionID = ""
    
    @IBOutlet weak var placeholderButton: UIButton!
    
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var errorMessageContainerViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var errorMessageContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableOrderSummary.delegate = self
        tableOrderSummary.dataSource=self
        self.navigationController?.navigationBar.isHidden = true
        
        // placeholderButton.applyGradient(colours: [UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1),UIColor(red: 132/255, green: 199/255, blue: 158/255, alpha: 1)])
        UserDetails.sharedInstance.apiHitInt = "21"
        currentLatitude = UserDetails.sharedInstance.latitude
        currentLongitude = UserDetails.sharedInstance.longitude
        currentAddress = UserDetails.sharedInstance.googleCoordinatesAddress
       
        // placeholderButton.applyGradient(colours: [UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1),UIColor(red: 132/255, green: 199/255, blue: 158/255, alpha: 1)])
        //callDeliverdOrderAPI(params: params)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if source == "NotificationController"{
            let parameters: Parameters = ["option": "order_info","order_id": orderID,"store_id":"\(StoreDetail.sharedInstance.store_id)"]
            callOrderDeatilApi(params: parameters, isNeedToShowLoader: false)
        }else{
            setData()
        }
        
    }
    
    func setData(){
        orderIdLbl.text = "Order# \(orderDetails!.order_id)"
        deliveryPhoneNumber = format(phoneNumber: UserDetails.sharedInstance.phoneNumber, shouldRemoveLastDigit: false)
        if isViewForBuyNow {
            productDetailsArray.append(productDetails!)
        }
        
     //   let params: Parameters = ["option": "MARK_AS_DELIVERED", "order_id":"\(orderDetails!.order_id)","store_id":orderDetails!.productDetailsArray[0].storeInfo.store_id]
        if orderDetails?.order_status == "6" || orderDetails?.order_status == "7" || orderDetails?.order_status == "4" || orderDetails?.order_status == "3"{
            self.placeholderButton.isHidden = true
            self.tableBottomConstraint.constant = 0
            if orderDetails?.order_status == "3" || orderDetails?.order_status == "4"{
                if orderDetails?.delivery_type == "2"{
                    self.placeholderButton.isHidden = false
                    self.tableBottomConstraint.constant = 46
                }
            }
          
        }else if orderDetails?.order_status == "5"{
            self.placeholderButton.isHidden = true
            self.tableBottomConstraint.constant = 0
        }else{
            self.placeholderButton.isHidden = false
            self.tableBottomConstraint.constant = 46
        }
        if currentLatitude == UserDetails.sharedInstance.latitude{
            return;
        }
        
        currentLatitude = UserDetails.sharedInstance.latitude
        currentLongitude = UserDetails.sharedInstance.longitude
        currentAddress = UserDetails.sharedInstance.googleCoordinatesAddress
    }
    
    // MARK:- IBAction Methods
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deliverMeAction(_ sender: Any) {
        date = "Date               "
        time = "Time               "
        shouldDeliverToMe = 1
        tableOrderSummary.reloadData()
        
    }
    @IBAction func pickStoreAction(_ sender: Any) {
        shouldDeliverToMe = 0
        tableOrderSummary.reloadData()
    }
    
    
    @IBAction func btnPhoneChangeTapped(_ sender: Any) {
        
    }
    
    @objc func cancelButtonTapped(button: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPlaceOrderTapped(_ sender: Any) {
        
        
    }
    
    @IBAction func netBankingTapped(_ sender: Any) {
        
    }
    
    @IBAction func removePromoCodeButtonTapped(_ sender: Any) {
        promoCode = ""
        promoAmount = "0"
        tableOrderSummary.reloadData()
    }
    
    @IBAction func creditCardTapped(_ sender: Any) {
        
    }
    
    @IBAction func debitCardTapped(_ sender: Any) {
        
    }
    
    @IBAction func cashOnDeliveryTapped(_ sender: Any) {
        
    }
    
    @IBAction func btnEditAddressTapped(_ sender: Any) {
        
    }
    
   
    
    func promoCodeApplySuccessfully(promoCode: String, promoCodeAmount: String) {
        self.promoCode = promoCode
        self.promoAmount = promoCodeAmount
        self.tableOrderSummary.reloadData()
    }
    
    
    //MARK:- TableView Data source and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderDetails != nil{
            if tableView.tag == 100 {
                return productDetailsArray.count;
            }
            return productDetailsArray.count + 4;
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100 {
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "InnerCellTableViewCell") as? InnerCellTableViewCell
            if cell == nil {
                cell = InnerCellTableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "AddressTableViewCell")
            }
            if orderDetails?.productDetailsArray[indexPath.row].promocode != ""{
                cell?.promoLbl.text = "\(orderDetails!.productDetailsArray[indexPath.row].promocode) applied ($ \(orderDetails!.productDetailsArray[indexPath.row].promotion_discount) off)"
                
                cell?.promoAmountLbl.text =  "- $ \(orderDetails!.productDetailsArray[indexPath.row].promotion_discount)"
            }
            cell?.selectionStyle = .none
            
            let productDetails = productDetailsArray[indexPath.row]
            var productPrice = 0.0
            var bestDeal = 0.0
            if productDetails.bestDeal.deal_id != ""{
                bestDeal = Double(productDetails.bestDeal.deal_max_discount)!
                let totalPaidPrice = Double(productDetails.productInfo.offerPrice)! * Double(bestDeal)/100.0
                productPrice = (Double(productDetails.productInfo.offerPrice)! - totalPaidPrice)
                cell?.lblPriceDetail.text = UserDetails.sharedInstance.currenySymbol + String(format: "%.2f",productPrice * Double(productDetails.cartAddedQuantities)!)
            }else{
                cell?.lblPriceDetail.text = UserDetails.sharedInstance.currenySymbol + productDetails.productInfo.offerPrice
            }
            
            cell?.lblProductName.text = productDetails.productName
            let totalPrice = Double(productDetails.productInfo.offerPrice)! *  Double(productDetails.cartAddedQuantities)!
            
            if Int(productDetails.cartAddedQuantities)! > 0{
                cell?.lblNumberOfUnit.text = UserDetails.sharedInstance.currenySymbol + String(format: "%.2f",productPrice) + " x "+productDetails.cartAddedQuantities + " Units"
            }
            return cell!
            
        }
        else if(indexPath.row == productDetailsArray.count + 1){
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell") as? AddressTableViewCell
            if cell == nil {
                cell = AddressTableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "AddressTableViewCell")
            }
            cell?.selectionStyle = .none
            if orderDetails?.delivery_type == "1"{
                shouldDeliverToMe = 1
            }else{
                shouldDeliverToMe = 0
            }
            
           // cell?.mobileLbl.text = AppHelper.format(phoneNumber: orderDetails!.dropPhone)
            cell?.selectionStyle = .none
            if shouldDeliverToMe == 0{
                cell?.addressView.isHidden = true
                cell?.dateBtn.isHidden = false
                let trimmedString = orderDetails!.pickup_date.trimmingCharacters(in: .whitespaces)
                cell?.timeBtn.isHidden = false
                cell?.dateBtn.setTitle(AppHelper.formattedDateFromString(dateString: trimmedString, withFormat: "MM/dd/yyyy")! + "        ", for: .normal)
                cell?.timeBtn.setTitle(orderDetails!.pickup_time + "               ", for: .normal)
                AppHelper.poshITCustomPopupShadow(view: (cell?.pickAtStore)!)
                cell!.pickAtStore.backgroundColor = UIColor.white
                cell?.pickAtStore.setImage(UIImage(named: "iconBagBlue"), for: .normal)
                cell?.delieverMe.setImage(UIImage(named: "iconPinGrey"), for: .normal)
                cell?.delieverMe.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
                cell?.delieverMe.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
                cell?.pickAtStore.layer.shadowOpacity = 1
                cell?.delieverMe.layer.shadowOpacity = 0
            }else{
                cell?.addressView.isHidden = false
                cell?.lblAddress.text = orderDetails?.dropAddress
                cell?.cityLbl.text = orderDetails?.drop_city
                cell?.stateLbl.text = orderDetails?.drop_state
                cell?.zipCodeLbl.text = orderDetails?.drop_zip
                
                cell?.lblAddress.isEditable = false
                cell?.cityLbl.isEnabled = false
                cell?.stateLbl.isEnabled = false
                cell?.zipCodeLbl.isEnabled = false
             //   cell?.mobileLbl.isEnabled = false
                
                cell?.dateBtn.isHidden = true
                cell?.timeBtn.isHidden = true
                AppHelper.poshITCustomPopupShadow(view: (cell?.delieverMe)!)
                cell!.delieverMe.backgroundColor = UIColor.white
                cell?.delieverMe.setImage(UIImage(named: "iconPin"), for: .normal)
                
                cell?.pickAtStore.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
                cell?.pickAtStore.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
                cell?.pickAtStore.setImage(UIImage(named: "iconBag"), for: .normal)
                cell?.pickAtStore.layer.shadowOpacity = 0
                cell?.delieverMe.layer.shadowOpacity = 1
                
            }
            
            
            return cell!;
        }
        else if(indexPath.row == productDetailsArray.count + 2){
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "PriceDetailTableViewCell") as? PriceDetailTableViewCell
            if cell == nil {
                cell = PriceDetailTableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "PriceDetailTableViewCell")
            }
            cell?.selectionStyle = .none
            
              //cell?.promoTopConstraint.constant = 97
            
            if UIDevice.current.userInterfaceIdiom == .pad{
                cell?.tblViewHeightConstraint.constant = CGFloat(productDetailsArray.count * 90)
            }else{
                cell?.tblViewHeightConstraint.constant = CGFloat(productDetailsArray.count * 80)
            }
            
            
            
            var totalProductsPrice = 0.0
            cell?.lblPromoCode.text = "(" + promoCode + ")"
            
            cell?.lblPromoCodeAmt.text = UserDetails.sharedInstance.currenySymbol + promoAmount
            /*
            if promoCode.count != 0{
                cell?.lblPromoCodeAmt.isHidden = false
                cell?.lblPromoCode.isHidden = false
                cell?.lblPromoCodeTitle.isHidden = false
                cell?.removePromoCodeButton.isHidden = false
            }
            else{
                cell?.lblPromoCodeAmt.isHidden = true
                cell?.lblPromoCode.isHidden = true
                cell?.lblPromoCodeTitle.isHidden = true
                cell?.removePromoCodeButton.isHidden = true
            }
            */
            
            for productDetails in productDetailsArray{
                totalProductsPrice = totalProductsPrice + Double(productDetails.productInfo.offerPrice)! * Double(productDetails.cartAddedQuantities)!
            }
            
            
            cell?.lblDeliveryCharges.text = UserDetails.sharedInstance.currenySymbol + "0"
            
            var totalDeliveryCharges = 0.0
            var totalConvenienceFee = 0.0
            
            for deliveryChargesDetails in deliveryChargesArray{
                
                let deliveryChargesStr = deliveryChargesDetails["delivery_fee"].string ?? "0"
                
                
                let deliveryCharges = Double(deliveryChargesStr)!/100
                
                totalDeliveryCharges = totalDeliveryCharges + deliveryCharges
                
            }
            
            
            cell?.lblDeliveryCharges.text = UserDetails.sharedInstance.currenySymbol + orderDetails!.shipCharges
            //  cell?.lblConvenenceFee.text = UserDetails.sharedInstance.currenySymbol +  String(totalConvenienceFee)
            let promoCodeAmt = Double(promoAmount)!
            
            if shouldDeliverToMe == 0{
                
                /*- promoCodeAmt */
                cell?.lblDeliveryCharges.isHidden = true
                cell?.deliveryChargeText.isHidden = true
                
                cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", Double(orderDetails!.orderPrice)!)
            }else{
                
                // let total = totalProductsPrice + totalDeliveryCharges  /*- promoCodeAmt */
                cell?.lblDeliveryCharges.isHidden = false
                cell?.deliveryChargeText.isHidden = false
                cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", Double(orderDetails!.orderPrice)!)
            }
            
            /*
             if promoCodeAmt > totalProductsPrice{
             let total = totalDeliveryCharges + totalConvenienceFee
             cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", total)
             
             }
             else{
             if shouldDeliverToMe == 0{
             let total = totalProductsPrice  /*- promoCodeAmt */
             cell?.lblDeliveryCharges.isHidden = true
             cell?.deliveryChargeText.isHidden = true
             
             cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", total)
             }else{
             let total = totalProductsPrice + totalDeliveryCharges  /*- promoCodeAmt */
             cell?.lblDeliveryCharges.isHidden = false
             cell?.deliveryChargeText.isHidden = false
             cell?.lblTotalCharges.text = UserDetails.sharedInstance.currenySymbol +  String(format: "%.2f", total)
             }
             
             }
             */
            cell?.layoutIfNeeded()
            return cell!
        }else if (indexPath.row == productDetailsArray.count + 3){
            var cell = tableView.dequeueReusableCell(withIdentifier: "OrderStatusCell") as? OrderStatusCell
            cell?.selectionStyle = .none
            cell?.cancelStatusView.isHidden = true
            cell?.orderApprovedLbl.isHidden = false
            cell?.imageStatus2.isHidden = false
            cell?.viewCell.layer.masksToBounds = false
            cell?.viewCell.layer.cornerRadius = 10
            cell?.viewCell.layer.shadowColor = UIColor.gray.cgColor
            cell?.viewCell.layer.shadowOpacity = 1
            cell?.viewCell.layer.shadowOffset = CGSize(width: -1, height: 1)
            cell?.viewCell.layer.shadowRadius = 2
            self.placeholderButton.isHidden = false
            self.tableBottomConstraint.constant = 46
            
            if orderDetails?.delivery_type == "1"{
                cell?.statusReadyToShip.text = "Ready to Ship"
            }else{
                cell?.statusReadyToShip.text = "Ready to Pick"
            }
            
            if orderDetails?.delivery_type == "1"{
                cell?.lblOrderDeliverdTime.isHidden = false
                cell?.imageStatus5.isHidden = false
                cell?.statusPackageDelivered.isHidden = false
                cell?.shippedLineView.isHidden = false
            }else{
                cell?.lblOrderDeliverdTime.isHidden = true
                cell?.imageStatus5.isHidden = true
                cell?.statusPackageDelivered.isHidden = true
                cell?.shippedLineView.isHidden = true
                cell?.statusPackageShipped.text = "Package Delivered"
            }
          
            
            
            if orderDetails?.order_status == "1"{
                
                cell?.lblOrderPlacedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.date_added)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.date_added)!)
                cell?.imageStatus1.image = UIImage(named: "statusDoneIcon")
            }
            else if orderDetails?.order_status == "2"{
                
                cell?.lblOrderPlacedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.date_added)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.date_added)!)
                cell?.lblOrderApprovedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.approve_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.approve_time)!)
                cell?.imageStatus1.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus2.image = UIImage(named: "statusDoneIcon")
            }
            else  if orderDetails?.order_status == "3"{
                cell?.lblOrderPlacedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.date_added)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.date_added)!)
                cell?.lblOrderApprovedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.approve_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.approve_time)!)
              
                if orderDetails?.delivery_type == "1" {
                    cell?.lblOrderProcessTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.ready_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.ready_time)!)
                    self.tableBottomConstraint.constant = 0
                    self.placeholderButton.isHidden = true
                }else{
                    cell?.lblOrderProcessTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.ready_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.ready_time)!)
                    self.placeholderButton.isHidden = false
                    self.tableBottomConstraint.constant = 46
                    self.placeholderButton.setTitle("Mark as Delivered", for: .normal)
                    self.actionPerform = 1
                }
                
                cell?.imageStatus1.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus2.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus3.image = UIImage(named: "statusDoneIcon")
                
                
                if orderDetails?.delivery_type == "1"{
                    
                }
            }
            else  if orderDetails?.order_status == "4"{
               
                cell?.lblOrderPlacedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.date_added)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.date_added)!)
                cell?.lblOrderApprovedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.approve_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.approve_time)!)
                cell?.lblOrderProcessTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.ready_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.ready_time)!)
                if orderDetails?.delivery_type == "1"{
                    cell?.lblOrderShippedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.shipped_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.shipped_time)!)
                    self.placeholderButton.isHidden = true
                    self.tableBottomConstraint.constant = 0
                    
                }else{
                    self.placeholderButton.isHidden = false
                    self.tableBottomConstraint.constant = 46
                }
                
                
                cell?.imageStatus1.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus2.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus3.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus4.image = UIImage(named: "statusDoneIcon")
            }else  if orderDetails?.order_status == "6" || orderDetails?.order_status == "7"{
                self.tableBottomConstraint.constant = 0
                cell?.cancelStatusView.isHidden = false
                cell?.orderApprovedLbl.isHidden = true
                cell?.imageStatus2.isHidden = true
                cell?.cancelTimeLbl.text = AppHelper.convertDateStringToFormattedDateString(date: orderDetails!.cancel_time) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.cancel_time)!)
                self.placeholderButton.isHidden = true
            }
            else {
                
                cell?.lblOrderPlacedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.date_added)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.date_added)!)
                cell?.lblOrderApprovedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.approve_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.approve_time)!)
                cell?.lblOrderProcessTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.ready_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.ready_time)!)
               
                cell?.lblOrderDeliverdTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.delivered_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.delivered_time)!)
                
                if orderDetails?.delivery_type == "1"{
                     cell?.lblOrderShippedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.shipped_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.shipped_time)!)
                }else{
                     cell?.lblOrderShippedTime.text = AppHelper.convertDateStringToFormattedDateString(date: (orderDetails?.delivered_time)!) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: (orderDetails?.delivered_time)!)
                }
                
                cell?.imageStatus1.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus2.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus3.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus4.image = UIImage(named: "statusDoneIcon")
                cell?.imageStatus5.image = UIImage(named: "statusDoneIcon")
                
                self.tableBottomConstraint.constant = 0
                self.placeholderButton.isHidden = true
            }
            return cell!;
        }else{
            
            if indexPath.row == 0{
                var cell = tableView.dequeueReusableCell(withIdentifier: "BuyerDetailTableViewCell") as? BuyerDetailTableViewCell
                cell?.profileName.text = orderDetails?.drop_name
                cell?.selectionStyle = .none
                cell?.profileNumber.text = AppHelper.format(phoneNumber: orderDetails!.dropPhone)
                let profileimageUrl = UserDetails.sharedInstance.userImagePath + orderDetails!.profile_image
                if let url = URL(string: profileimageUrl){
                    cell!.profileImage.setImage(with: url , placeholder: UIImage(named: "profile_pic"), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            cell!.profileImage.image = image
                        }
                        else{
                            cell!.profileImage.image = UIImage(named: "profile_pic")
                        }
                    })
                }
                else{
                    cell!.profileImage.image = UIImage(named: "profile_pic")
                }
                
                return cell!
            }else{
                var cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell2") as? OrderTableViewCell2
                
                if cell == nil {
                    cell = OrderTableViewCell2(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "OrderTableViewCell2")
                }
                
                if indexPath.row == productDetailsArray.count {
                    cell?.orderStatus.isHidden = false
                    if Int(orderDetails!.order_status) == 1{
                        cell?.orderStatus.text = "Order Pending"
                    }else if Int(orderDetails!.order_status) == 2{
                        cell?.orderStatus.text = "Order Approved"
                    }else if Int(orderDetails!.order_status) == 3{
                        if orderDetails!.delivery_type == "1"{
                          cell?.orderStatus.text = "Order Ready to Ship"
                        }else{
                            cell?.orderStatus.text = "Order Ready to Pick"
                        }
                    }else if Int(orderDetails!.order_status) == 4{
                        cell?.orderStatus.text = "Order Shipped"
                    }else if Int(orderDetails!.order_status) == 5{
                        cell?.orderStatus.text = "Order Delivered"
                    }else if Int(orderDetails!.order_status) == 6{
                        cell?.orderStatus.text = "Order Cancelled"
                    }else if Int(orderDetails!.order_status) == 7{
                        cell?.orderStatus.text = "Order Cancelled"
                    }
                    orderState = (cell?.orderStatus.text!)!
                }else{
                    cell?.currentStatus.isHidden = true
                    cell?.lineLbl.isHidden = true
                    cell?.orderStatus.isHidden = true
                }
                
                
                cell?.selectionStyle = .none
                
                let productDetails = productDetailsArray[indexPath.row - 1 ]
                
                var bestDeal = 0.0
                if productDetails.bestDeal.deal_id != ""{
                    
                    bestDeal = Double(productDetails.bestDeal.deal_max_discount)!
                    let totalPaidPrice = Double(productDetails.productInfo.offerPrice)! * Double(bestDeal)/100.0
                    cell?.lblrateProduct.text = "Amount " + UserDetails.sharedInstance.currenySymbol + String(format: "%.2f", "\(Double(productDetails.productInfo.offerPrice)! - totalPaidPrice)")
                    
                    
                }else{
                    cell?.lblrateProduct.text = "Amount " + UserDetails.sharedInstance.currenySymbol + productDetails.productInfo.offerPrice
                }
                cell?.configData(productDetails: productDetails)
                for deliverInfo in deliveryChargesArray{
                    
                    if productDetails.storeID == deliverInfo["store_id"].string{
                        
                        break;
                    }
                }
                
                return cell!;
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView.tag == 100{
            if UIDevice.current.userInterfaceIdiom == .pad{
                return 90
            }else{
                return 80
            }
        }
        else{
            if(indexPath.row == productDetailsArray.count + 1){
                if shouldDeliverToMe == 0{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        return 200
                    }else{
                      return 135
                    }
                }else{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                      return 307
                    }else{
                        return 270
                    }
                    
                   // return 260
                }
                
            }
            else if(indexPath.row == productDetailsArray.count + 2){
                if promoCode.count == 0{
                    if shouldDeliverToMe == 0{
                        if UIDevice.current.userInterfaceIdiom == .pad{
                            return CGFloat(135.0 + Double(productDetailsArray.count * 90));
                        }else{
                            return CGFloat(115.0 + Double(productDetailsArray.count * 75));
                        }
                       
                    }else{
                        if UIDevice.current.userInterfaceIdiom == .pad{
                            return CGFloat(155.0 + Double(productDetailsArray.count * 90));
                        }else{
                            return CGFloat(135.0 + Double(productDetailsArray.count * 75));
                        }
                        
                    }
                    
                }
                if UIDevice.current.userInterfaceIdiom == .pad{
                    return CGFloat(240.0 + Double(productDetailsArray.count * 90))
                }else{
                    return CGFloat(220.0 + Double(productDetailsArray.count * 75))
                }
                
            }else if (indexPath.row == productDetailsArray.count + 3){
                if  orderDetails?.order_status == "6"{
                    if UIScreen.main.bounds.size.width == 320{
                        return 140;
                    }
                    return 100;
                }
                else if  orderDetails?.order_status == "7"{
                    if UIScreen.main.bounds.size.width == 320{
                        return 140;
                    }
                    return 100;
                }
                else {
                    if orderDetails?.delivery_type == "1"{
                        return 290
                    }else{
                        return 270
                    }
                   
                }
            }else {
                if indexPath.row == 0{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        return 164
                    }else{
                        return 79
                    }
                    
                }
                if indexPath.row == productDetailsArray.count {
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        return 206
                    }else{
                        return 151.0
                    }
                }else{
                    return 140
                }
                
                
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
         if (tableView.tag != 100) && (indexPath.row < productDetailsArray.count){
         let productDetails = productDetailsArray[indexPath.row]
         let productDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as?
         ProductDetailViewController
         productDetailVC?.productId = productDetails.productId
         self.navigationController?.pushViewController(productDetailVC!, animated: true)
         }
         */
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if deliveryPhoneNumberTxtField == textField {
            deliveryPhoneNumber = textField.text ?? ""
            tableOrderSummary.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == deliveryPhoneNumberTxtField {
            var fullString = textField.text ?? ""
            fullString.append(string)
            if range.length == 1 {
                textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: true)
            } else {
                textField.text = format(phoneNumber: fullString)
            }
            return false
        }
        return true
    }
    
    func format(phoneNumber: String, shouldRemoveLastDigit: Bool = false) -> String {
        guard !phoneNumber.isEmpty else { return "" }
        guard let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive) else { return "" }
        let r = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: .init(rawValue: 0), range: r, withTemplate: "")
        
        if number.count > 10 {
            let tenthDigitIndex = number.index(number.startIndex, offsetBy: 10)
            number = String(number[number.startIndex..<tenthDigitIndex])
        }
        
        if shouldRemoveLastDigit {
            let end = number.index(number.startIndex, offsetBy: number.count-1)
            number = String(number[number.startIndex..<end])
        }
        
        if number.count < 7 {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d+)", with: "($1) $2", options: .regularExpression, range: range)
            
        } else {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: range)
        }
        
        return number
    }
    
    //MARK:- API Related Methods
    
    func callDeliveryEstimateAPI(params: Parameters)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kProductsURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            self.deliveryChargesArray.removeAll()
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if let responseDict  = result!["response"]?.dictionary{
                        
                        if let cartCount = responseDict["total_cart"]?.string{
                            UserDetails.sharedInstance.cartCount = cartCount
                        }
                        
                        if (responseDict["total_cart"]?.string) != nil
                            
                        {
                            UserDetails.sharedInstance.cartCount = (responseDict["total_cart"]?.string)!
                        }
                        
                        if let shipChargesDict  = responseDict["ship_charges"]?.dictionary{
                            
                            let deliveryCharges = shipChargesDict["fee"]?.stringValue ?? "0"
                            var cartStoreID = self.productDetails?.storeID ?? "0"
                            if self.isViewForReOrder{
                                if self.productDetailsArray.count > 0{
                                    let details = self.productDetailsArray[0];
                                    cartStoreID = details.storeID
                                }
                            }
                            let convenienceFee = responseDict["convenience_fee"]?.stringValue ?? "0"
                            let dropEta = shipChargesDict["drop_eta"]?.stringValue ?? "0"
                            
                            let devliveryDetails: JSON = ["delivery_fee": deliveryCharges, "store_id":  cartStoreID, "convenience_fee": convenienceFee,"drop_eta": dropEta]
                            self.deliveryChargesArray.append(devliveryDetails)
                        }
                        else if let shipChargesArray  = responseDict["shippings"]?.array{
                            
                            for shipChargesDict in shipChargesArray{
                                
                                let deliveryCharges = shipChargesDict["fee"].stringValue
                                let cartStoreID = shipChargesDict["store_id"].stringValue
                                let convenienceFee = responseDict["convenience_fee"]?.string ?? "0"
                                let dropEta = shipChargesDict["drop_eta"].stringValue
                                let devliveryDetails: JSON = ["delivery_fee": deliveryCharges, "store_id":  cartStoreID, "convenience_fee": convenienceFee,"drop_eta": dropEta]
                                
                                self.deliveryChargesArray.append(devliveryDetails)
                            }
                        }
                    }
                    else if let responseDictArray  = result!["response"]?.array{
                        
                        for responseDict in responseDictArray{
                            if let shipChargesDict  = responseDict["ship_charges"].dictionary{
                                
                                let deliveryCharges = shipChargesDict["fee"]?.string ?? "0"
                                let cartStoreID = shipChargesDict["store_id"]?.string ?? "0"
                                let convenienceFee = responseDict["convenience_fee"].string ?? "0"
                                let dropEta = shipChargesDict["drop_eta"]?.stringValue ?? "0"
                                let devliveryDetails: JSON = ["delivery_fee": deliveryCharges, "store_id":  cartStoreID, "convenience_fee": convenienceFee,"drop_eta": dropEta]
                                self.deliveryChargesArray.append(devliveryDetails)
                            }
                        }
                    }
                    self.placeholderButton.isEnabled = true
                    //self.lblErrorMessage.text = ""
                    //  self.errorMessageContainerView.isHidden = false
                    //  self.errorMessageContainerView.isHidden = true
                    
                    
                    self.tableOrderSummary.reloadData()
                }
                else{
                    
                    let errorMsg = result!["message"]?.string
                    
                    
                    self.placeholderButton.isEnabled = false
                    
                }
            }
            else{
                
                self.placeholderButton.isEnabled = false
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            self.tableOrderSummary.reloadData()
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    func callCreateOrderAPI(params: Parameters)  {
        
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if let response = result!["response"]?.dictionary{
                        self.orderAmountValue = response["order_price"]?.string ?? ""
                        self.orderTransactionID = response["txn_id"]?.string ?? ""
                        UserDefaults.standard.set("final", forKey: "order")
                        UserDefaults.standard.synchronize()
                        
                        let params: Parameters = ["option": "place_order", "amount": self.orderAmountValue, "nonce": "nonce", "txn_id": self.orderTransactionID]
                        self.updatePlaceorderToServer(params: params)
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string ?? kErrorMsg
                    AppHelper.showAlertView(message: errorMsg)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    func updatePlaceorderToServer(params: Parameters) {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner(loaderMessage: kPaymentMsg)
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    let oderConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationViewController")
                    self.navigationController?.pushViewController(oderConfirmVC!, animated: true)
                    
                    AppHelper.sharedInstance.removeSpinner()
                    
                }
                else{
                    
                    let errorMsg = result!["message"]?.string ?? kErrorMsg
                    AppHelper.showAlertView(message: errorMsg)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    
    // For Getting the Order Status.
    func callDeliverdOrderAPI(params: Parameters)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kSellerAppURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            self.deliveryChargesArray.removeAll()
            if (result != nil){
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                if let responseDict  = result!["response"]?.dictionary{
                    
                    print(responseDict)
                }
                    self.tableOrderSummary.reloadData()
                }
                else{
                    let errorMsg = result!["message"]?.string
                    
                    
                }
            }
            else{
                
               
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            self.tableOrderSummary.reloadData()
            AppHelper.sharedInstance.removeSpinner()
        }
    }
    
    func callOrderDeatilApi(params :Parameters, isNeedToShowLoader: Bool)  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            DispatchQueue.main.async {
                // self.refreshControl.endRefreshing()
                if (result != nil){
                    let statusCode = result!["status"]?.string
                    if statusCode == "200"
                    {
                         if  let dataDict = result!["response"]?.dictionary{
                            if let details = dataDict["order"]?.dictionary{
                                self.orderDetails = OrderDetails.parseOrderDetails(details: dataDict["order"]!)
                                
                                self.productDetailsArray = self.orderDetails!.productDetailsArray
                               // self.orderDetails = self.ordersDetailsArray[0];
                                self.orderState = self.orderDetails!.order_status
                                self.setData()
                                if  self.orderDetails!.order_status == "5" || self.orderDetails!.order_status == "6" || self.orderDetails!.order_status == "7" {
                                    
                                }
                                else if self.orderDetails!.order_status == "1" {
                                    
                                }
                                else {
                                    
                                }
                                
                                self.tableOrderSummary.reloadData()
                            }
                        }
                    }
                    else{
                        let errorMsg = result!["message"]?.string
                        AppHelper.showAlertView(message: errorMsg!)
                    }
                }
                else{
                    AppHelper.showAlertView(message: kErrorMsg)
                }
                AppHelper.sharedInstance.removeSpinner()
            }
        }
    }
    
    
    //Delegate Methods:
    func cancelTapped() {
        let params: Parameters = ["option": "cancel_order","store_id":"\(orderDetails!.productDetailsArray[0].storeInfo.store_id)","order_id":"\(orderDetails!.order_id)"]
        self.orderDetails?.order_status = "6"
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
       // let yourDate = formatter.date(from: myString)
        self.orderDetails?.cancel_time =
            AppHelper.convertDateStringToFormattedDateString(date: myString) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: myString)
        callChangeStatusApi(params: params, isNeedToShowLoader: true)
        
    }
    
    func approvedTapped() {
        let params: Parameters = ["option": "approve_order","store_id":"\(orderDetails!.productDetailsArray[0].storeInfo.store_id)","order_id":"\(orderDetails!.order_id)"]
        self.orderDetails?.order_status = "2"
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         formatter.timeZone = TimeZone(abbreviation: "UTC")
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        // let yourDate = formatter.date(from: myString)
        self.orderDetails?.approve_time = myString
        callChangeStatusApi(params: params, isNeedToShowLoader: true)
    }
    
    func markAsDelivered() {
        let params: Parameters = ["option": "mark_as_delivered","store_id":"\(orderDetails!.productDetailsArray[0].storeInfo.store_id)","order_id":"\(orderDetails!.order_id)"]
        self.orderDetails?.order_status = "5"
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        // let yourDate = formatter.date(from: myString)
        self.orderDetails?.delivered_time = myString
        callChangeStatusApi(params: params, isNeedToShowLoader: true)
    }
    
    func readyToPickTapped() {
        let params: Parameters = ["option": "mark_as_pick","store_id":"\(orderDetails!.productDetailsArray[0].storeInfo.store_id)","order_id":"\(orderDetails!.order_id)"]
        self.orderDetails?.order_status = "3"
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        // let yourDate = formatter.date(from: myString)
        if self.orderDetails?.delivery_type == "1"{
            self.orderDetails?.ready_time = myString
        }else{
            self.orderDetails?.ready_time = myString
        }
        
            
        callChangeStatusApi(params: params, isNeedToShowLoader: true)
        
    }
    func callChangeStatusApi(params:[String:Any],isNeedToShowLoader :Bool)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                let file_path = result!["file_path"]?.dictionary
              //  self.user_Image = (file_path!["user_image"]?.string)!
                if statusCode == "200"
                {
                    self.tableOrderSummary.reloadData()
                    if  let dataDict = result!["response"]?.dictionary{
                        
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            if isNeedToShowLoader {
                AppHelper.sharedInstance.removeSpinner()
            }
        }
        
    }
    
    
    @IBAction func changeStatusBtnTapped(_ sender: Any) {
        if self.actionPerform == 1{
            markAsDelivered()
        }else{
            if(changeStatusView != nil && !changeStatusView!.isHidden)
            {
                changeStatusView!.removeFromSuperview()
            }
            changeStatusView = Bundle.main.loadNibNamed("ChangeStatus", owner: self, options: nil)?.first as? ChangeStatus
            changeStatusView?.delegate=self
            changeStatusView?.status = orderDetails!.order_status
            changeStatusView?.currentStatus.text = orderState
            changeStatusView?.deliveryType = orderDetails!.delivery_type
            let currentWindow: UIWindow? = UIApplication.shared.keyWindow
            changeStatusView!.frame = UIApplication.shared.keyWindow!.frame
            currentWindow?.addSubview(changeStatusView!)
        }
        
    }
    
    func downAction() {
        changeStatusView?.removeFromSuperview()
    }
    
    
    @IBAction func cancelOrderAction(_ sender: Any) {
    }
    
    @IBAction func browseMoreAction(_ sender: Any) {
    }
    @objc func buttonClicked(sender:UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if   let cell = tableOrderSummary.cellForRow(at: indexPath) as? AddressTableViewCell {
            
            cell.txtPhone.isEnabled = true;
            cell.viewPhone.isHidden = false;
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


// MARK: - UINavigationControllerDelegate

extension OrderTrackingViewController: UINavigationControllerDelegate {
    
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return .portrait
    }
}



