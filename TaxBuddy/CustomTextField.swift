//
//  CustomTextField.swift
//  TaxBuddy
//
//  Created by Vikash Rajput on 25/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
    
    override func draw(_ rect: CGRect) {
        let placeholderText = placeholder
        if (placeholderText != nil) {
            attributedPlaceholder = NSAttributedString(string: placeholderText!, attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 122.0/255, green: 135.0/255, blue: 187.0/255, alpha: 1.0)])
        }
        
    }

}
