//
//  OrdersViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 09/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import XLActionController
class OrdersViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,ProfileDropDownDelegate,OrdersTableViewCellDelegate {
   
    
   var refreshControl = UIRefreshControl()
   var popUpStatus = -1
   var index = -1
    var user_Image = ""
     var tag = 0
    @IBOutlet weak var statusLabel: UILabel!
    var moreBtnPopUp:ProfileDropDown?
    var isNeedLoaderMore = false;
    var pageNumber = 1
    @IBOutlet weak var filterStatusContainer: UIView!
    @IBOutlet weak var itemList: UITableView!
    var ordersDetailsArray = Array<OrderDetails>()
    var filteredOrdersDetailsArray = Array<OrderDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         let nib = UINib(nibName: "LoadMoreTableViewCell", bundle: nil)
        itemList.register(nib, forCellReuseIdentifier: "LoadMoreTableViewCell")
         //callGetOrderApi(isNeedToShowLoader: false)
    }
    @objc func reloadingData(){
        pageNumber = 1
        statusLabel.text = "All"
        ordersDetailsArray.removeAll()
        filteredOrdersDetailsArray.removeAll()
        itemList.reloadData()
        callGetOrderApi(isNeedToShowLoader: true)
    }
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.reloadingData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadingData), name: NSNotification.Name(rawValue: "OrdersButtonTapped"), object: nil)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        itemList.addSubview(refreshControl) // not required whe
        
        filterStatusContainer.layer.cornerRadius = 5
        itemList.delegate=self
        itemList.dataSource=self
        reloadingData()
    }
    
    func callGetOrderApi(isNeedToShowLoader :Bool)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let params: Parameters = ["option": "orders","store_id":"\(UserDefaults.standard.string(forKey: "store_id")!)","store_gmt":"\(UserDefaults.standard.string(forKey: "store_gmt")!)","page": String(pageNumber)]
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                let file_path = result!["file_path"]?.dictionary
                self.user_Image = (file_path!["user_image"]?.string)!
                if statusCode == "200"
                {
                    if  let dataDict = result!["response"]?.dictionary{
                        
                        if let ordersArray = dataDict["orders"]?.array{
                            let tempArray = OrderDetails.getAllOrderList(ordersArray: ordersArray)
                            
                            if self.ordersDetailsArray.count == 0 {
                                self.ordersDetailsArray = tempArray
                            }
                            else{
                                self.ordersDetailsArray.append(contentsOf: tempArray)
                            }
                            
                            if let totalPodcuts = dataDict["total"]?.intValue{
                                if totalPodcuts >= self.ordersDetailsArray.count{
                                    self.isNeedLoaderMore = true;
                                    self.pageNumber += 1;
                                }
                            }
                            
                            
                            if let limit = dataDict["limit"]?.intValue{
                                if tempArray.count < limit{
                                    self.isNeedLoaderMore = false;
                                }
                            }
                            if self.ordersDetailsArray.count > 0{
                                self.filteredOrdersDetailsArray = self.ordersDetailsArray
                                self.itemList.reloadData()
                            }
                            
                            self.refreshControl.endRefreshing()
                            
                            
                        }
                        else{
                            if self.ordersDetailsArray.count == 0{
                                
                                // self.tableMyorder.isHidden = true;
                                // self.lblOrder.isHidden = false;
                            }
                            self.itemList.reloadData()
                            self.refreshControl.endRefreshing()
 
                        }
 
                    }
 
                }
                else{
                    if let errorMsg = result!["message"]?.string{
                        AppHelper.showAlertView(message: errorMsg)
                    }else{
                        AppHelper.showAlertView(message: "No Data Found")
                    }
                    self.refreshControl.endRefreshing()
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
                self.refreshControl.endRefreshing()
            }
            
            if isNeedToShowLoader {
                AppHelper.sharedInstance.removeSpinner()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfSections: Int = 0
        if filteredOrdersDetailsArray.count > 0
        {
            tableView.separatorStyle = .none
            if isNeedLoaderMore{
                numOfSections            = filteredOrdersDetailsArray.count + 1
            }else{
                numOfSections            = filteredOrdersDetailsArray.count
            }
            
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No Order"
            noDataLabel.textColor     = UIColor.darkGray
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == filteredOrdersDetailsArray.count{
            var cell = itemList.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            callGetOrderApi(isNeedToShowLoader: false)
            isNeedLoaderMore = false
            return cell!
        }else{
            let cell = itemList.dequeueReusableCell(withIdentifier: "OrdersTableViewCell") as! OrdersTableViewCell
            cell.delegate=self
            // cell.productImage.
            cell.moreBtnOutlet.tag = indexPath.row
            if cell.moreBtnOutlet.tag == index{
                cell.dropDownView.isHidden = false
            }else{
                cell.dropDownView.isHidden = true
            }
            let profileimageUrl = UserDetails.sharedInstance.userImagePath + filteredOrdersDetailsArray[indexPath.row].profile_image
            if let url = URL(string: profileimageUrl){
                cell.profileImage.setImage(with: url , placeholder: UIImage(named: "profile_pic"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        cell.profileImage.image = image
                    }
                    else{
                        cell.profileImage.image = UIImage(named: "profile_pic")
                    }
                })
            }
            else{
                cell.profileImage.image = UIImage(named: "profile_pic")
            }
            
            cell.orderedLbl.text = "Order# \(filteredOrdersDetailsArray[indexPath.row].order_id)"
            cell.cancelOutlet.tag = indexPath.row
            cell.approvedOutlet.tag = indexPath.row
            cell.readyToPickOutlet.tag = indexPath.row
            cell.selectionStyle = .none
            cell.moreBtnOutlet.tag = indexPath.row
            cell.productName.text = filteredOrdersDetailsArray[indexPath.row].productDetailsArray[0].productName
            cell.productPriceLbl.text = UserDetails.sharedInstance.currenySymbol + filteredOrdersDetailsArray[indexPath.row].orderPrice
            cell.productQuantity.text = filteredOrdersDetailsArray[indexPath.row].productDetailsArray[0].cartAddedQuantities
            cell.orderDate.text = AppHelper.convertDateStringToFormattedDateString(date: filteredOrdersDetailsArray[indexPath.row].date_added) + " at " + AppHelper.convertDateStringToFormattedTimeString(date: filteredOrdersDetailsArray[indexPath.row].date_added)
            cell.customerMobile.text = AppHelper.format(phoneNumber: filteredOrdersDetailsArray[indexPath.row].dropPhone)
            cell.customerName.text = filteredOrdersDetailsArray[indexPath.row].drop_name
            cell.cancelOutlet.setTitleColor(UIColor.black, for: .normal)
            cell.approvedOutlet.setTitleColor(UIColor.black, for: .normal)
            cell.statusLabel.layer.borderColor = UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1).cgColor
            cell.moreBtnOutlet.isHidden = false
            if Int(filteredOrdersDetailsArray[indexPath.row].order_status) == 1{
                cell.statusLabel.text = "  PENDING  "
                cell.cancelOutlet.isEnabled = true
                //cell.statusLabel.layer.borderColor = UIColor.yellow.cgColor
                cell.approvedOutlet.isEnabled = true
                self.popUpStatus = 0
            }else if Int(filteredOrdersDetailsArray[indexPath.row].order_status) == 2{
                cell.statusLabel.text = "  APPROVED  "
                cell.statusLabel.layer.borderColor = UIColor(red: 0/255, green: 144/255, blue: 81/255, alpha: 1).cgColor
                cell.cancelOutlet.setTitleColor(UIColor.lightGray, for: .normal)
                cell.approvedOutlet.setTitleColor(UIColor.lightGray, for: .normal)
                cell.cancelOutlet.isEnabled = false
                cell.approvedOutlet.isEnabled = false
                self.popUpStatus = 1
            }else if Int(filteredOrdersDetailsArray[indexPath.row].order_status) == 3{
                if filteredOrdersDetailsArray[indexPath.row].delivery_type == "1"{
                    cell.statusLabel.text = "  READY TO SHIP  "
                }else{
                    cell.statusLabel.text = "  READY TO PICK  "
                }
                
                cell.cancelOutlet.isEnabled = true
                cell.statusLabel.layer.borderColor = UIColor(red: 255/255, green: 212/255, blue: 121/255, alpha: 1).cgColor
                cell.moreBtnOutlet.isHidden = true
                cell.approvedOutlet.isEnabled = true
            }else if Int(filteredOrdersDetailsArray[indexPath.row].order_status) == 4{
                cell.statusLabel.text = "  SHIPPED  "
                cell.cancelOutlet.isEnabled = true
                cell.moreBtnOutlet.isHidden = true
                cell.statusLabel.layer.borderColor = UIColor(red: 255/255, green: 212/255, blue: 121/255, alpha: 1).cgColor
                cell.approvedOutlet.isEnabled = true
            }else if Int(filteredOrdersDetailsArray[indexPath.row].order_status) == 5{
                cell.statusLabel.text = "  DELIVERED  "
                cell.statusLabel.layer.borderColor = UIColor(red: 4/255, green: 51/255, blue: 255/255, alpha: 1).cgColor
                cell.moreBtnOutlet.isHidden = true
                cell.cancelOutlet.isEnabled = true
                cell.approvedOutlet.isEnabled = true
            }else if Int(filteredOrdersDetailsArray[indexPath.row].order_status) == 6{
                cell.statusLabel.text = "  CANCELED BY USER  "
                cell.statusLabel.layer.borderColor = UIColor.red.cgColor
                cell.cancelOutlet.isEnabled = true
                self.popUpStatus = 2
                cell.moreBtnOutlet.isHidden = true
                cell.approvedOutlet.isEnabled = true
            }else if Int(filteredOrdersDetailsArray[indexPath.row].order_status) == 7{
                cell.statusLabel.text = "  CANCELED  "
                cell.cancelOutlet.isEnabled = true
                self.popUpStatus = 3
                cell.statusLabel.layer.borderColor = UIColor.red.cgColor
                cell.moreBtnOutlet.isHidden = true
                cell.approvedOutlet.isEnabled = true
            }else{
                cell.statusLabel.text = "  ALL  "
                cell.cancelOutlet.isEnabled = true
                cell.approvedOutlet.isEnabled = true
            }
            cell.deliveryType = filteredOrdersDetailsArray[indexPath.row].delivery_type
            cell.popUpStatus = self.popUpStatus
            let imageUrl = UserDetails.sharedInstance.productImagePath + filteredOrdersDetailsArray[indexPath.row].productDetailsArray[0].productImage
            if let url = URL(string: imageUrl){
                cell.productImage.setImage(with: url , placeholder: UIImage(named: "icon_placeholder_image"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        cell.productImage.image = image
                    }
                    else{
                        cell.productImage.image = UIImage(named: "icon_placeholder_image")
                    }
                })
            }
            else{
                cell.productImage.image = UIImage(named: "icon_placeholder_image")
            }
            return cell
        }
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad{
            if indexPath.row == filteredOrdersDetailsArray.count{
                return 50
            }else{
                return 271
            }
           
            
        }else{
            if indexPath.row == filteredOrdersDetailsArray.count{
                return 50
            }else{
                return 208
            }
           
        }
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom == .pad{
            let storyboard = UIStoryboard(name: "MainIPadStoryboard", bundle: nil)
            let  vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackingViewController") as! OrderTrackingViewController
            vc.orderDetails = filteredOrdersDetailsArray[indexPath.row]
            vc.productDetailsArray = filteredOrdersDetailsArray[indexPath.row].productDetailsArray
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let  vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackingViewController") as! OrderTrackingViewController
            vc.orderDetails = filteredOrdersDetailsArray[indexPath.row]
            vc.productDetailsArray = filteredOrdersDetailsArray[indexPath.row].productDetailsArray
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func moreBtnTapped(index: Int) {
        self.index = index
        itemList.reloadData()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if index > -1{
            self.index = -1
            itemList.reloadData()
        }
    }
   
    
    @IBAction func dropDownAction(_ sender: UIButton) {
      
       // addMoreBtnPopUp()
        
        let alertController = UIAlertController(title: nil, message: "Filter By:", preferredStyle: .actionSheet)
        
        let all = UIAlertAction(title: "All", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.AllAction()
        })
        
        let pending = UIAlertAction(title: "Pending", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.PendingAction()
        })
        
        
        let approved = UIAlertAction(title: "Approved", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.ApprovedAction()
        })
        
        let readyToPick = UIAlertAction(title: "Ready To Pick", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.ReadyToPickAction()
        })
        
        let shipped = UIAlertAction(title: "Shipped", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.ShippedAction()
        })
        
        
        let delivered = UIAlertAction(title: "Delivered", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.DeliveredAction()
        })
        
        let canceled = UIAlertAction(title: "Canceled by Seller", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.CancelActionUser()
        })
        
        let canceledByUser = UIAlertAction(title: "Canceled by User", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            
            self.CancelAction()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        all.setValue(UIColor.black, forKey: "titleTextColor")
        pending.setValue(UIColor.black, forKey: "titleTextColor")
        readyToPick.setValue(UIColor.black, forKey: "titleTextColor")
        shipped.setValue(UIColor.black, forKey: "titleTextColor")
        approved.setValue(UIColor.black, forKey: "titleTextColor")
        delivered.setValue(UIColor.black, forKey: "titleTextColor")
        canceled.setValue(UIColor.black, forKey: "titleTextColor")
        canceledByUser.setValue(UIColor.black, forKey: "titleTextColor")
        
        alertController.addAction(all)
        alertController.addAction(pending)
        alertController.addAction(approved)
        alertController.addAction(readyToPick)
        alertController.addAction(shipped)
        alertController.addAction(delivered)
        alertController.addAction(canceled)
        alertController.addAction(canceledByUser)
        
        alertController.addAction(cancel)
//        var height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.50)
//        alertController.view.addConstraint(height);
        if UIDevice.current.userInterfaceIdiom == .pad{
            if let popoverController = alertController.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
        }
       
        
        self.present(alertController, animated: true, completion: nil)
 
    }
    
    func addMoreBtnPopUp(){
        if(moreBtnPopUp != nil && !moreBtnPopUp!.isHidden)
        {
            moreBtnPopUp!.removeFromSuperview()
        }
        moreBtnPopUp = Bundle.main.loadNibNamed("ProfileDropDown", owner: self, options: nil)?.first as? ProfileDropDown
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        moreBtnPopUp!.frame = UIApplication.shared.keyWindow!.frame
        moreBtnPopUp?.delegate=self
        currentWindow?.addSubview(moreBtnPopUp!)
    }
    
    func AllAction() {
        filteredOrdersDetailsArray.removeAll()
        filteredOrdersDetailsArray = ordersDetailsArray
        moreBtnPopUp?.removeFromSuperview()
        statusLabel.text = "All"
        itemList.reloadData()
    }
    
    func PendingAction() {
        filteredOrdersDetailsArray.removeAll()
        for i in 0..<ordersDetailsArray.count{
            if ordersDetailsArray[i].order_status == "1"{
                filteredOrdersDetailsArray.append(ordersDetailsArray[i])
            }
        }
        statusLabel.text = "Pending"
        moreBtnPopUp?.removeFromSuperview()
        itemList.reloadData()
    }
    func ApprovedAction() {
        filteredOrdersDetailsArray.removeAll()
        for i in 0..<ordersDetailsArray.count{
            if ordersDetailsArray[i].order_status == "2"{
                filteredOrdersDetailsArray.append(ordersDetailsArray[i])
            }
        }
        statusLabel.text = "Approved"
        moreBtnPopUp?.removeFromSuperview()
        itemList.reloadData()
    }
    
    func ReadyToPickAction() {
        filteredOrdersDetailsArray.removeAll()
        for i in 0..<ordersDetailsArray.count{
            if ordersDetailsArray[i].order_status == "3"{
                filteredOrdersDetailsArray.append(ordersDetailsArray[i])
            }
        }
        statusLabel.text = "Ready to Pick"
        moreBtnPopUp?.removeFromSuperview()
        itemList.reloadData()
    }
    
    func ShippedAction() {
        filteredOrdersDetailsArray.removeAll()
        for i in 0..<ordersDetailsArray.count{
            if ordersDetailsArray[i].order_status == "4"{
                filteredOrdersDetailsArray.append(ordersDetailsArray[i])
            }
        }
        statusLabel.text = "Shipped"
        moreBtnPopUp?.removeFromSuperview()
        itemList.reloadData()
    }
    
    func DeliveredAction() {
        filteredOrdersDetailsArray.removeAll()
        for i in 0..<ordersDetailsArray.count{
            if ordersDetailsArray[i].order_status == "5"{
                filteredOrdersDetailsArray.append(ordersDetailsArray[i])
            }
        }
        statusLabel.text = "Delivered"
        moreBtnPopUp?.removeFromSuperview()
        itemList.reloadData()
    }
    
    func CancelAction() {
        filteredOrdersDetailsArray.removeAll()
        for i in 0..<ordersDetailsArray.count{
            if ordersDetailsArray[i].order_status == "6"{
                filteredOrdersDetailsArray.append(ordersDetailsArray[i])
            }
        }
        statusLabel.text = "Canceled by User"
        moreBtnPopUp?.removeFromSuperview()
        itemList.reloadData()
    }
    
    
    func CancelActionUser() {
        filteredOrdersDetailsArray.removeAll()
        for i in 0..<ordersDetailsArray.count{
            if ordersDetailsArray[i].order_status == "7"{
                filteredOrdersDetailsArray.append(ordersDetailsArray[i])
            }
        }
        statusLabel.text = "Canceled"
        moreBtnPopUp?.removeFromSuperview()
        itemList.reloadData()
    }
    
    func crossAction() {
        moreBtnPopUp?.removeFromSuperview()
    }
    //Delegate Methods:
    func cancelTapped(index: Int) {
        let params: Parameters = ["option": "cancel_order","store_id":"\(filteredOrdersDetailsArray[index].productDetailsArray[0].storeInfo.store_id)","order_id":"\(filteredOrdersDetailsArray[index].order_id)"]
        callChangeStatusApi(params: params, isNeedToShowLoader: true)

    }
    
    func approvedTapped(index: Int) {
        let params: Parameters = ["option": "approve_order","store_id":"\(filteredOrdersDetailsArray[index].productDetailsArray[0].storeInfo.store_id)","order_id":"\(filteredOrdersDetailsArray[index].order_id)"]
       callChangeStatusApi(params: params, isNeedToShowLoader: true)
    }
    
    func readyToPickTapped(index: Int) {
        let params: Parameters = ["option": "mark_as_pick","store_id":"\(filteredOrdersDetailsArray[index].productDetailsArray[0].storeInfo.store_id)","order_id":"\(filteredOrdersDetailsArray[index].order_id)"]
        callChangeStatusApi(params: params, isNeedToShowLoader: true)

    }
    
    @IBAction func moreBtnAction(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "Mark This Order As", preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.cancelTapped(index: sender.tag)
        })
        
        let approved = UIAlertAction(title: "Approved", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.approvedTapped(index: sender.tag)
        })
        
        
        let readyToPick = UIAlertAction(title: "Ready To Pick", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.readyToPickTapped(index: sender.tag)
        })
        
        let close = UIAlertAction(title: "Close", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        cancel.isEnabled = true
        approved.isEnabled = true
        readyToPick.isEnabled = true
        cancel.setValue(UIColor.black, forKey: "titleTextColor")
        approved.setValue(UIColor.black, forKey: "titleTextColor")
        readyToPick.setValue(UIColor.black, forKey: "titleTextColor")
        if self.filteredOrdersDetailsArray[sender.tag].order_status == "1" {
            cancel.isEnabled = true
            approved.isEnabled = true
            readyToPick.isEnabled = false
            cancel.setValue(UIColor.black, forKey: "titleTextColor")
            approved.setValue(UIColor.black, forKey: "titleTextColor")
            readyToPick.setValue(UIColor.lightGray, forKey: "titleTextColor")
        }else if self.filteredOrdersDetailsArray[sender.tag].order_status == "2"{
            cancel.isEnabled = false
            approved.isEnabled = false
            readyToPick.isEnabled = true
            cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
            approved.setValue(UIColor.lightGray, forKey: "titleTextColor")
            readyToPick.setValue(UIColor.black, forKey: "titleTextColor")
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            if let popoverController = alertController.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
        }
        
        alertController.addAction(cancel)
        alertController.addAction(approved)
        alertController.addAction(readyToPick)
         alertController.addAction(close)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callChangeStatusApi(params:[String:Any],isNeedToShowLoader :Bool)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                let file_path = result!["file_path"]?.dictionary
                self.user_Image = (file_path!["user_image"]?.string)!
                if statusCode == "200"
                {
                   
                    self.filteredOrdersDetailsArray.removeAll()
                    self.ordersDetailsArray.removeAll()
                    
                    self.callGetOrderApi(isNeedToShowLoader: false)
                    if  let dataDict = result!["response"]?.dictionary{
                        
                    }
                    
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            if isNeedToShowLoader {
                AppHelper.sharedInstance.removeSpinner()
            }
        }
        
    }
    

}
