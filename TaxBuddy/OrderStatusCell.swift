//
//  OrderStatusCell.swift
//  Posh_IT
//
//  Created by MADSTECH on 13/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class OrderStatusCell: UITableViewCell {
    
    @IBOutlet weak var orderApprovedLbl: UILabel!
    @IBOutlet weak var cancelTimeLbl: UILabel!
    @IBOutlet weak var cancelStatusView: UIView!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imageStatus1: UIImageView!
    @IBOutlet weak var imageStatus2: UIImageView!
    @IBOutlet weak var imageStatus3: UIImageView!
    @IBOutlet weak var imageStatus4: UIImageView!
    @IBOutlet weak var imageStatus5: UIImageView!
    @IBOutlet weak var lblOrderPlacedTime: UILabel!
    @IBOutlet weak var lblOrderApprovedTime: UILabel!
    @IBOutlet weak var lblOrderProcessTime: UILabel!
    @IBOutlet weak var lblOrderShippedTime: UILabel!
    @IBOutlet weak var lblOrderDeliverdTime: UILabel!
    
    @IBOutlet weak var statusReadyToShip: UILabel!
    
    @IBOutlet weak var packageDeliveredTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusPackageShipped: UILabel!
    @IBOutlet weak var statusPackageDelivered: UILabel!
    @IBOutlet weak var shippedLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cancelStatusView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
