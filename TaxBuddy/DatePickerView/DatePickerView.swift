//
//  DatePickerView.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 19/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
protocol DatePickerViewDelegate {
    func doneBtnTapped()
}

class DatePickerView: UIView {

    var delegate:DatePickerViewDelegate?
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBAction func doneBtnAction(_ sender: Any) {
        self.delegate?.doneBtnTapped()
    }
    
    override func awakeFromNib() {
        //datePickerView.minimumDate = Date()
    }

}
