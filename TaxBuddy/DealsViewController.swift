//
//  DealsViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 09/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class DealsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var isNeedLoaderMore = false;
    var pageNumber = 1
    var dealDetailsArray = Array<DealDetails>()

    var refreshControl = UIRefreshControl()
    
    private var dealsCell:DealsTableViewCell?
    var index = -1
    
    @IBOutlet weak var itemList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadingData), name: NSNotification.Name(rawValue: "DealsButtonTapped"), object: nil)
        itemList.addSubview(refreshControl)
        itemList.delegate=self
        itemList.dataSource=self
        self.reloadingData()
       // callDealApi(isNeedToShowLoader: false)
        // Do any additional setup after loading the view.
    }
    
    @objc func reloadingData(){
        dealDetailsArray.removeAll()
        
        itemList.reloadData()
        callDealApi(isNeedToShowLoader: true)
    }
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
       self.reloadingData()
    }
    
    func callDealApi(isNeedToShowLoader :Bool)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        if isNeedToShowLoader {
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let params: Parameters = ["option": "deals","store_id":"\(UserDefaults.standard.string(forKey: "store_id")!)","store_gmt":"\(UserDefaults.standard.string(forKey: "store_gmt")!)","page": String(pageNumber)]
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if  let dataDict = result!["response"]?.dictionary{
                        
                        if let dealsArray = dataDict["deals"]?.array{
                            let tempArray = DealDetails.getAllDealsList(dealsArray: dealsArray)
                            
                            if self.dealDetailsArray.count == 0 {
                                self.dealDetailsArray = tempArray
                            }
                            else{
                              //  self.dealDetailsArray.append(contentsOf: tempArray)
                            }
                            self.refreshControl.endRefreshing()
                            self.itemList.reloadData()
                            /*
                            if let totalPodcuts = dataDict["total"]?.intValue{
                                if totalPodcuts >= self.ordersDetailsArray.count{
                                    self.isNeedLoaderMore = true;
                                    self.pageNumber += 1;
                                }
                            }
                            
                            
                            if let limit = dataDict["limit"]?.intValue{
                                if tempArray.count < limit{
                                    self.isNeedLoaderMore = false;
                                }
                            }
                            
                            if self.ordersDetailsArray.count > 0{
                                //self.tableMyorder.isHidden = false;
                                //  self.lblOrder.isHidden = true;
                                self.itemList.reloadData()
                            }
                            else {
                                //   self.tableMyorder.isHidden = true;
                                //   self.lblOrder.isHidden = false;
                            }
                            */
                        }
                        else{
                            if self.dealDetailsArray.count == 0{
                                // self.tableMyorder.isHidden = true;
                                // self.lblOrder.isHidden = false;
                            }
                            self.refreshControl.endRefreshing()
                        }
                    }
                }
                else{
                    if let errorMsg = result!["message"]?.string{
                        AppHelper.showAlertView(message: errorMsg)
                    }else{
                        AppHelper.showAlertView(message: "No Data Found")
                    }
                    self.refreshControl.endRefreshing()
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
                self.refreshControl.endRefreshing()
            }
            
            if isNeedToShowLoader {
                AppHelper.sharedInstance.removeSpinner()
            }
            self.refreshControl.endRefreshing()
        }
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dealDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "DealsTableViewCell") as! DealsTableViewCell
        cell.selectionStyle = .none
        cell.changeDateOutlet.tag = indexPath.row
        cell.offerName.text = dealDetailsArray[indexPath.row].deal_name
       
        let fromDate = AppHelper.convertSpecifiedDateStringToFormattedDateString(date: dealDetailsArray[indexPath.row].from_date)
        let endDate = AppHelper.convertSpecifiedDateStringToFormattedDateString(date: dealDetailsArray[indexPath.row].end_date)
        cell.validDate.text = "from " + fromDate + " at " + AppHelper.formattedTimeFromString(dateString: dealDetailsArray[indexPath.row].from_time, withFormat: "h:mm a")!
        cell.toDateLbl.text = "to " + endDate + " at " + AppHelper.formattedTimeFromString(dateString: dealDetailsArray[indexPath.row].end_time, withFormat: "h:mm a")!
        cell.dealsPercentOff.text = "\(dealDetailsArray[indexPath.row].deal_max_discount)%"
        cell.activateSwitch.tag = indexPath.row
        if dealDetailsArray[indexPath.row].deal_status == "1"{
            cell.activateSwitch.isOn = true
        }else{
            cell.activateSwitch.isOn = false
        }
        let profileimageUrl = UserDetails.sharedInstance.dealImagePath + dealDetailsArray[indexPath.row].deal_image
        if let url = URL(string: profileimageUrl){
            cell.dealImage.setImage(with: url , placeholder: UIImage(named: "Square"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    cell.dealImage.image = image
                }
                else{
                    cell.dealImage.image = UIImage(named: "Square")
                }
            })
        }
        else{
            cell.dealImage.image = UIImage(named: "Square")
        }
        
        index = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return 255
            
        }else{
            return 177
        }
    }
    @IBAction func changeDateAction(_ sender: UIButton) {
        
        let changeDateVc = self.storyboard!.instantiateViewController(withIdentifier: "ChangeDateViewController") as! ChangeDateViewController
        changeDateVc.deal_id = dealDetailsArray[sender.tag].deal_id
        changeDateVc.fromStart = dealDetailsArray[sender.tag].from_date
        changeDateVc.fromClose = dealDetailsArray[sender.tag].from_time
        changeDateVc.toStart = dealDetailsArray[sender.tag].end_date
        changeDateVc.toClose = dealDetailsArray[sender.tag].end_time
        self.navigationController?.pushViewController(changeDateVc, animated: true)
    }
    
    @IBAction func activateOfferAction(_ sender: UISwitch) {
        if dealDetailsArray[(sender.tag)].deal_status == "1"{
            let params = ["option": "deal_edit","deal_id":"\(dealDetailsArray[(sender.tag)].deal_id)","status":"2"]
            //     dealsCell?.activateSwitch.isOn = true
            callActivateDeactivateDealApi(params:params)
        }else if dealDetailsArray[(sender.tag)].deal_status == "2"{
            let params = ["option": "deal_edit","deal_id":"\(dealDetailsArray[(sender.tag)].deal_id)","status":"1"]
            //     dealsCell?.activateSwitch.isOn = true
            callActivateDeactivateDealApi(params:params)
        }
    }
    
    func callActivateDeactivateDealApi(params:Parameters)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
      

        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if  let dataDict = result!["response"]?.dictionary{
                        
                        if let dealsArray = dataDict["deals"]?.array{
                            
                           
                        }
                        else{
                            if self.dealDetailsArray.count == 0{
                                // self.tableMyorder.isHidden = true;
                                // self.lblOrder.isHidden = false;
                            }
                        }
                    }
                }
                else{
                    if let errorMsg = result!["message"]?.string{
                        AppHelper.showAlertView(message: errorMsg)
                    }else{
                        AppHelper.showAlertView(message: "Something went wrong")
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            
        }
        
    }
}
