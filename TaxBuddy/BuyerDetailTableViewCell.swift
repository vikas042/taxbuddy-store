//
//  BuyerDetailTableViewCell.swift
//  TaxBuddy
//
//  Created by MAC on 10/1/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class BuyerDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var profileNumber: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        
        containerView.layer.masksToBounds = false
        containerView.layer.cornerRadius = 5
        containerView.layer.shadowColor = UIColor.gray.cgColor
        containerView.layer.shadowOpacity = 1
        containerView.layer.shadowOffset = CGSize(width: -1, height: 1)
        containerView.layer.shadowRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
