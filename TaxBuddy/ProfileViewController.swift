//
//  ProfileViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 09/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import MapleBacon
import SwiftyJSON
import Alamofire
class ProfileViewController: UIViewController,DatePickerViewDelegate {
    
    
    
    var datePicker:DatePickerView?
    
    var opeingTime = ""
    var closingTime = ""
    var isCloseTime = 1
    @IBOutlet weak var datePickerView: UIDatePicker!
  
    @IBOutlet weak var timingBtn: UIButton!
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var switchOutlet: UISwitch!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var storeCategory: UILabel!
    @IBOutlet weak var storeAddress: UILabel!
    @IBOutlet weak var storeEmail: UILabel!
    @IBOutlet weak var storeMobile: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var openTimeBtn: UIButton!
    var formattedOpenTime = ""
    var formattedCloseTime = ""
    @IBOutlet weak var closeTimeBtn: UIButton!
    @IBOutlet weak var openTime: UILabel!
    @IBOutlet weak var closeTime: UILabel!
    
    @IBOutlet weak var openTimeView: UIView!
    
    @IBOutlet weak var closeTimeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openTime.textColor = UIColor.gray
        closeTime.textColor = UIColor.gray
        openTimeBtn.isEnabled = false
        closeTimeBtn.isEnabled = false
        
        imageContainerView.layer.cornerRadius = imageContainerView.frame.height/2
        imageContainerView.layer.borderWidth = 3
        imageContainerView.clipsToBounds = true
        imageContainerView.layer.borderColor = UIColor(red: 122/255, green: 135/255, blue: 187/255, alpha: 1).cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        
        openTimeView.layer.cornerRadius = 15
        closeTimeView.layer.cornerRadius = 15
        openTimeView.layer.borderWidth = 1
        openTimeView.layer.borderColor = UIColor.lightGray.cgColor
        closeTimeView.layer.borderWidth = 1
        closeTimeView.layer.borderColor = UIColor.lightGray.cgColor
        NotificationCenter.default.addObserver(self, selector: #selector(self.setData), name: NSNotification.Name(rawValue: "ProfileButtonTapped"), object: nil)
      //  switchOutlet.transform = CGAffineTransform(scaleX: 1, y: 0.75)
        //AppHelper.poshITCustomPopupShadow(view: openTimeView)
        //AppHelper.poshITCustomPopupShadow(view: closeTimeView)
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       setData()
    }
    
    
    @objc func setData(){
        if StoreDetail.sharedInstance.in_service == "1"{
            switchOutlet.setOn(true, animated: true)
        }else{
            switchOutlet.setOn(false, animated: true)
        }
        storeName.text = UserDefaults.standard.string(forKey: "store_name")
        storeAddress.text = UserDefaults.standard.string(forKey: "store_address")
        storeEmail.text = UserDefaults.standard.string(forKey: "store_email")
        storeMobile.text = AppHelper.format(phoneNumber: UserDefaults.standard.string(forKey: "store_phone")!)
        openTime.text = AppHelper.formattedTimeFromString(dateString: UserDefaults.standard.string(forKey: "store_open_time")!, withFormat: "hh:mm a")
        closeTime.text = AppHelper.formattedTimeFromString(dateString: UserDefaults.standard.string(forKey: "store_close_time")!, withFormat: "hh:mm a")
        formattedOpenTime = UserDefaults.standard.string(forKey: "store_open_time")!
        formattedCloseTime = UserDefaults.standard.string(forKey: "store_close_time")!
        // closeTime.text = UserDefaults.standard.string(forKey: "store_close_time")
        var imageUrl = ""
        if UserDetails.sharedInstance.storeImagePath == ""{
            imageUrl = "https://api.taxbuddyapp.com/uploads/store_images/" + UserDefaults.standard.string(forKey: "store_image")!
        }else{
            imageUrl = UserDetails.sharedInstance.storeImagePath + UserDefaults.standard.string(forKey: "store_image")!
        }
        
        if let url = URL(string: imageUrl){
            profileImageView.setImage(with: url , placeholder: UIImage(named: "profile_pic"), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self!.profileImageView.image = image
                }
                else{
                    self!.profileImageView.image = UIImage(named: "profile_pic")
                }
            })
        }
        else{
            profileImageView.image = UIImage(named: "profile_pic")
        }
    }
    
    func addDatePicker(){
        if(datePicker != nil && !datePicker!.isHidden)
        {
            datePicker!.removeFromSuperview()
        }
        datePicker = Bundle.main.loadNibNamed("DatePickerView", owner: self, options: nil)?.first as? DatePickerView
        datePicker?.delegate=self

        datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.time
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        datePicker!.frame = UIApplication.shared.keyWindow!.frame
        currentWindow?.addSubview(datePicker!)
    }
    
    @IBAction func closeTimeAction(_ sender: Any) {
        isCloseTime = 1
      //  datePickerView.datePickerMode = UIDatePicker.Mode.time
      //  pickerContainerView.isHidden = false
        addDatePicker()
    }
    
    @IBAction func openTimeAction(_ sender: Any) {
        isCloseTime = 0
       // datePickerView.datePickerMode = UIDatePicker.Mode.time
      //  pickerContainerView.isHidden = false
        addDatePicker()
    }
    
   
    
    func doneBtnTapped() {
        if isCloseTime == 0{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            let strDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            dateFormatter.dateFormat = "HH:mm"
            formattedOpenTime = dateFormatter.string(from: datePicker!.datePickerView.date)
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.time
            openTime.text = strDate
            
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            let strDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            dateFormatter.dateFormat = "HH:mm"
            formattedCloseTime = dateFormatter.string(from: datePicker!.datePickerView.date)
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.time
            closeTime.text = strDate
        }
        datePicker?.removeFromSuperview()
    }
    
    @IBAction func donePickerAction(_ sender: Any) {
        
       
    }
    
    
    @IBAction func changeSwitchAction(_ sender: Any) {
        var value = ""
        if switchOutlet.isOn{
            value = "online"
        }else{
            value = "offline"
        }
         let params = ["option": "edit_availability","store_id":"\(StoreDetail.sharedInstance.store_id)","store_gmt":"\(StoreDetail.sharedInstance.store_gmt)","in_service":value]
        callActivateDeactivateStoreApi(params:params)
    }
    
    @IBAction func timingBtnAction(_ sender: Any) {

        let params = ["option": "edit_timing","store_id":"\(StoreDetail.sharedInstance.store_id)","store_gmt":"\(StoreDetail.sharedInstance.store_gmt)","store_open_time":"\(formattedOpenTime)","store_close_time":"\(formattedCloseTime)"]
        //     dealsCell?.activateSwitch.isOn = true
        callEditTimeApi(params:params)
    }
    
    @IBAction func editBtnAction(_ sender: Any) {
        openTime.textColor = UIColor.black
        closeTime.textColor = UIColor.black
        openTimeBtn.isEnabled = true
        closeTimeBtn.isEnabled = true
        timingBtn.setImage(UIImage(named: "statusDoneIcon"), for: .normal)
        editBtn.isHidden = true
        formattedOpenTime = UserDefaults.standard.string(forKey: "store_open_time")!
        formattedCloseTime = UserDefaults.standard.string(forKey: "store_close_time")!
        
    }
    
    
    func callEditTimeApi(params:Parameters)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
  
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
        
                    self.openTime.textColor = UIColor.gray
                    self.closeTime.textColor = UIColor.gray
                    self.openTimeBtn.isEnabled = false
                    self.closeTimeBtn.isEnabled = false
                    self.timingBtn.setImage(UIImage(named: "statusInProcessIcon"), for: .normal)
                    self.editBtn.isHidden = false
                    UserDefaults.standard.set(self.formattedOpenTime, forKey: "store_open_time")
                    UserDefaults.standard.set(self.formattedCloseTime, forKey: "store_close_time")
                    
                    
                    if  let dataDict = result!["response"]?.dictionary{
                        /*
                        StoreDetail.sharedInstance.store_gmt = dataDict["store_gmt"]?.string ?? ""
                        StoreDetail.sharedInstance.store_distance = dataDict["store_gmt"]?.string ?? ""
                        StoreDetail.sharedInstance.store_id = dataDict["store_id"]?.string ?? ""
                        StoreDetail.sharedInstance.store_image = dataDict["store_image"]?.string ?? ""
                        
                        StoreDetail.sharedInstance.in_service = dataDict["in_service"]?.string ?? ""
                        
                        StoreDetail.sharedInstance.store_name = dataDict["store_name"]?.string ?? ""
                        StoreDetail.sharedInstance.store_latitude = dataDict["store_latitude"]?.string ?? ""
                        StoreDetail.sharedInstance.store_longitude = dataDict["store_longitude"]?.string ?? ""
                        
                        StoreDetail.sharedInstance.store_image = dataDict["store_image"]?.string ?? ""
                        StoreDetail.sharedInstance.store_close_time = dataDict["store_close_time"]?.string ?? ""
                        StoreDetail.sharedInstance.store_open_time = dataDict["store_open_time"]?.string ?? ""
                        StoreDetail.sharedInstance.store_address = dataDict["store_address"]?.string ?? ""
                        StoreDetail.sharedInstance.store_phone = dataDict["store_phone"]?.string ?? ""
                        StoreDetail.sharedInstance.store_email = dataDict["store_email"]?.string ?? ""
                        AppHelper.saveStoreDetails()
                        */
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            
        }
        
    }
    
    func callActivateDeactivateStoreApi(params:Parameters)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }

        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    
                    if self.switchOutlet.isOn ==  true{
                        StoreDetail.sharedInstance.in_service = "1"
                    }else{
                        StoreDetail.sharedInstance.in_service = "0"
                    }
                    AppHelper.saveStoreDetails()
                    if  let dataDict = result!["response"]?.dictionary{
                        
                        if let dealsArray = dataDict["deals"]?.array{
                            
                            
                        }
                        else{
                            
                        }
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            
        }
        
    }
    

}
