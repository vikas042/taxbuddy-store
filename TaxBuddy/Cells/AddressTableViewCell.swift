//
//  AddressTableViewCell.swift
//  Posh_IT
//
//  Created by MADSTECH on 08/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell,UITextViewDelegate {
  
    
    @IBOutlet weak var cityLbl: UITextField!
    
    @IBOutlet weak var stateLbl: UITextField!
    
    @IBOutlet weak var zipCodeLbl: UITextField!
    
    @IBOutlet weak var mobileLbl: UITextField!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var pickAtStore: UIButton!
    @IBOutlet weak var delieverMe: UIButton!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lblAddress: UITextView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnPhoneChange: UIButton!
    @IBOutlet weak var viewPhone: UIView!
    
    @IBOutlet weak var appartmentNumber: UITextField!
    
    @IBOutlet weak var timeBtn: UIButton!
    @IBOutlet weak var dateBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        viewCell.layer.masksToBounds = false
        viewCell.layer.cornerRadius = 5
        viewCell.layer.shadowColor = UIColor.gray.cgColor
        viewCell.layer.shadowOpacity = 1
        viewCell.layer.shadowOffset = CGSize(width: -1, height: 1)
        viewCell.layer.shadowRadius = 2
        
        
        
        dateBtn.layer.cornerRadius = 5
        
        dateBtn.layer.borderColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1).cgColor
        dateBtn.layer.borderWidth = 1
        
        timeBtn.layer.cornerRadius = 5
        timeBtn.layer.borderColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1).cgColor
        timeBtn.layer.borderWidth = 1
        
        lblAddress.layer.cornerRadius = 5
        lblAddress.text = "Address"
        //lblAddress.textColor = UIColor.lightGray
        lblAddress.delegate=self
        lblAddress.layer.borderColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1).cgColor
        lblAddress.layer.borderWidth = 1
        
        timeBtn.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        dateBtn.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        pickAtStore.layer.cornerRadius = 5
        delieverMe.layer.cornerRadius = 5
        pickAtStore.backgroundColor = UIColor.white
        pickAtStore.setImage(UIImage(named: "iconBagBlue"), for: .normal)
  
        delieverMe.layer.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        delieverMe.setTitleColor(UIColor(red: 152/255, green: 156/255, blue: 157/255, alpha: 1), for: .normal)
       // AppHelper.poshITCustomPopupShadow(view: pickAtStore)
        
    }
    
//    func adjustUITextViewHeight(arg : UITextView)
//    {
//        arg.translatesAutoresizingMaskIntoConstraints = true
//        arg.sizeToFit()
//        arg.isScrollEnabled = false
//    }
    
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Address"
            textView.textColor = UIColor.lightGray
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
