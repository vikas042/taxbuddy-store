
//
//  InnerCellTableViewCell.swift
//  Alamofire
//
//  Created by MADSTECH on 09/08/18.
//

import UIKit

class InnerCellTableViewCell: UITableViewCell {

    @IBOutlet weak var promoAmountLbl: UILabel!
    @IBOutlet weak var promoLbl: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblNumberOfUnit: UILabel!
    @IBOutlet weak var lblPriceDetail: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
