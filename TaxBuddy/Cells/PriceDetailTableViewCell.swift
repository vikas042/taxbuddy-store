//
//  PriceDetailTableViewCell.swift
//  Posh_IT
//
//  Created by MADSTECH on 08/08/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit

class PriceDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var promoTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var deliveryChargeViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var deliveryChargeView1: UIView!
    @IBOutlet weak var deliveryChargeView2: UIView!
   
    @IBOutlet weak var shippingCharge2: UILabel!
    @IBOutlet weak var lblDeliveryCharges1: UILabel!
    @IBOutlet weak var deliveryChargeText1: UILabel!
    @IBOutlet weak var shippingCharge1: UILabel!
    @IBOutlet weak var deliveryChargeText: UILabel!
    @IBOutlet weak var promoCodeButton: UIButton!
    @IBOutlet weak var lblDeliveryCharges: UILabel!
    @IBOutlet weak var lblTotalCharges: UILabel!
    @IBOutlet weak var tableInner: UITableView!
    @IBOutlet weak var viewCell: UIView!
    
    @IBOutlet weak var removePromoCodeButton: UIButton!
    @IBOutlet weak var lblPromoCodeTitle: UILabel!
    @IBOutlet weak var lblConvenenceFee: UILabel!
    
    @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPromoCodeAmt: UILabel!
    @IBOutlet weak var lblPromoCode: UILabel!
    
    @IBOutlet weak var constraintLeadinglblPromocode: NSLayoutConstraint!
    
    @IBOutlet weak var constraintLeadinglblTitlePromocode: NSLayoutConstraint!
    
    override func awakeFromNib() {
        promoCodeButton.isHidden = true
        super.awakeFromNib()
        layoutIfNeeded()
        viewCell.layer.masksToBounds = false
        viewCell.layer.cornerRadius = 5
        viewCell.layer.shadowColor = UIColor.gray.cgColor
        viewCell.layer.shadowOpacity = 1
        viewCell.layer.shadowOffset = CGSize(width: -1, height: 1)
        viewCell.layer.shadowRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension PriceDetailTableViewCell{
    func setTableViewDataSourceDelegate
        <D:UITableViewDelegate & UITableViewDataSource>
        (_ dataSourceDelegate: D,forRow row:Int)
    {
        tableInner.delegate = dataSourceDelegate
        tableInner.dataSource = dataSourceDelegate
        
        tableInner.reloadData()
    }
    
}
