//
//  ChangeDateViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 19/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ChangeDateViewController: UIViewController,DatePickerViewDelegate {
    
    var fromStart = "MM/DD/YYYY"
    var fromClose = "00:00"
    var toStart = "MM/DD/YYYY"
    var toClose = "00:00"
    @IBOutlet weak var note1Lbl: UILabel!
    @IBOutlet weak var note2Lbl: UILabel!
    
    var isFromStart = 0
    var isFromClose = 0
    var isToStart = 0
    var isToClose = 0
    
    var realDate = ""
    var realTime = ""
    var deal_id = ""

    @IBOutlet weak var updateNowButton: UIButton!
    @IBOutlet weak var fromStartLabel: UILabel!
    @IBOutlet weak var fromCloseLabel: UILabel!
    @IBOutlet weak var toStartLabel: UILabel!
    @IBOutlet weak var toCloseLabel: UILabel!
    
    var formattedStartDate = ""
    var formattedCloseDate = ""
    var formattedStartTime = ""
    var formattedCloseTime = ""
    
    @IBOutlet weak var fromCloseTimeView: UIView!
    @IBOutlet weak var fromStartTimeView: UIView!
    @IBOutlet weak var toStartTimeView: UIView!
    @IBOutlet weak var toCloseTimeView: UIView!
    
    var datePicker:DatePickerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fromCloseTimeView.layer.cornerRadius = 15
        fromStartTimeView.layer.cornerRadius = 15
        fromCloseTimeView.layer.borderWidth = 1
        fromCloseTimeView.layer.borderColor = UIColor.lightGray.cgColor
        fromStartTimeView.layer.borderWidth = 1
        fromStartTimeView.layer.borderColor = UIColor.lightGray.cgColor
        toStartTimeView.layer.cornerRadius = 15
        toCloseTimeView.layer.cornerRadius = 15
        toStartTimeView.layer.borderWidth = 1
        toStartTimeView.layer.borderColor = UIColor.lightGray.cgColor
        toCloseTimeView.layer.borderWidth = 1
        toCloseTimeView.layer.borderColor = UIColor.lightGray.cgColor
        
        updateNowButton.applyGradient(colours: [UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1),UIColor(red: 132/255, green: 199/255, blue: 158/255, alpha: 1)])

    }
    override func viewWillAppear(_ animated: Bool) {
        formattedStartTime = fromClose
        formattedCloseTime = toClose
        formattedStartDate = AppHelper.convertDateStringToSendDateString(date: fromStart)
        formattedCloseDate = AppHelper.convertDateStringToSendDateString(date: toStart)
        fromStartLabel.text = AppHelper.convertSpecifiedDateStringToFormattedDateString(date: fromStart)
        fromCloseLabel.text = AppHelper.formattedTimeFromString(dateString: fromClose, withFormat: "h:mm a")!
        toStartLabel.text = AppHelper.convertSpecifiedDateStringToFormattedDateString(date: toStart)
        toCloseLabel.text = AppHelper.formattedTimeFromString(dateString: toClose, withFormat: "h:mm a")!
        note1Lbl.text = "\u{2022} Start Date must be greater than or equal to Current Date."
        note2Lbl.text = "\u{2022} End Date must be greater than or equal to Start Date."
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addDatePicker(){
        if(datePicker != nil && !datePicker!.isHidden)
        {
            datePicker!.removeFromSuperview()
        }
        datePicker = Bundle.main.loadNibNamed("DatePickerView", owner: self, options: nil)?.first as? DatePickerView
        datePicker?.delegate=self
        if isFromStart == 1 || isToStart == 1 {
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.date
            datePicker?.datePickerView.minimumDate = Date()
        }else{
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.time
        }
        
       
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        datePicker!.frame = UIApplication.shared.keyWindow!.frame
        currentWindow?.addSubview(datePicker!)
    }
    
    
    func callDashboardApi(params :[String : String])  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kSellerAppURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                UserDetails.sharedInstance.userToken = result!["token"]?.string
                if statusCode == "200"
                {
                    if let response = result!["response"]?.dictionary{
                        
                        
                        print(response)
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            AppHelper.sharedInstance.removeSpinner()
            
        }
    }
    
    @IBAction func updateNowAction(_ sender: Any) {
        let params = ["option": "deal_edit","deal_id":"\(deal_id)","from_date":"\(formattedStartDate)","end_date":"\(formattedCloseDate)","start_time":"\(formattedStartTime)","end_time":"\(formattedCloseTime)"]
        callUpdateNowApi(params: params)
    }
    func doneBtnTapped() {
        if isFromStart == 1{
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/YYYY"
            let strDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            dateFormatter.dateFormat = "YYYY/MM/dd"
            formattedStartDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            realDate = strDate
            fromStartLabel.text = realDate
        
        }else if isFromClose == 1{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let strDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            dateFormatter.dateFormat = "HH:mm"
            formattedStartTime = dateFormatter.string(from: datePicker!.datePickerView.date)
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.time
            realTime = strDate
            fromCloseLabel.text = realTime
        }else if isToStart == 1{
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/YYYY"
            let strDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            dateFormatter.dateFormat = "YYYY/MM/dd"
            formattedCloseDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            realDate = strDate
            toStartLabel.text = realDate
            
        }else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let strDate = dateFormatter.string(from: datePicker!.datePickerView.date)
            dateFormatter.dateFormat = "HH:mm"
            formattedCloseTime = dateFormatter.string(from: datePicker!.datePickerView.date)
            datePicker!.datePickerView.datePickerMode = UIDatePicker.Mode.time
            realTime = strDate
            toCloseLabel.text = realTime
        }
        datePicker?.removeFromSuperview()
    }
    @IBAction func fromStartDateAction(_ sender: Any) {
        isFromStart = 1
        isFromClose = 0
        isToStart = 0
        isToClose = 0
        addDatePicker()
    }
    @IBAction func fromCloseDateAction(_ sender: Any) {
        isFromStart = 0
        isFromClose = 1
        isToStart = 0
        isToClose = 0
        addDatePicker()
    }
    
    @IBAction func toStartDateAction(_ sender: Any) {
        isFromStart = 0
        isFromClose = 0
        isToStart = 1
        isToClose = 0
        addDatePicker()
    }
    @IBAction func toCloseDateAction(_ sender: Any) {
        isFromStart = 0
        isFromClose = 0
        isToStart = 0
        isToClose = 1
        addDatePicker()
        
    }
    func callUpdateNowApi(params:Parameters)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    if  let dataDict = result!["response"]?.dictionary{
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    let errorMsg = result!["message"]?.string
                    AppHelper.showAlertView(message: errorMsg!)
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            
        }
        
    }
}
