//
//  AppDelegate.swift
//  TaxBuddy
//
//  Created by Vikash Rajput on 25/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import GoogleSignIn
import IQKeyboardManagerSwift
import UserNotifications

enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod touch style UI
    case pad // iPad style UI
}

@available(iOS 10.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate  {


    var window: UIWindow?
    let TRACKORDER_CATEGORY = "TRACKORDER_CATEGORY"

  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        AppHelper.getUserDetails();
        AppHelper.getStoreDetails()
        let userID = UserDetails.sharedInstance.userID
        GIDSignIn.sharedInstance().clientID = "382809007874-j147tl23iv903ajjcjdkbmemvldum80g.apps.googleusercontent.com"
        IQKeyboardManager.shared.enable = true

        if userID.count > 0{
            if UIDevice.current.userInterfaceIdiom == .pad{
                let storyboard = UIStoryboard.init(name: "MainIPadStoryboard", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
                let navigationVC = UINavigationController(rootViewController: homeVC)
                navigationVC.navigationBar.isHidden = true
                APPDELEGATE.window?.rootViewController = navigationVC
                
            }else{
                AppHelper.makeHomeViewControllerAsRootViewController()
            }
            
        }
        let counterCategory = UIMutableUserNotificationCategory()
        counterCategory.identifier = TRACKORDER_CATEGORY
        
        // increment Action
        let incrementAction = UIMutableUserNotificationAction()
        incrementAction.identifier = "TRACK_ORDER_BUTTON"
        incrementAction.title = "Track"
        incrementAction.activationMode = UIUserNotificationActivationMode.foreground
        incrementAction.isAuthenticationRequired = true
        incrementAction.isDestructive = false
        
        // A. Set actions for the default context
        counterCategory.setActions([incrementAction],
                                   for: UIUserNotificationActionContext.default)
        
        
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: [counterCategory])
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        
        UNUserNotificationCenter.current().delegate = self
       
        return true
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        UserDetails.sharedInstance.deviceToken = deviceTokenString
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
    

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        if UIApplication.shared.applicationState == UIApplication.State.active {
            
            if let userInfoDict = userInfo["data"] as? NSDictionary {
                let type = userInfoDict["notification_type"]
                let notificationType = String(format: "%@", type as! CVarArg)
                
                if (notificationType == "1")  || (notificationType == "2") || (notificationType == "3") || (notificationType == "4") || (notificationType == "5") || (notificationType == "6") || (notificationType == "7") || (notificationType == "8") || (notificationType == "32") || (notificationType == "52"){
                    
                    if let params = userInfoDict["params"] as? NSDictionary{
                        let orderID = params["order_id"] as? String
                        
                        let navVC : UINavigationController = (APPDELEGATE.window?.rootViewController as? UINavigationController)!
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let orderTrackVC = storyboard.instantiateViewController(withIdentifier: "OrderTrackingViewController") as! OrderTrackingViewController
                        orderTrackVC.orderID = orderID!
                        
                        orderTrackVC.source = "NotificationController"
                        navVC.pushViewController(orderTrackVC, animated: true)
                        
                        UserDetails.sharedInstance.apiHitInt = "1"
                        UserDetails.sharedInstance.orderId = orderID!
                    }
                }
            }
            
            let notification = Int(UserDetails.sharedInstance.notificaionCount)
            let newNotification = notification! + 1
            let stringNotification = String(newNotification)
            UserDetails.sharedInstance.notificaionCount = stringNotification
        }
        else if UIApplication.shared.applicationState == UIApplication.State.background
        {
            
            if let userInfoDict = userInfo["data"] as? NSDictionary {
                let type = userInfoDict["notification_type"]
                let notificationType = String(format: "%@", type as! CVarArg)
                
                if (notificationType == "1")  || (notificationType == "2") || (notificationType == "3") || (notificationType == "4") || (notificationType == "5") || (notificationType == "6") || (notificationType == "7") || (notificationType == "8") || (notificationType == "32") || (notificationType == "52"){
                    
                    if let params = userInfoDict["params"] as? NSDictionary{
                        let orderID = params["order_id"] as? String
                        
                        let navVC : UINavigationController = (APPDELEGATE.window?.rootViewController as? UINavigationController)!
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let orderTrackVC = storyboard.instantiateViewController(withIdentifier: "OrderTrackingViewController") as! OrderTrackingViewController
                        orderTrackVC.orderID = orderID!
                        
                        orderTrackVC.source = "NotificationController"
                        navVC.pushViewController(orderTrackVC, animated: true)
                        
                        UserDetails.sharedInstance.apiHitInt = "1"
                        UserDetails.sharedInstance.orderId = orderID!
                    }
                    
                    
                }
                
            }
        }
        else{
            if let userInfoDict = userInfo["data"] as? NSDictionary {
                let type = userInfoDict["notification_type"]
                let notificationType = String(format: "%@", type as! CVarArg)
                
                if (notificationType == "1")  || (notificationType == "2") || (notificationType == "3") || (notificationType == "4") || (notificationType == "5") || (notificationType == "6") || (notificationType == "7") || (notificationType == "8") || (notificationType == "32") || (notificationType == "52"){
                    
                    if let params = userInfoDict["params"] as? NSDictionary{
                        let orderID = params["order_id"] as? String
                        
                        let navVC : UINavigationController = (APPDELEGATE.window?.rootViewController as? UINavigationController)!
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let orderTrackVC = storyboard.instantiateViewController(withIdentifier: "OrderTrackingViewController") as! OrderTrackingViewController
                        orderTrackVC.orderID = orderID!
                        
                        orderTrackVC.source = "NotificationController"
                        navVC.pushViewController(orderTrackVC, animated: true)
                        
                        UserDetails.sharedInstance.apiHitInt = "1"
                        UserDetails.sharedInstance.orderId = orderID!
                    }
                }
                
            }
        }
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: () -> Void) {
        
        let userInfo = notification.userInfo;
        if notification.category == TRACKORDER_CATEGORY {
            if identifier == "TRACK_ORDER_BUTTON"{
                
                if let userInfoDict = userInfo?["data"] as? NSDictionary {
                    let type = userInfoDict["notification_type"]
                    let notificationType = String(format: "%@", type as! CVarArg)
                    /*
                    if (notificationType == "4") {
                        if let params = userInfoDict["params"] as? NSDictionary{
                            if let orderID = params["order_id"] as? String{
                                if let navVC : UINavigationController = APPDELEGATE.window?.rootViewController as? UINavigationController{
                                    
                                    let currentVC = navVC.visibleViewController;
                                    
                                }
                            }
                        }
                    }
                    */
                }
            }
        }
        completionHandler()
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
