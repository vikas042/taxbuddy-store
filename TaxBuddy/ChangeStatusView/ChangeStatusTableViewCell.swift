//
//  ChangeStatusTableViewCell.swift
//  TaxBuddy
//
//  Created by MAC on 10/1/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class ChangeStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var orderStatusLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
