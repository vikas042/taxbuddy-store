//
//  ChangeStatus.swift
//  TaxBuddy
//
//  Created by MAC on 10/1/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
protocol ChangeStatusDelegate{
    func downAction()
    func cancelTapped()
    func approvedTapped()
    func readyToPickTapped()
}

class ChangeStatus: UIView,UITableViewDelegate,UITableViewDataSource {
    
    var status = ""
    var deliveryType = ""
    
    var actionPerformed = -1
    @IBOutlet weak var currentStatus: UILabel!
    @IBOutlet weak var itemList: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var upperView: UIView!
    var delegate:ChangeStatusDelegate?
    override func awakeFromNib() {
        currentStatus.text = status
        itemList.delegate=self
        itemList.dataSource=self
        AppHelper.showShadowOnView(view: upperView)
        let nib = UINib(nibName: "ChangeStatusTableViewCell", bundle: nil)
        itemList.register(nib, forCellReuseIdentifier: "ChangeStatusTableViewCell")
        
    }
    var statusArray = ["Cancel","Approved","Ready To Ship"]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var counter = 0
        if status == "1"{
            counter = 2
            
        }else if status == "2"{
            counter = 1
           
        }else if status == "3"{
            counter = 0
            
        }
        var numOfSections: Int = 0
        if counter > 0
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = counter
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "Nothing To Show"
            noDataLabel.textColor     = UIColor.darkGray
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "ChangeStatusTableViewCell") as! ChangeStatusTableViewCell
        //cell.statusButton.isEnabled = false
        cell.selectionStyle = .none
        cell.orderStatusLbl.text = statusArray[indexPath.row]
        if status == "1"{
            if indexPath.row == 0{
                if actionPerformed == 0{
                    cell.statusButton.setImage(UIImage(named: "iconSelected"), for: .normal)
                }else{
                    cell.statusButton.setImage(UIImage(named: "statusInProcessIcon"), for: .normal)
                }
                cell.orderStatusLbl.text = "Cancel"
            }else{
                if actionPerformed == 1 || actionPerformed == -1{
                    cell.statusButton.setImage(UIImage(named: "iconSelected"), for: .normal)
                }else{
                    cell.statusButton.setImage(UIImage(named: "statusInProcessIcon"), for: .normal)
                }
                cell.orderStatusLbl.text = "Approve"
                
            }
        }else if status == "2"{
            if indexPath.row == 0{
                actionPerformed = 2
                if deliveryType == "1"{
                    cell.orderStatusLbl.text = "Ready to Ship"
                }else{
                    cell.orderStatusLbl.text = "Ready To Pick"
                }
                cell.statusButton.setImage(UIImage(named: "iconSelected"), for: .normal)
            }
        }else if status == "3"{
            actionPerformed = 3
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = itemList.cellForRow(at: indexPath) as! ChangeStatusTableViewCell
        if status == "1"{
            if indexPath.row == 0{
                cell.statusButton.setImage(UIImage(named: "iconSelected"), for: .normal)
                
                actionPerformed = 0
                
            }else if indexPath.row == 1{
                cell.statusButton.setImage(UIImage(named: "iconSelected"), for: .normal)
                
                actionPerformed = 1
            }
            tableView.reloadData()
            
        }else if status == "2"{
            if indexPath.row == 0{
                cell.statusButton.setImage(UIImage(named: "iconSelected"), for: .normal)
                
                actionPerformed = 2
            }
        }else if status == "3"{
            if indexPath.row == 0{
              //  cell.statusButton.setImage(UIImage(named: "iconSelected"), for: .normal)
            }
        }
        
    }
    
    @IBAction func downBtnTapped(_ sender: Any) {
        self.delegate?.downAction()
    }
    
    @IBAction func changeStatusAction(_ sender: Any) {
        if status == "1"{
            if actionPerformed == -1{
                actionPerformed = 1
            }
        }
        if actionPerformed == 0{
            self.delegate?.cancelTapped()
        }else if actionPerformed == 1{
            self.delegate?.approvedTapped()
        }else if actionPerformed == 2{
            self.delegate?.readyToPickTapped()
        }
        if status == "3"{
            AppHelper.showAlertView(message: "Nothing To Show")
        }
        actionPerformed = -1
        self.removeFromSuperview()
    }
}
