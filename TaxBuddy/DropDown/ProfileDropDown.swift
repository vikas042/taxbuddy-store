//
//  ProfileDropDown.swift
//  TaxBuddy
//
//  Created by MAC on 9/20/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
protocol ProfileDropDownDelegate {
    func AllAction()
    func PendingAction()
    func ApprovedAction()
    func ReadyToPickAction()
    func ShippedAction()
    func DeliveredAction()
    func CancelAction()
    func crossAction()
}
class ProfileDropDown: UIView,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var itemList: UITableView!
    var delegate:ProfileDropDownDelegate?
    
    @IBOutlet weak var crossBtn: UIButton!
    let array = ["All","Pending","Approved","Ready to Pick","Shipped","Delivered","Cancelled"]
    override func awakeFromNib() {
        crossBtn.layer.cornerRadius = crossBtn.frame.height/2
        itemList.delegate=self
        itemList.dataSource=self
        let nib = UINib(nibName: "ProfileDropDownTableViewCell", bundle: nil)
        itemList.register(nib, forCellReuseIdentifier: "ProfileDropDownTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemList.dequeueReusableCell(withIdentifier: "ProfileDropDownTableViewCell") as! ProfileDropDownTableViewCell
        cell.selectionStyle = .none
        cell.itemLbl.text = array[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
         self.delegate?.AllAction()
        }else if indexPath.row == 1{
            self.delegate?.PendingAction()
        }else if indexPath.row == 2{
            self.delegate?.ApprovedAction()
        }else if indexPath.row == 3{
            self.delegate?.ReadyToPickAction()
        }else if indexPath.row == 4{
            self.delegate?.ShippedAction()
        }else if indexPath.row == 5{
            self.delegate?.DeliveredAction()
        }else{
            self.delegate?.CancelAction()
        }
    }

    @IBAction func crossAction(_ sender: Any) {
        self.delegate?.crossAction()
    }
    
}
