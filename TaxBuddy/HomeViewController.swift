//
//  HomeViewController.swift
//  TaxBuddy
//
//  Created by Vikash Rajput on 25/06/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
enum SelectedTab: Int {
    
    case DashboardTab = 100
    case OrdersTab = 200
    case DealsTab = 300
    case ProfileTab = 400
}

class HomeViewController: UIViewController {

   
    @IBOutlet weak var sliderView: UIView!
   
    @IBOutlet weak var pageTitle: UILabel!
    
    @IBOutlet weak var searchCircularView: UIView!
    @IBOutlet weak var dashboardView: UIView!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var dealView: UIView!
    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var tabContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabContainerView.layer.masksToBounds = false
        AppHelper.poshITCustomPopupShadow(view: searchCircularView)
        searchCircularView.layer.cornerRadius = searchCircularView.frame.height/2
        tabContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabContainerView.layer.shadowRadius = 10
        tabContainerView.layer.shadowColor = UIColor.gray.cgColor
        tabContainerView.layer.shadowOpacity = 0.5
        tabContainerView.layer.cornerRadius = 5
        print()
        dashboardView.isHidden = false
        orderView.isHidden = true
        searchView.isHidden = true
        dealView.isHidden = true
        profileView.isHidden = true
        
        
    }
    
  
    @IBAction func logoutButtonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "TaxBuddy Store", message: "Are you sure, you want to logout?", preferredStyle: .alert)
        
        let Ok = UIAlertAction(title: "Ok", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            let params = ["option": "logout","store_id":"\(UserDetails.sharedInstance.store_id)"]
            self.callLogoutApi(params: params)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            
        })
        alertController.addAction(Ok)
        alertController.addAction(cancel)
        self.navigationController?.present(alertController, animated: true, completion: nil)
        
    }
    
    func moveAnimatedView(xPosition:CGFloat,yPosition:CGFloat,width:CGFloat,height:CGFloat){
        UIView.animate(withDuration: 0.5, animations: {
            self.sliderView.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
            
        })
    }
    
    @IBAction func dashboardBtnAction(_ sender: Any) {
        sliderView.isHidden = false
        let xPosition = 0.0
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DashButtonTapped"), object:  SelectedTab.DashboardTab.rawValue)
        let yPosition = sliderView.frame.origin.y
        let width = sliderView.frame.size.width
        let height = sliderView.frame.size.height
        moveAnimatedView(xPosition: CGFloat(xPosition), yPosition: yPosition, width: width, height: height)
        dashboardView.isHidden = false
        orderView.isHidden = true
        searchView.isHidden = true
        dealView.isHidden = true
        profileView.isHidden = true
        pageTitle.text = "Dashboard"
        
    }
    
    @IBAction func ordersBtnAction(_ sender: Any) {
        sliderView.isHidden = false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OrdersButtonTapped"), object:  SelectedTab.OrdersTab.rawValue)
        let xPosition = sliderView.frame.size.width
        let yPosition = sliderView.frame.origin.y
        let width = sliderView.frame.size.width
        let height = sliderView.frame.size.height
        moveAnimatedView(xPosition: xPosition, yPosition: yPosition, width: width, height: height)
        dashboardView.isHidden = true
        orderView.isHidden = false
        searchView.isHidden = true
        dealView.isHidden = true
        profileView.isHidden = true
        pageTitle.text = "Orders"
        
    }
    @IBAction func searchBtnAction(_ sender: Any) {
        sliderView.isHidden = true
        
        let xPosition = sliderView.frame.size.width * 2
        let yPosition = sliderView.frame.origin.y
        let width = sliderView.frame.size.width
        let height = sliderView.frame.size.height
        moveAnimatedView(xPosition: xPosition, yPosition: yPosition, width: width, height: height)
        dashboardView.isHidden = true
        orderView.isHidden = true
        searchView.isHidden = false
        dealView.isHidden = true
        profileView.isHidden = true
        pageTitle.text = "Search"
    }
    @IBAction func dealsBtnAction(_ sender: Any) {
        sliderView.isHidden = false
        
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DealsButtonTapped"), object:  SelectedTab.DealsTab.rawValue)
        let xPosition = sliderView.frame.size.width * 3
        let yPosition = sliderView.frame.origin.y
        let width = sliderView.frame.size.width
        let height = sliderView.frame.size.height
        sliderView.isHidden = false
        moveAnimatedView(xPosition: xPosition, yPosition: yPosition, width: width, height: height)
        dashboardView.isHidden = true
        orderView.isHidden = true
        searchView.isHidden = true
        dealView.isHidden = false
        profileView.isHidden = true
        pageTitle.text = "Deals"
        
    }
    @IBAction func profileBtnAction(_ sender: Any) {
        sliderView.isHidden = false
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ProfileButtonTapped"), object:  SelectedTab.ProfileTab.rawValue)
        let xPosition = sliderView.frame.size.width * 4
        let yPosition = sliderView.frame.origin.y
        let width = sliderView.frame.size.width
        let height = sliderView.frame.size.height
        sliderView.isHidden = false
        moveAnimatedView(xPosition: xPosition, yPosition: yPosition, width: width, height: height)
        dashboardView.isHidden = true
        orderView.isHidden = true
        searchView.isHidden = true
        dealView.isHidden = true
        profileView.isHidden = false
        pageTitle.text = "Profile"
        
    }
   
    @IBAction func doneBtnAction(_ sender: Any) {
    }
    
    func callLogoutApi(params:Parameters)  {
        
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        
        WebServiceHandler.performPOSTRequest(withURL: kOrdersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                
                let statusCode = result!["status"]?.string
                if statusCode == "200"
                {
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
                    
                    let navVC = UINavigationController(rootViewController: loginVC!)
                    navVC.navigationBar.isHidden = true;
                    AppHelper.resetDefaults()
                    APPDELEGATE.window?.rootViewController = navVC;
                }
                else{
                    if let errorMsg = result!["message"]?.string{
                        AppHelper.showAlertView(message: errorMsg)
                    }else{
                        AppHelper.showAlertView(message: "No Data Found")
                    }
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            
            
        }
        
    }
    
}
extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
    }
}
