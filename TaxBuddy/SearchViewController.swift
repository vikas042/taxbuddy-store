//
//  SearchViewController.swift
//  TaxBuddy
//
//  Created by Ankit Kejriwal on 09/08/19.
//  Copyright © 2019 Vikash Rajput. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITextFieldDelegate {
    var productDetailsArray = [ProductDetails]()
    var orderDetails:OrderDetails?
    @IBOutlet weak var otpField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var fieldContainerView: UIView!
    
    @IBOutlet weak var orderBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBtn.layer.cornerRadius = searchBtn.frame.height/2
        searchBtn.clipsToBounds = true
        orderBtn.layer.cornerRadius = orderBtn.frame.height/2
        orderBtn.clipsToBounds = true
        otpField.delegate = self
        fieldContainerView.layer.cornerRadius = 15
        searchBtn.applyGradient(colours: [UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1),UIColor(red: 132/255, green: 199/255, blue: 158/255, alpha: 1)])
        orderBtn.applyGradient(colours: [UIColor(red: 45/255, green: 150/255, blue: 134/255, alpha: 1),UIColor(red: 132/255, green: 199/255, blue: 158/255, alpha: 1)])
        fieldContainerView.layer.borderWidth = 1
        fieldContainerView.layer.borderColor = UIColor.gray.cgColor
        // Do any additional setup after loading the view.
    }
    
    func callValidateOTPApi(params :[String : String])  {
        
        if !AppHelper.isInterNetConnectionAvailable(){
            AppHelper.showAlertView(message: kNoInternetConnection)
            return
        }
        AppHelper.sharedInstance.displaySpinner()
        
        WebServiceHandler.performPOSTRequest(withURL: kSellerAppURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if (result != nil){
                let statusCode = result!["status"]?.string
                UserDetails.sharedInstance.userToken = result!["token"]?.string
                if statusCode == "200"
                {
                    self.otpField.text = ""
                    if let response = result!["response"]{
                        
                        self.orderDetails = OrderDetails.parseOrderDetails(details: response)
                        self.productDetailsArray = self.orderDetails!.productDetailsArray
                        if UIDevice.current.userInterfaceIdiom == .pad{
                            let storyboard = UIStoryboard(name: "MainIPadStoryboard", bundle: nil)
                            let  vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackingViewController") as! OrderTrackingViewController
                            vc.orderDetails = self.orderDetails
                            vc.productDetailsArray = self.productDetailsArray
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }else{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let  vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackingViewController") as! OrderTrackingViewController
                            vc.orderDetails = self.orderDetails
                            vc.productDetailsArray = self.productDetailsArray
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        /*
                         totalPacked.text = reponse["totalPacked"].
                         totalPending = reponse["totalDelivered"]
                         totalShipped = reponse["totalShipped"]
                         totalApproved = reponse["totalApproved"]
                         totalCanelled = reponse["totalCancelled"]
                         totalDelivered = reponse["totalDelivered"]
                         */
                        print(response)
                        
                    }
                }
                else{
                    if let errorMsg = result!["message"]?.string{
                        AppHelper.showAlertView(message: errorMsg)
                    }else{
                        AppHelper.showAlertView(message: "Something went wrong")
                    }
                    self.otpField.text = ""
                }
            }
            else{
                AppHelper.showAlertView(message: kErrorMsg)
            }
            self.otpField.text = ""
            AppHelper.sharedInstance.removeSpinner()
            
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /*
        let maxLength = 6
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
       */
        return true
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        let parameters = ["option": "validate_otp","order_otp": "\(otpField.text!)"]
        callValidateOTPApi(params: parameters)
    }
    
    @IBAction func orderBtnAction(_ sender: Any) {
        let parameters = ["option": "validate_otp","order_number": "\(otpField.text!)"]
        callValidateOTPApi(params: parameters)
    }
    
}
